CREATE DATABASE db_staff;
USE db_staff;

CREATE TABLE table_staff(
	Staff_ID INT NOT NULL AUTO_INCREMENT,
	Name VARCHAR(30) NOT NULL,
	Gender VARCHAR(1) NOT NULL,
	Address VARCHAR(75) NOT NULL,
	Nationality VARCHAR(25) NOT NULL,
	Qualification VARCHAR(100) NOT NULL,
	Department_ID INT NOT NULL,
	Post VARCHAR(20) NOT NULL,
	Specialist VARCHAR(25) NOT NULL,
	Timing_ID INT NOT NULL,
	Payroll_ID INT NOT NULL,
	Enrolled_From DATE ,
	PRIMARY KEY(Staff_ID)
	);

 
CREATE TABLE table_staff_timing (
	Timing_ID INT NOT NULL AUTO_INCREMENT,
    Sun_Check_In TIME,
 	Sun_Check_Out TIME,
	Mon_Check_In TIME,
	Mon_Check_Out TIME,
	Tue_Check_In TIME,
	Tue_Check_Out TIME,
	Wed_Check_In TIME,
	Wed_Check_Out TIME,
	Thu_Check_In TIME,
	Thu_Check_Out TIME,
	Fri_Check_In TIME,
	Fri_Check_Out TIME,
	Sat_Check_In TIME,
	Sat_Check_Out TIME,
	UNIQUE(Sun_Check_In,Sun_Check_Out,Mon_Check_In,Mon_Check_Out,Tue_Check_In,Tue_Check_Out,Wed_Check_In,Wed_Check_Out,Thu_Check_In,Thu_Check_Out,Fri_Check_In,Fri_Check_Out,Sat_Check_In,Sat_Check_Out),
	PRIMARY KEY(Timing_ID)
	);


CREATE TABLE table_staff_CONTACT_NO (	Contact_Number VARCHAR(15) NOT NULL,
	Staff_ID INT NOT NULL,
	PRIMARY KEY(Contact_Number)
	);
				

/*---------------DATABASE PATIENT-------------*/
CREATE DATABASE db_patient;
USE db_patient;
CREATE TABLE table_patient(	
	Patient_ID INT NOT NULL,
	Name VARCHAR(30) NOT NULL,
	Gender VARCHAR(1) NOT NULL,
	Age INT NOT NULL,
	Address VARCHAR(75) NOT NULL,
	Contact_Number VARCHAR(15),
	Bloodgroup ENUM("A+","A-","B+","B-","O+","O-","AB+","AB-",""),
	PRIMARY KEY(Patient_ID)
	);

CREATE TABLE table_opd (
	Examination_ID INT,
	Patient_ID INT NOT NULL,
	Doctor_ID INT NOT NULL,
	Diagnosed_With VARCHAR(50),
	PRIMARY KEY(Examination_ID)
	);

CREATE TABLE table_opd_TO_IPD_TRANSFER_HISTORY (
	Examination_ID INT,
	Date_Of_Transfer DATE NOT NULL,
	PRIMARY KEY(Examination_ID)
	);

CREATE TABLE table_ipd (	
	Examination_ID INT NOT NULL,
	Patient_ID INT NOT NULL,
	Doctor_ID INT NOT NULL,
	Diagnosed_With VARCHAR(50),
	Date_Of_Birth Date,
	Current_Room_Number VARCHAR(5) ,
	Current_Bed_Number VARCHAR(5) ,
	Room_History_ID INT ,
	Guardian_Name VARCHAR(30) NOT NULL,
	Guardian_Contact_Number VARCHAR(15) NOT NULL,
	Discharged_On Date,
	PRIMARY KEY(Examination_ID)
	);
				


/*--------------Infrastructure database----------------------*/

CREATE DATABASE db_infrastructure;
USE db_infrastructure;

CREATE TABLE table_room(	
	Room_No VARCHAR(5) ,
	Category_ID INT NOT NULL,
	Capacity INT NOT NULL,
	Beds VARCHAR(200) NOT NULL,
	Beds_Occupied VARCHAR(100),
	Beds_Available VARCHAR(100),
	PRIMARY KEY(Room_No)
	);

CREATE TABLE table_room_category(
	Category ENUM("Deluxe","General"),
	Category_ID INT NOT NULL,
	Price FLOAT NOT NULL,
	PRIMARY KEY(Category_ID)
);

CREATE TABLE TABLE_OCCUPIED_BEDS(
	Patient_ID INT NOT NULL,
	Room_No VARCHAR(5) NOT NULL,
	Bed_No VARCHAR (3) ,
	PRIMARY KEY(Bed_No)
);


				


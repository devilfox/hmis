/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package hmis.DB.Queries;
import hmis.Engines.AppointmentSchedulerEngine.DocSchedule;
import hmis.Shared.Identifiers.fields;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Rupak
 */
 
public class Staff_Timings implements Serializable {
 public int staff_id;
 public String department;
 public int timing_id;
 public String staff_name;
 public String speciality;
 public Time sun_check_in;
 public Time sun_check_out;
 public Time mon_check_in;
 public Time mon_check_out;
 public Time tue_check_in;
 public Time tue_check_out;
 public Time wed_check_in;
 public Time wed_check_out;
 public Time thu_check_in;
 public Time thu_check_out;
 public Time fri_check_in;
 public Time fri_check_out;
 public Time sat_check_in;
 public Time sat_check_out;
 
 public Staff_Timings(String _name,String _speciality,Time sun_in,Time sun_out,Time mon_in,Time mon_out,Time tue_in,Time tue_out,Time wed_in,Time wed_out,Time thu_in,Time thu_out,Time fri_in,Time fri_out,Time sat_in,Time sat_out){
     staff_name=_name;
     speciality=_speciality;
     set_timings(sun_in,sun_out,mon_in,mon_out,tue_in,tue_out,wed_in,wed_out,thu_in,thu_out,fri_in,fri_out,sat_in,sat_out);    
 }
 public Staff_Timings(String _name,int _staff_id,Time sun_in,Time sun_out,Time mon_in,Time mon_out,Time tue_in,Time tue_out,Time wed_in,Time wed_out,Time thu_in,Time thu_out,Time fri_in,Time fri_out,Time sat_in,Time sat_out,String Dept)
 {
     staff_name=_name;
     staff_id=_staff_id;
     department=Dept;
     set_timings(sun_in,sun_out,mon_in,mon_out,tue_in,tue_out,wed_in,wed_out,thu_in,thu_out,fri_in,fri_out,sat_in,sat_out);    
 }
 
 public void set_timing_id(int id)
 {
     timing_id=id;
 }
 
 public void set_staff_id(int id)
 {
     staff_id=id;
 }
 public void set_staff_name_and_speciality(String _name,String _speciality)
 {
     staff_name=_name;
     speciality=_speciality;
 }
 
 public void set_timings(Time sun_in,Time sun_out,Time mon_in,Time mon_out,Time tue_in,Time tue_out,Time wed_in,Time wed_out,Time thu_in,Time thu_out,Time fri_in,Time fri_out,Time sat_in,Time sat_out)
 {
     sun_check_in=sun_in;
     sun_check_out=sun_out;
     mon_check_in=mon_in;
     mon_check_out=mon_out;
     tue_check_in=tue_in;
     tue_check_out=tue_out;
     wed_check_in=wed_in;
     wed_check_out=wed_out;
     thu_check_in=thu_in;
     thu_check_out=thu_out;
     fri_check_in=fri_in;
     fri_check_out=fri_out;
     sat_check_in=sat_in;
     sat_check_out=sat_out;
 }
 public String toMilitary(Time t1,Time t2){
     if(t1==null || t2==null){return "-";}
     
     String si[] = t1.toString().split(":");
     String so[]= t2.toString().split(":");
     String s = si[0]+si[1]+"-"+so[0]+so[1];
       return s;
 }
 public DocSchedule getSchedule(String adate,Integer day,Update_Appointment_Panel_Constraints_Query q){
        try {
            //public void setParams(String name, String id, String dept,Integer rep, HashMap<Integer,String> sch,HashMap<Integer,Integer> existingappointments){
            DocSchedule d = new DocSchedule();
            HashMap<Integer,String> sch = new HashMap();
            sch.put(0,this.toMilitary(sun_check_in, sun_check_out));
            sch.put(1,this.toMilitary(mon_check_in, mon_check_out));
            sch.put(2,this.toMilitary(tue_check_in, tue_check_out));
            sch.put(3,this.toMilitary(wed_check_in, wed_check_out));
            sch.put(4,this.toMilitary(thu_check_in, thu_check_out));
            sch.put(5,this.toMilitary(fri_check_in, fri_check_out));
            sch.put(6,this.toMilitary(sat_check_in, sat_check_out));
            
            
            //now extract the existing appointments here
             Calendar c = Calendar.getInstance();
             c.setTime(new SimpleDateFormat("dd/M/yyyy").parse(adate));
             SimpleDateFormat formatter=new SimpleDateFormat("yyyy-M-dd");
             
             //iterate through seven days for getting full result
             HashMap<Integer,Integer> ex = new HashMap();
             Boolean flag = false;
             for(int i=0;i<7;i++){
                 System.out.println("-------------------------------->>");
                 String s = formatter.format(c.getTime());
                 System.out.println(s+" "+day);
                try{
                    List<HashMap<String,String>> res = q.Get_Number_Of_Scheduled_Appointments(Date.valueOf(s)).get(department);
                    System.out.println("res "+department+" "+ res+" id "+staff_id);
                    for(int j=0;j<res.size();j++){
                          System.out.println(res.get(j).get(fields.doctorid)+" "+ String.valueOf(staff_id));
                     if(res.get(j).get(fields.doctorid).equalsIgnoreCase(String.valueOf(staff_id))){ 
                         System.out.println("here "+Integer.valueOf(res.get(j).get(fields.number_of_appointments)));
                         flag = true;
                        ex.put(day,Integer.valueOf(res.get(j).get(fields.number_of_appointments))) ;
                         System.out.println("details: day:"+day+" value:"+Integer.valueOf(res.get(j).get(fields.number_of_appointments)));
                         break;
                     }
                 }
                    day++;
                    if(day>6){day=0;}
                    
                }catch(NullPointerException e){
                    ex.put(day,0) ;
                    day++;
                    if(day>6){day=0;}
                }
                
                 
                 c.add(Calendar.DAY_OF_MONTH, 1);
             }
            //System.out.println("size" + ex.size());
            if(!flag){ex = null;}
            if(ex==null){
                System.out.println(staff_name+" why null here");
            }
            d.setParams(staff_name, String.valueOf(staff_id), department, 0, sch, ex);
            //System.out.println("");
            return d;
        } catch (ParseException ex) {
            Logger.getLogger(Staff_Timings.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
 }
 
}

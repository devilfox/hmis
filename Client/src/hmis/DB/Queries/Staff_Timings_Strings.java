/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.DB.Queries;

import hmis.Layer.DB.*;

/**
 *
 * @author Rupak
 */
public class Staff_Timings_Strings {
 public String name;
 public String speciality;
 public String sun;
 public String mon;
 public String tue;
 public String wed;
 public String thu;
 public String fri;
 public String sat;
 
 public Staff_Timings_Strings(String _name,String _special,String _sun,String _mon,String _tue,String _wed,String _thu,String _fri,String _sat)
 {
     name=_name;
     speciality=_special;
     sun=_sun;
     mon=_mon;
     tue=_tue;
     wed=_wed;
     thu=_thu;
     fri=_fri;
     sat=_sat;
 }

}

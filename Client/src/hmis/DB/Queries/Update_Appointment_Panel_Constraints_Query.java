 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.DB.Queries;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import hmis.Shared.Identifiers.fields;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;


/**
 *
 * @author Rupak
 */
public class Update_Appointment_Panel_Constraints_Query  implements Serializable{
  PreparedStatement pstatement=null;
    Connection connection=null;
    List<Staff_Timings> output=new ArrayList<Staff_Timings>();
    
    public Update_Appointment_Panel_Constraints_Query (Connection c){
     connection=c;   
    }
   
    public List<Staff_Timings> Retrieve_Doctor_Timings(){
        ResultSet result=null;
        try{     
        pstatement=connection.prepareStatement("Select a.Name,a.Staff_ID,b.Sun_Check_In,b.Sun_Check_Out,b.Mon_Check_in,b.Mon_Check_Out,b.Tue_Check_In,b.Tue_Check_Out,b.Wed_Check_In,b.Wed_Check_Out,b.Thu_Check_In,Thu_Check_Out,b.Fri_Check_In,b.Fri_Check_Out,b.Sat_Check_In,b.Sat_Check_Out,c.Name AS Department from db_staff.table_staff a,db_staff.table_department c INNER JOIN db_staff.table_staff_timing b where a.timing_id=b.timing_id AND c.ID=a.Department_ID AND a.Post='Doctor' ORDER BY Department;");
        result=pstatement.executeQuery();
        
        while(result.next()){
            Staff_Timings staff=new Staff_Timings(result.getString("Name"),result.getInt("Staff_ID"),result.getTime("Sun_Check_In"),result.getTime("Sun_Check_out"),result.getTime("Mon_Check_In"),result.getTime("Mon_Check_out"),result.getTime("Tue_Check_In"),result.getTime("Tue_Check_out"),result.getTime("Wed_Check_In"),result.getTime("Wed_Check_out"),result.getTime("Thu_Check_In"),result.getTime("Thu_Check_out"),result.getTime("Fri_Check_In"),result.getTime("Fri_Check_out"),result.getTime("Sat_Check_In"),result.getTime("Sat_Check_Out"),result.getString("Department"));
            output.add(staff);
        }
        
    }catch(SQLException e)
    {
        e.printStackTrace();
    }
        return output;   
    }
public HashMap<String,Vector<HashMap<String,String>>> Get_Available_Doctors()
    {
        ResultSet results;
        HashMap<String,Vector<HashMap<String,String>>> output=new HashMap();
        try{
            pstatement=connection.prepareStatement("SELECT b.name,b.Staff_ID,a.name as 'Dept_Name' FROM db_staff.table_department a,db_staff.table_staff b where b.department_id=a.id AND b.post='Doctor' ORDER BY a.name;");
            results=pstatement.executeQuery();
  
            String prev_dept="";
            String cur_dept=null;
            
            while(results.next())
            {
                HashMap<String,String> single_info=new HashMap();
                Vector<HashMap<String,String>> same_dept_info=new Vector();
                
                cur_dept=results.getString("Dept_Name");
                
                single_info.put(fields.doctorid,String.valueOf(results.getInt("Staff_ID")));
                single_info.put(fields.doctor,results.getString("Name"));
                
                
                if(cur_dept.equals(prev_dept))
                {
                    same_dept_info.removeAllElements();
                    output.get(cur_dept).add(single_info);
                    
                }
                else{
                    same_dept_info.add(single_info);
                    output.put(cur_dept, same_dept_info);
                    
                }
                prev_dept=cur_dept;
            }
            
            }   
        catch(SQLException e)
            {
            e.printStackTrace();
            }
        
        return output;
    }
public HashMap<String,List<HashMap<String,String>>> Get_Number_Of_Scheduled_Appointments(Date _date_of_appointment)
    {
        List<HashMap<String,String>> output=new ArrayList();
        List<HashMap<String,String>> dpart_doc_info=new ArrayList();
        HashMap<String,List<HashMap<String,String>>> arranged_output=new HashMap(); 
        ResultSet result=null;
        try{
            pstatement=connection.prepareStatement("SELECT COUNT(*),Doctor_ID,Appointment_Date FROM( SELECT a.Doctor_ID, a.Appointment_Date FROM db_patient.table_appointment a WHERE a.Appointment_Date=? UNION ALL SELECT b.Doctor_ID,b.Date_Of_First_Examination FROM db_patient.table_opd b WHERE b.Date_Of_First_Examination=?) AS Total  GROUP BY Doctor_ID ORDER BY Doctor_ID ;");
            //Date d=Date.valueOf("2014-03-13");
            pstatement.setDate(1,_date_of_appointment);
            pstatement.setDate(2,_date_of_appointment);
            result=pstatement.executeQuery();
           
            while(result.next()){
            HashMap<String,String> single_doc_appt_info=new HashMap();
            single_doc_appt_info.put(fields.number_of_appointments,String.valueOf(result.getInt("Count(*)")));
            single_doc_appt_info.put(fields.doctorid, String.valueOf(result.getString("Doctor_ID")));
            single_doc_appt_info.put(fields.appointment_date,result.getDate("Appointment_Date").toString());
           // single_doc_appt_info.put(fields.department, result.getString("Department"));
            output.add(single_doc_appt_info);
            }
            pstatement=connection.prepareStatement("SELECT a.Staff_ID,b.Name AS Department FROM db_staff.table_staff a,db_staff.table_department b WHERE a.Department_ID=b.ID;");
            result=pstatement.executeQuery();
            while(result.next())
            {
                HashMap<String,String> single_info=new HashMap();
                single_info.put(fields.doctorid,result.getString("Staff_ID"));
                single_info.put(fields.department, result.getString("Department"));
                dpart_doc_info.add(single_info);
            }
            for(int i=0;i<output.size();i++)
            {
                for(int j=0;j<dpart_doc_info.size();j++)
                {
                    if(output.get(i).get(fields.doctorid).equals(dpart_doc_info.get(j).get(fields.doctorid)))
                    {
                        output.get(i).put(fields.department,dpart_doc_info.get(j).get(fields.department));
                    }
                }
            }
            
            
            for(int i=0;i<output.size();i++)
            {
                String key=output.get(i).get(fields.department);
                if(arranged_output.containsKey(key))
                {
                    arranged_output.get(key).add(output.get(i));
                }
                else{
                    arranged_output.put(key,new ArrayList());
                    arranged_output.get(key).add(output.get(i));        
                }
            }
        }catch(SQLException e)
        {
            e.printStackTrace();
            return null;
        }
        System.out.println(""+arranged_output);
       return arranged_output; 
    }
    
}



/**
 *
 * @author Rupak
 */

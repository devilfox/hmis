/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hmis.DB.lab;

import hmis.Shared.Identifiers.fields;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Rupesh
 */
public class LabPatient implements Serializable{
    
    public String reg_no;
    public String name;
    public String age;
    public String sex;
    public String address;
    public String contact;
    public List<LabTest> tests;
    
    public LabPatient(){
        
    }
            
    public LabPatient(HashMap<String,String> info,List<LabTest> _tests)
    {
        Set_Fields(info, tests);
    }
    
    public void Set_Fields(HashMap<String,String> info,List<LabTest> _tests)
    {
        reg_no=info.get(fields.lab_registration_no);
        name=info.get(fields.name);
        age=info.get(fields.age);
        sex=info.get(fields.sex);
        address=info.get(fields.address);
        contact=info.get(fields.contact);
        tests=_tests;
    }
}


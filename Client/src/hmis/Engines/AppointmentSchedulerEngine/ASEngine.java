/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

 * Though this is main scheduling engine, another object must maintain the schedule to the format asked by this class, so this will not be called directly
 */

package hmis.Engines.AppointmentSchedulerEngine;

import hmis.Layer.Time.Time;
import java.io.Serializable;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 *
 * @author root
 * This engine will help automating the appointment scheduling process
 */
public class ASEngine implements Serializable{
        public class Appointment implements Serializable{
            //this object contains the doctor selected and day selected
            public DocSchedule doc;
            public int day;
        }
    //this contains max time alloted for one interview
    private Integer AppointmentTimeWt = new Integer(15); 
    // this sets the value 0-6 representing days of the week(the day at which this object is beubg called)
    private Integer todayis = new Integer(1);           
    //this will contatins all doctors details
    private Vector<DocSchedule> DocDetails = new Vector();
    
    

    private int[][] prepareAvavlityList(){
        //this will create 2d list (doc index vs days) with value 1|0 representing avavlity of doc in that day or not
        int[][] avail = new int[DocDetails.size()][7];
                
        for(int i=0; i<DocDetails.size();i++){
            for(int j=0;j<7;j++){
                if(!DocDetails.get(i).getScheduleByDay(j).equalsIgnoreCase("-")) avail[i][j] = 1;
            }
        }
        return avail;
    }
    
    private int[][] prepareAvavlityWtList(int[][] avails){
        //returns a 2d list (doc index vs day), each value will represent the minutes the day the doc will be available
        int[][] wt = new int[avails.length][7];
        for(int i=0;i<avails.length;i++){
            for(int j=0;j<7;j++){
                if(avails[i][j]==1){
                    String shift = DocDetails.get(i).getScheduleByDay(j);
                    Pattern r = Pattern.compile("\\b([0-9]{1,2}+)([0-9]{1,2}+)-([0-9]{1,2}+)([0-9]{1,2}+)\\b");
                    Matcher m = r.matcher(shift);
                    if(m.find()){
                        Integer FH = Integer.valueOf(m.group(1)); Integer FM =Integer.valueOf(m.group(2));
                        Integer TH = Integer.valueOf(m.group(3)); Integer TM = Integer.valueOf(m.group(4));
                        Integer gap;
                        Integer F = FH * 60 + FM; Integer T = TH * 60 + TM;
                        if(T>F){
                            gap = T-F;
                        }else{
                            gap = 24*60 -F + T;
                        }
                        wt[i][j] = gap;
                    }
                }
            }
        }
        return wt;
    }
    
    private char[][] prepareTimeNotationList(int[][] avails){
        //avails in this case is the returns of prepareAvavlityWtList()
        //keywords:
        // F = full
        // O = Overflow
        // U = Underflow
        // - = NA
        char[][] wt = new char[avails.length][7];
        for(int i=0;i<avails.length;i++){
            for(int j=0;j<7;j++){
                if(avails[i][j]!=0){
                    char tag;
                    Integer booked_time = (DocDetails.get(i).getNoOfAppointmentsOnDay(j)) * AppointmentTimeWt;
                   if(booked_time < avails[i][j]){
                       tag = 'U';
                   }
                   else if(booked_time > avails[i][j]){
                      tag = 'O';
                    }
                   else{
                       tag = 'F';
                   }
                   wt[i][j] = tag;
                }else{
                    wt[i][j] = '-';
                }
                
            }
        }
        return wt;
    }
    
    private int[][] prepareTimeList(int[][] avails){
        int[][] wt = new int[avails.length][7];
        for(int i=0;i<avails.length;i++){
            for(int j=0;j<7;j++){
                if(avails[i][j]!=0){
                    Integer booked_time = (DocDetails.get(i).getNoOfAppointmentsOnDay(j)) * AppointmentTimeWt;
                    wt[i][j] = avails[i][j] - booked_time;
                }else{
                    //to distinguish from full, we'll set all other integer to 1, because 1 will be invalid value here
                    wt[i][j] = 1;
                }
            }
        }
        return wt;
    }
    
    private int[][] createLoadList(int[][] avails_wt){
        //will create the percentage load on each doctor busy day
        int[][] wt = new int[avails_wt.length][7];
        for(int i=0;i<avails_wt.length;i++){
            for(int j=0;j<7;j++){
                if(avails_wt[i][j]!=0){
                    Integer booked_time = (DocDetails.get(i).getNoOfAppointmentsOnDay(j)) * AppointmentTimeWt;
                    int pc =0;
                    if(avails_wt[i][j]<booked_time) pc =100;
                    else{
                        float d1 = Math.abs((float)(booked_time));
                        d1 = d1/(float)(avails_wt[i][j])*100;
                        pc = (int)d1;
                    }
                    wt[i][j] = pc;
                }else{
                    wt[i][j] = -1;
                }
            }
        }
        return wt;
    }
    
    
    private Appointment getFeasibleAppointment(char[][] lst,int day){
        //checks if there is any appointment possible of the given day(if not will check for other following days) in provided timeNotation list
        for(int i=day;i<7;i++){
           int d =  isThisDayFeasible(lst,i);
           if(d!=-1){
               Appointment f = new Appointment();
               f.day = i;
               f.doc = DocDetails.get(d);
               return f;
           }
           
        }
        for(int i=0;i<day;i++){
            int d =  isThisDayFeasible(lst,day);
           if(d!=-1){
               Appointment f = new Appointment();
               f.day = i;
               f.doc = DocDetails.get(d);
               return f;
        }
           
     }
        return null;
    }
    
    private int isThisDayFeasible(char[][] lst,Integer day){
        //this will take input from prepareTimeNotationList()
        //this will check if the appointment is possible in the day given on the given list
        for(int i=0;i<lst.length;i++){
            
            if(lst[i][day]=='U') return i;
        }
        return -1;
    }
    
    
    
  //public  
    //import doc details one by one
    public void importDocSchedule(DocSchedule sch){DocDetails.addElement(sch);}
    //set appointment weight per appointment
    public void setAppointmentTimeWt(Integer w){AppointmentTimeWt=w;}
    //set todays day integer (sun=0,mon=1...)
    public void TodayIs(Integer t){todayis = t;}
    
    
    public Appointment getFeasibleAppointment(Integer day){
        //1. this will get the feasible day of DocDetailss based on normal method
        //2. this method will skip all the doctors creditabiltiy and just checks for underflowed positions
    /*
        This is based on the normal method that it checks for Underflowed timings of the DocDetailsd lists until it gets one
        If the match wasnt found on the day asked, it will search for next most feasible date
        If we want to implemetn getnextFeasibleAppointment, just input the day vaiable of the current output to this same method and get the next 
        feasible appointment
    */
        //will use date to get the most feasible option
        
        Appointment f = getFeasibleAppointment (prepareTimeNotationList(prepareAvavlityWtList(prepareAvavlityList())),day);
        return f;
    }    
   
    public Appointment getprobableScheduleByDoc(String docid,int day){
        //takes doctor id and todays' day to integer
        //will check if any doctor has underflowed appointment time, and hence will return it
        //it will check and make a round trip to all posible days, so in case of different weekly DocDetails, returned date will be used to detect error in
        //scheduling
        char[][] lst = prepareTimeNotationList(prepareAvavlityWtList(prepareAvavlityList()));
        for(int i=0;i<lst.length;i++){
            for(int j=day;j<7;j++){
                if(DocDetails.get(i).getID().equalsIgnoreCase(docid) && lst[i][j]=='U'){
                    Appointment a = new Appointment();
                    a.day = j;
                    a.doc = DocDetails.get(i);
                    return a;
                }
            }
        }
        for(int i=0;i<lst.length;i++){
            for(int j=0;j<day;j++){
                if(DocDetails.get(i).getID().equalsIgnoreCase(docid) && lst[i][j]=='U'){
                    Appointment a = new Appointment();
                    a.day = j;
                    a.doc = DocDetails.get(i);
                    return a;
                }
            }
        }
        return null;
    }
}
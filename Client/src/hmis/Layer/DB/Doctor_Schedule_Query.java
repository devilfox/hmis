/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Layer.DB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rupak
 */
public class Doctor_Schedule_Query {
  PreparedStatement pstatement=null;
    Connection connection=null;
    List<Staff_Timings> output=new ArrayList<Staff_Timings>();
    
    public Doctor_Schedule_Query(Connection c){
     connection=c;   
    }
   
    public List<Staff_Timings> Retrieve_Doctor_Timings(){
        ResultSet result=null;
        try{     
        pstatement=connection.prepareStatement("Select a.Name,a.Specialist,b.Sun_Check_In,b.Sun_Check_Out,b.Mon_Check_in,b.Mon_Check_Out,b.Tue_Check_In,b.Tue_Check_Out,b.Wed_Check_In,b.Wed_Check_Out,b.Thu_Check_In,Thu_Check_Out,b.Fri_Check_In,b.Fri_Check_Out,b.Sat_Check_In,b.Sat_Check_Out from db_staff.table_staff a INNER JOIN db_staff.table_staff_timing b where a.timing_id=b.timing_id;");
        result=pstatement.executeQuery();
        
        while(result.next()){
            Staff_Timings staff=new Staff_Timings(result.getString("Name"),result.getString("Specialist"),result.getTime("Sun_Check_In"),result.getTime("Sun_Check_out"),result.getTime("Mon_Check_In"),result.getTime("Mon_Check_out"),result.getTime("Tue_Check_In"),result.getTime("Tue_Check_out"),result.getTime("Wed_Check_In"),result.getTime("Wed_Check_out"),result.getTime("Thu_Check_In"),result.getTime("Thu_Check_out"),result.getTime("Fri_Check_In"),result.getTime("Fri_Check_out"),result.getTime("Sat_Check_In"),result.getTime("Sat_Check_Out"));
            output.add(staff);
        }
        
    }catch(SQLException e)
    {
        e.printStackTrace();
    }
        return output;   
    }
}

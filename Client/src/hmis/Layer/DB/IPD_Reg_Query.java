/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Layer.DB;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rupak
 */
public class IPD_Reg_Query {
    private PreparedStatement pstatement1=null;
    private PreparedStatement pstatement2=null;
    private Connection connection=null;
    private int patient_id;
    
    public IPD_Reg_Query(Connection c)
    {
        connection=c;
    }
    
    public Old_patient Get_details_using_exam_id(int exam_id)
    {
        ResultSet result=null;
        Old_patient patient=null;
        try{
        pstatement1=connection.prepareStatement("SELECT a.Patient_ID, a.Name, a.gender, a.Address, a.Contact_Number, a.Bloodgroup from db_patient.table_patient a, db_patient.table_opd b WHERE a.Patient_ID=b.Patient_ID and b.Examination_ID=?;");
        pstatement1.setInt(1,exam_id);
        result=pstatement1.executeQuery();
        while(result.next()){
             patient=new Old_patient(result.getString("Name"),result.getString("Address"),result.getString("Gender"),result.getString("Contact_Number"),Integer.parseInt(result.getString("Patient_ID")));
        }
        }
        
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        return patient;        
    }
    
    public void Update_Details(Old_patient patient){
        PreparedStatement pstatement=null;
        try{
            pstatement=connection.prepareStatement("UPDATE db_patient.table_patient SET Age=?, Address=?, Contact_Number=? ,Bloodgroup=? WHERE PATIENT_ID=?");
            pstatement.setInt(1, patient.age);
            pstatement.setString(2, patient.address);
            pstatement.setString(3, patient.contact_number);
            pstatement.setString(4, patient.bloodgroup);
            pstatement.setInt(5, patient.patient_id);
            pstatement.executeUpdate();
        }catch(SQLException e)
        {
            System.out.println("error in update");
            e.printStackTrace();
        }
        
        try {
            pstatement.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    public void Insert_IPD_Data(IPD_Reg_Query_Details details,Old_patient previous_details)
    {
        
        try{
            pstatement2=connection.prepareStatement("INSERT INTO db_patient.table_ipd (Examination_ID,Patient_ID,Doctor_ID,Diagnosed_With,Date_Of_Birth,Current_Room_Number,Current_Bed_Number,Guardian_Name,Guardian_Contact_Number) VALUES (?,?,?,?,?,?,?,?,?);INSERT INTO db_patient.table_opd_TO_IPD_TRANSFER_HISTORY (Examination_ID,Date_Of_Transfer) VALUES (?,?);");
            pstatement2.setInt(1, details.examination_id);
            pstatement2.setInt(2, details.patient_details.patient_id);
            pstatement2.setInt(3, details.doc_id);
            pstatement2.setString(4, details.diagnosed_with);
            pstatement2.setDate(5,details.date_of_birth);
            pstatement2.setString(6,details.room_details.room_no);
            pstatement2.setString(7,details.room_details.bed_no);
            pstatement2.setString(8,details.guardian_name);
            pstatement2.setString(9,details.guardian_contact);
            pstatement2.setInt(10, details.examination_id);
            pstatement2.setDate(11, Date.valueOf("2020-10-20"));  //current date is to be inserted
            
            pstatement2.executeUpdate();
            if(!previous_details.equals(details.patient_details))
            {
                Update_Details(details.patient_details);
            }
        }catch(SQLException e)
        {
            System.out.println("error in insertion");
            e.printStackTrace();
        }
        
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Layer.DB;

import java.sql.Date;



/**
 *
 * @author Rupak
 */
public class IPD_Reg_Query_Details {
 public int examination_id;
 public String diagnosed_with;
 public String marital_status;
 
 public String guardian_name;
 public String guardian_contact;

 public String depart_name;
 public int  doc_id;
 public Date date_of_birth;
 
 public Old_patient patient_details;
 public Room_Info room_details; 
  
 public IPD_Reg_Query_Details(int _exam_id,Old_patient _patient_details,String _diagnosed_with,String _marital_status,String _guardian_name,String _guardian_contact,String _depart_name,int _doc_id,Date _dob,Room_Info _room){
     examination_id=_exam_id;
     patient_details=_patient_details;
     diagnosed_with=_diagnosed_with;
     marital_status=_marital_status;
     guardian_name=_guardian_name;
     guardian_contact=_guardian_contact;
     depart_name=_depart_name;
     doc_id=_doc_id;
     date_of_birth=_dob;
     room_details=_room;
 }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Layer.DB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Rupak
 */

public class OPD_Reg_Query {
    PreparedStatement pstatement=null;
    Connection connection=null;
    Tbl_patient_data patient;
    
    public OPD_Reg_Query(Connection c,Tbl_patient_data pat){
        connection =c;
        patient=pat;
    }
    
    public void Insert_Details(){
        try{
            pstatement=connection.prepareStatement("INSERT INTO db_patient.table_patient (Patient_ID,Name,Gender,Age,Address,Contact_Number,Bloodgroup) VALUES (?,?,?,?,?,?,?);INSERT INTO db_patient.table_opd (Examination_id,Patient_ID,Doctor_ID,Diagnosed_With,Date_Of_First_Examination) VALUES(?,?,?,?,?);");
            pstatement.setInt(1, patient.patient_id);
            pstatement.setString(2, patient.name);
            pstatement.setString(3, patient.gender);
            pstatement.setInt(4, patient.age);
            pstatement.setString(5, patient.address);
            pstatement.setString(6,patient.contact_number);
            pstatement.setString(7, patient.bloodgroup);
            pstatement.setInt(8, patient.exam_id);
            pstatement.setInt(9, patient.patient_id);
            pstatement.setInt(10, patient.doc_id);
            pstatement.setString(11, patient.diagnosed_with);
            pstatement.setDate(12, patient.date_of_first_examination);
            pstatement.executeUpdate();
        }catch(SQLException e)
        {
            e.printStackTrace();
        }
        
    }
    
    public void Update_Details(){
        try{
            pstatement=connection.prepareStatement("UPDATE  table_patient SET Age=?,Address=?,Contact_Number=? WHERE Patient_ID=?;INSERT INTO table_opd (Examination_id,Patient_ID,Doctor_ID,Diagnosed_With) VALUES(?,?,?,?);");
            pstatement.setInt(1, patient.age);
            pstatement.setString(2, patient.address);
            pstatement.setString(3,patient.contact_number);
            pstatement.setInt(4, patient.patient_id);
            pstatement.setInt(5, patient.exam_id);
            pstatement.setInt(6, patient.patient_id);
            pstatement.setInt(7, patient.doc_id);
            pstatement.setString(8, patient.diagnosed_with);
            pstatement.executeUpdate();
        }catch(SQLException e)
        {
            e.printStackTrace();
        }
        
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Layer.DB;

/**
 *
 * @author Rupak
 */

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class Old_Patient_Query {
    PreparedStatement pstatement=null;
    Connection connection=null;
    List<Old_patient> output=new ArrayList<Old_patient>();
    public Old_Patient_Query(Connection c){
     connection=c;   
    }

    
public List<Old_patient> Search_patient(String available_info,char type){
    ResultSet result=null;
    try{
        if(type=='n'){
        pstatement=connection.prepareStatement("Select Patient_ID,Name,Address,Contact_Number,Gender FROM table_patient WHERE name=?");
        pstatement.setString(1,available_info);
        }
        else if(type=='i')
        {
        pstatement=connection.prepareStatement("Select Patient_ID,Name,Address,Contact_Number,Gender FROM table_patient WHERE Patient_ID=?");
        pstatement.setInt(1,Integer.parseInt(available_info));
        }
        result=pstatement.executeQuery();
        
        while(result.next()){
            Old_patient p=new Old_patient(result.getString("Name"),result.getString("Address"),result.getString("Gender"),result.getString("Contact_Number"),Integer.parseInt(result.getString("Patient_ID")));
            output.add(p);
        }
        
    }catch(SQLException e)
    {
        e.printStackTrace();
    }
        return output;   
}

    
}

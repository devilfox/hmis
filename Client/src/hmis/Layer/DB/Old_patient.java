/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Layer.DB;

/**
 *
 * @author Rupak
 */
//The Object of this class is used for holding the results Obtained when old_patient query is executed.
//Each Object created is then appended to a LIST<Old_patient> and this list is returned to the GUI client.

public class Old_patient {
    public  String name;
    public  String address;
    public  String contact_number;
    public  int patient_id;
    public String gender;
    public int age;        //this field is rarely used since age is dynamic..one of its use is in IPD_REG_Query
    public String bloodgroup;
    
    public Old_patient(String _name,String _address,String _gender,String _contact_number,int _patient_id){
        name=_name;
        address=_address;
        contact_number=_contact_number;
        patient_id=_patient_id;
        gender=_gender;
    }
    
   public void Set_Age(int _age)
   {
       age=_age;
   }
   public void Set_Bloodgroup(String _bloodgroup)
   {
       bloodgroup=_bloodgroup;
   }
}

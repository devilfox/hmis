/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Layer.DB;

/**
 *
 * @author Rupak
 */
public class Room_Info {
    public int patient_id;
    public String room_type;
    public String room_no;
    public String bed_no;
 
public Room_Info(String _room_type,String _room_no,String _bed_no)
{
    room_no=_room_no;
    room_type=_room_type;
    bed_no=_bed_no;
}

public Room_Info(int _patient_id,String _room_type,String _room_no,String _bed_no)
{
    patient_id=_patient_id;
    room_no=_room_no;
    room_type=_room_type;
    bed_no=_bed_no;
}


}

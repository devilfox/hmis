/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package hmis.Layer.DB;

import java.io.Serializable;
import java.sql.Time;


/**
 *
 * @author Rupak
 */
 
public class Staff_Timings implements Serializable {
 public int staff_id;
 public int timing_id;
 public String staff_name;
 public String speciality;
 public Time sun_check_in;
 public Time sun_check_out;
 public Time mon_check_in;
 public Time mon_check_out;
 public Time tue_check_in;
 public Time tue_check_out;
 public Time wed_check_in;
 public Time wed_check_out;
 public Time thu_check_in;
 public Time thu_check_out;
 public Time fri_check_in;
 public Time fri_check_out;
 public Time sat_check_in;
 public Time sat_check_out;
 
 public Staff_Timings(String _name,String _speciality,Time sun_in,Time sun_out,Time mon_in,Time mon_out,Time tue_in,Time tue_out,Time wed_in,Time wed_out,Time thu_in,Time thu_out,Time fri_in,Time fri_out,Time sat_in,Time sat_out){
     staff_name=_name;
     speciality=_speciality;
     set_timings(sun_in,sun_out,mon_in,mon_out,tue_in,tue_out,wed_in,wed_out,thu_in,thu_out,fri_in,fri_out,sat_in,sat_out);    
 }
 
 public void set_timing_id(int id)
 {
     timing_id=id;
 }
 
 public void set_staff_id(int id)
 {
     staff_id=id;
 }
 public void set_staff_name_and_speciality(String _name,String _speciality)
 {
     staff_name=_name;
     speciality=_speciality;
 }
 
 public void set_timings(Time sun_in,Time sun_out,Time mon_in,Time mon_out,Time tue_in,Time tue_out,Time wed_in,Time wed_out,Time thu_in,Time thu_out,Time fri_in,Time fri_out,Time sat_in,Time sat_out)
 {
     sun_check_in=sun_in;
     sun_check_out=sun_out;
     mon_check_in=mon_in;
     mon_check_out=mon_out;
     tue_check_in=tue_in;
     tue_check_out=tue_out;
     wed_check_in=wed_in;
     wed_check_out=wed_out;
     thu_check_in=thu_in;
     thu_check_out=thu_out;
     fri_check_in=fri_in;
     fri_check_out=fri_out;
     sat_check_in=sat_in;
     sat_check_out=sat_out;
 }
 
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Layer.DB;

import java.sql.Date;

/**
 *
 * @author Rupak
 */
//This class is for holding the data to be input or selected from table_patient or OPD registration query 

public class Tbl_patient_data {
    String name;
    String address;
    String contact_number;
    String bloodgroup;
    String gender;
    int patient_id;
    int age;
    int exam_id;
    int doc_id;
    String diagnosed_with;
    Date date_of_first_examination; 
    //this constructor is for constructing objects when all details except patient_id and exam_id are known
    public Tbl_patient_data(String _name, String _address, String _contact_number, String _bloodgroup, String _gender, int _doc_id, int _age,String _diagnosed_with,Date _date_of_first_examination){
    name=_name;
    address=_address;
    contact_number=_contact_number;
    bloodgroup=_bloodgroup;
    gender=_gender;
    age=_age;
    diagnosed_with=_diagnosed_with;
    doc_id=_doc_id;
    date_of_first_examination=_date_of_first_examination;
    }
    
       //this constructor is for constructing objects when all details are known
    public Tbl_patient_data(String _name, String _address, String _contact_number, String _bloodgroup, String _gender, int _doc_id, int _age,String _diagnosed_with, int _patient_id,int _exam_id,Date _date_of_first_examination){
    name=_name;
    address=_address;
    contact_number=_contact_number;
    bloodgroup=_bloodgroup;
    gender=_gender;
    age=_age;
    diagnosed_with=_diagnosed_with;
    doc_id=_doc_id;
    date_of_first_examination=_date_of_first_examination;
    Set_pat_exam_id(_patient_id,_exam_id);
    }
    
    public void Set_pat_exam_id(int _patient_id, int _exam_id){
        patient_id=_patient_id;
        exam_id=_exam_id;  
    }
}

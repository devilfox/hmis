/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Layer.Time;
import hmis.Shared.Exception.GlobalException; 
import java.util.Calendar; 
/**
 *
 * @author daredevils
 */
public class Time {
    Calendar cal;
    
    private final String[] months = {"JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"};
    private final String[] days = {"SUN","MON","TUE","WED","THU","FRI","SAT"};
    private final String[] AMPM = {"AM","PM"};
    
    private int Year;
    private int Month;  private String month;
    private int Day;    private String day;
    private int Hour;
    private int Minute;
    private int Second;
    private int AM_PM;  private String ampm;
    
    public String tostrday(int day){
        if(day<7&&day>=0){
            return days[day];
        }
        return " ";
    }
    public String tostrmonth(int month){
        if(month>=1 && month<13){
            return months[month];
        }
        return " ";
    }
    
    public Time(){
        //This will insert time to the object in time of its creation
        cal = Calendar.getInstance();
        Year = cal.get(Calendar.YEAR);
        Month = cal.get(Calendar.MONTH);    month = months[Month];
        Day = cal.get(Calendar.DAY_OF_MONTH); day = days[cal.get(Calendar.DAY_OF_WEEK)-1];
        Hour = cal.get(Calendar.HOUR);
        Minute = cal.get(Calendar.MINUTE);
        Second = cal.get(Calendar.SECOND);
        AM_PM = cal.get(Calendar.AM_PM);    ampm = AMPM[AM_PM];
    }
    public void refresh(){
        //this will refresh this object to represent the refreshed instance time
        cal = Calendar.getInstance();
        Year = cal.get(Calendar.YEAR);
        Month = cal.get(Calendar.MONTH);    month = months[Month];
        Day = cal.get(Calendar.DAY_OF_MONTH); day = days[cal.get(Calendar.DAY_OF_WEEK)-1];
        Hour = cal.get(Calendar.HOUR);
        Minute = cal.get(Calendar.MINUTE);
        Second = cal.get(Calendar.SECOND);
        AM_PM = cal.get(Calendar.AM_PM);    ampm = AMPM[AM_PM];
    }
    
    public String getTimeForDB(){
        //This will format the time to save into database in format [Year month day Hour Minute Second ampm]
        return String.format("%d-%d-%d-%s-%d-%d-%d-%s",Year,Month,Day,day,Hour,Minute,Second,ampm);
    }
    
    public void setTimeFromDB(String data) throws GlobalException{
        //This will take in time as format: [Year month day Hour Minute Second ampm] and parse them to variables for use
        String elements[] = data.split(" ");
        if(elements.length<7 || elements.length>7){
            throw new GlobalException("","error parsing database date",null);
        }
        Year = Integer.parseInt(elements[0]);
        month = elements[1];
        day = elements[2];
        Hour = Integer.parseInt(elements[3]);
        Minute = Integer.parseInt(elements[4]);
        Second = Integer.parseInt(elements[5]);
        ampm = elements[6];
        
        Month = -1;
        for(int i=0;i<months.length;i++){
            if(months[i].equalsIgnoreCase(month)){
                Month = i;
                break;
            }
        }
        if(Month == -1){
            throw new GlobalException("1","error parsing database date",null);
        }
        
        Day = -1;
        for(int i=0;i<days.length;i++){
            if(days[i].equalsIgnoreCase(day)){
                Day = i+1;
                break;
            }
        }
        if(Day == -1){
            throw new GlobalException("2","error parsing database date",null);
        }
        
        AM_PM = -1;
        for(int i=0;i<AMPM.length;i++){
            if(AMPM[i].equalsIgnoreCase(ampm)){
                AM_PM = i;
                break;
            }
        }
        if(AM_PM == -1){
            throw new GlobalException("3","error parsing database date",null);
        }
   }
    
    public String getYear(){return Integer.toString(Year);}
    public String getMonth(){return month;}
    public String getDay(){return day;}
    public Integer getiDay(){return Day;}
    public String getHour(){return Integer.toString(Hour);}
    public String getMinute(){return Integer.toString(Minute);}
    public String getSecond(){return Integer.toString(Second);}
    public String getMeridiem(){return ampm;}
    
    public String getcurrenttime(){
        this.refresh();
        return this.getTimeForDB();
    }
}

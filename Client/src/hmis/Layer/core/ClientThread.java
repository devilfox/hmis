/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Layer.core;

import hmis.Shared.Commobjects.primitive.Auth_Response;
import hmis.Shared.Commobjects.primitive.ReceptionModuleUpdates;
import hmis.Shared.Commobjects.primitive._rr;
import hmis.Shared.Commobjects.standard.query;
import hmis.Shared.Identifiers.codes;
import hmis.Shared.sharedresources.rshared;
import hmis.gui.base._rbase;
import hmis.gui.main.LoginFrame;
import javax.swing.JOptionPane;


/**
 *
 * @author daredevils
 */
public class ClientThread implements Runnable{
    private Thread thread;
    
    public ClientThread(String identifier){
            thread = new Thread(this,identifier);
            
            //create the instance of listner each time it is created for globalization
            rshared.listner = this;
    }
    @Override
    public void run(){
        try{
            while(true){
                try{
                    //check if the server actually exists
                    if(rshared.server == null){
                        throw new GlobalException("server variable error",null,null);
                    }
                    //receive from the server
                    _rr response = rshared.server.readObj();               
                    throw new GlobalException(response.getid(),thread.getName()+" "+"Getting response",response);
                } catch (GlobalException ex){
                    System.out.println(ex.getExceptionCode());
                    //now get down to resolve the actions for what we have a reply from server
                    System.out.println("Here->"+ex.getExceptionCode());
                    switch (ex.getExceptionCode()){
                        
                        
                        case codes.AuthenticationRequest:{
                            Auth_Response response = (Auth_Response)ex.getObject();
                            if(response.isSuccess){
                                //this checks if the query has been executed sucessfully and returned some goodies
                                //use logic of what to do when this case has a return response with parameters
                                
                                //At this time, the the caller the core is definately the loginframe, hencing using rshared var
                                //and loading the home screen and piping the login information to it
                                rshared.clientdepartment = response.deptid;
                                ((LoginFrame)rshared.recentcaller._object).load_home_screen(response.deptid,response.username,response.clientid);
                            }else{
                                //If the authentication fails
                                //Server will instantly close the connection on void authentication, then we'll have to
                                //handle that situation here
                                //close the current login frame
                                rshared.login.dispose();
                                //show the failed message, resolve the correct reason here
                                if(!response.isMultipleInstance){ //credentials error
                                    JOptionPane.showMessageDialog(null,"Login failed! Please check your login credentials! :(");
                                }else{//multiple login error
                                    JOptionPane.showMessageDialog(null,"Login failed! Already logged in! :(");
                                }
                               //create a new connection to the server
                                rshared.server = new Server();
                                //create new handler thread
                                rshared.listner = new ClientThread("listner");
                                rshared.listner.execute();
                                //create new login frame
                                rshared.login = new LoginFrame(350,280);
                                rshared.login.setVisible(true);
                                //now quit the current thread by throwing any exception
                                throw ex;
                            }
                            
                            break;
                        }
                            
                        case codes.updatemodule:{
                                        ReceptionModuleUpdates update = (ReceptionModuleUpdates)ex.getObject();     
                                        //check if we are expecting this update
                                        if(this.isexpected(update.updatefor)){                 
                                            //Hence, the updates for the receptionpanel will be piped here
                                             ((_rbase)rshared.recentcaller._object).pipeupdate(update);
                                        }
                                        break;
                        }
                            //if the reply relates to data input
                        case codes.datainput:{
                                            //get the query object
                                    query reply = (query)ex.getObject();
                                    //see if the reply is expected
                                    if(this.isexpected(reply.section)){
                                        //Now pipe reply to the respective panel
                                        //This should also notify panel to come out of wait state (if it is)
                                        ((_rbase)rshared.recentcaller._object).pipequeryreply(reply);
                                    }
                                    break;
                        }
                        
                        
                        //follow the cases using the register and codes static class values and use the 
                        //if and else logic as of the above case
                            
                        default:{
                            
                            if(rshared.isloggedout==null){//if not set means , not yet logged out
                                if(rshared.login!=null){
                                    rshared.login.dispose();
                                }
                                if(rshared.home!=null){
                                    rshared.home.dispose();
                                }
                                JOptionPane.showMessageDialog(null,"server went down! :(");
                            }
                            rshared.clear();
                            throw ex;
                        }
                    }
                }
                
            }
        }catch (GlobalException e){          
        }
        
    }
    private Boolean isexpected(String val){
        if(rshared.recentcaller == null){return false;}
        return rshared.recentcaller._name.equalsIgnoreCase(val);
    }
    public void execute(){
        thread.start();
    }
}

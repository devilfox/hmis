/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Layer.core;

/**
 *
 * @author daredevils
 */
public class GlobalException extends Exception {
    private String exceptioncode;                                                       //code will be used to define each exception conditions
    private String Description = "No description available";                            //Description is provided to the exception conditions
    private Object obj;
    
    public GlobalException(String exception_code, String description,Object Obj){
        exceptioncode = exception_code;
        if(!description.isEmpty()){
            Description = description;
        }
        obj = Obj;
    }
    
    public String getExceptionCode(){                                           //Returns exceptioncode on demand
        return exceptioncode;
    }
    
    public String getExceptionDesc(){                                            //Return exceptiondescription on demand
        return Description;
    }
    
    public Object getObject(){
        return obj;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Layer.core;
import hmis.Shared.Commobjects.primitive._rr;
import hmis.Shared.Commobjects.primitive._rr;
import hmis.Layer.DB.servervars;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.*;
import hmis.Shared.sharedresources.Do;
import hmis.Shared.sharedresources.rshared;

/**
 *
 * @author daredevils
 */
public class Server {
 private String MainServer;
 private int Port;
    private Socket connection;
    DataOutputStream OUT;
    DataInputStream IN;
    ObjectInputStream OIS;
    ObjectOutputStream OOS;
 
    
 
    public Server() throws GlobalException{
        try{
            connection = new Socket(servervars.ip,servervars.port);
            OUT = new DataOutputStream(connection.getOutputStream());
            IN = new DataInputStream(connection.getInputStream());
            OOS = new ObjectOutputStream(connection.getOutputStream());
            OIS = new ObjectInputStream(connection.getInputStream());
            rshared.server = this;
        }
        catch(Throwable ex){
                throw new GlobalException("#EX_0007","client unable to connect to the server",null);
        }
    }

    
    public String read() throws GlobalException{
        try {
            return IN.readUTF();
        } catch (IOException ex) {
            throw new GlobalException("#EX_0008","client unable to read from server",null);
        }
    }
    public void write(String message) throws GlobalException{
        try {
           OUT.writeUTF(message);
        } catch (IOException ex) {
            throw new GlobalException("#EX_0009","client unable to write to server",null);
        }
    }
    
    public _rr readObj()throws GlobalException{
        try{
            return (_rr)OIS.readObject();
        }catch(Throwable e){
            throw new GlobalException("","Unable to read object from server",null);
        }
            
    }
    
    public void send(_rr obj,Do caller)throws GlobalException{
        //Setting the static shared variable Do, so that we can track the recent caller of this function
        if(caller != null){
            rshared.recentcaller = caller;
        }
        //-----------------------------------------------------------------------------------------------//
        try{
            OOS.writeObject(obj);
        }catch(Throwable e){
            throw new GlobalException("","Unable to wite object to server",null);
        }
            
    }
    public void close() throws GlobalException{
        try {
            connection.close();
        } catch (IOException ex) {
            throw new GlobalException("#EX_0005","error closing socket",null);
        }
    }
}

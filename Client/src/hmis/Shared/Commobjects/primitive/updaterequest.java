/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Shared.Commobjects.primitive;

import hmis.Shared.Identifiers.codes;

/**
 *
 * @author daredevils
 */
public class updaterequest extends _rr{
    /*
     * This will request the update for specific section of the clients department
     * Since, department is already identified while client signs in..so only section will be used to request the update
     */
    public String section;
    public String type;
    public updaterequest(){
        super.setid(codes.updatemodule);
    }
}

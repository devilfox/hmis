/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hmis.Shared.Commobjects.standard;

import hmis.Shared.Commobjects.primitive._rr;

/**
 *
 * @author root
 */
/*
    This is similar to packet, section and code are the routing information and payload is the data(if it involves which is accessible
       by predefined keywords). This will be used for both response and request simultaneously

       |******************************|**************************|*******************************|
       | section(who is making query) | code (what is the query) | payload(data involved if any) |
       |******************************|**************************|*******************************|
*/
public class query extends _rr{
    //who is making the query
    public  String section;
    //what is the query code
    public  String code;
    //payload
    public Object payload;    
    
    public query(String id){
        super.setid(id);
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Shared.Exception;

/**
 *
 * @author daredevils
 * This will help raising exceptions for handling the clients requests
 */
public class ServiceEx extends Exception{
    //The switch will be used to raise the exceptions limited to the department only
    private String DeptID; 
    //Exception code
    private String exp;
    //This exception is able to carry objects too
    private Object obj;
    
    public ServiceEx(String ID,String exp_code,Object ob){
        DeptID = ID;
        obj = ob;
        exp = exp_code;
        
    }
    public String getID(){
        return DeptID;
    }
    public Object getObject(){
        return obj;
    }
    
    public Boolean isCode(String code){
        //checks the code for match
        if(exp.equalsIgnoreCase(code)){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
    public String getCode(){
        return exp;
    }
}

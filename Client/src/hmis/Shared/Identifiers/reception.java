/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Shared.Identifiers;

/**
 *
 * @author daredevils
 */
public class reception {
    public static final String ID = "#DEPT_0001";
    public static final String UpdateModule = "#REQ_0001_000";
    public static final String OPDNewPatientRegister = "#REQ_0001_001";
    public static final String OldPatientExistanceCheck = "#REQ_0001_002";
    public static final String OldPatientreRegister = "#REQ_0001_003";
    public static final String AppointmentRegister = "#REQ_0001_004";
    public static final String AppointmentDetails = "#REQ_0001_005";
    public static final String SpecificBilling = "#REQ_0001_006";
    public static final String PatientSearch = "#REQ_0001_007";
    public static final String DoctorScheduleCheck = "#REQ_0001_008";
    public static final String AmbulanceStatusCheck = "#REQ_0001_009";
    public static final String AmbulanceRequest = "#REQ_0001_010";
    public static final String OPDPatientInfoRequest = "#REQ_0001_011";
    public static final String IPDPatientRegister="#REQ_0001_012";
    public static final String SearchPatientDetails="#REQ_001_013";
}

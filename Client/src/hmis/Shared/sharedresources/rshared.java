/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Shared.sharedresources;
import hmis.Layer.core.ClientThread;
import hmis.gui.main.HomeScreen;
import hmis.gui.main.LoginFrame;
import hmis.Layer.core.Server;
import hmis.Layer.DB.DBManager;

/**
 *
 * @author daredevils
 * strictly rshared objects for reception panel only
 */
public class rshared {
    /*
     * All rshared resources will be listed here
     * This will be the synchronising static class class
     */
    public static String clientdepartment = null;
    
    public static Server server = null;
    public static ClientThread listner = null;
    
    //Panel syncronizing vars
    public static LoginFrame login = null;
    public static HomeScreen home = null;
    
    /*
     * syncronizing variables is listed here
     */
    //This variable will store the caller of the core
    public static Do recentcaller = null;
    public static Boolean isloggedout = null;
    //Query queue
    public static void clear(){
        clientdepartment = null;
        server = null;
        listner = null;
        login = null;
        home = null;
        recentcaller = null;
        isloggedout = null;
    }
}

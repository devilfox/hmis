/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hmis.gui.Laboratory;

//import gui.Laboratory.LabPatient;
import hmis.Layer.core.GlobalException;
import hmis.Shared.Commobjects.primitive.ReceptionModuleUpdates;
import hmis.Shared.Commobjects.primitive.updaterequest;
import hmis.Shared.Commobjects.standard.query;
import hmis.Shared.Identifiers.codes;
import hmis.Shared.Identifiers.fields;
import hmis.Shared.Identifiers.section;
import hmis.Shared.sharedresources.Do;
import hmis.Shared.sharedresources.rshared;
import hmis.gui.base._rbase;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Rupesh
 */
public class PatientInfoPanel extends _rbase{

    /**
     * Creates new form PatientInfoPanel
     */
    
    public static String __name =section.labpatientinfopanel;
    public Do getdo(){
        return new Do(__name,this);
    }
    public void refresh(){
        this.revalidate();
        this.repaint();
    }
    
    public void requestupdate() throws hmis.Layer.core.GlobalException{
        updaterequest request = new updaterequest();
        request.section = __name;
        //request.type=request_types.rq_available_doctors;
        rshared.server.send(request,getdo());
    }
    
    public void pipeupdate(ReceptionModuleUpdates updates){
    
    }
    
    public void pipequeryreply(query q){
   
    }
    
    public String name,age,sex,address,contact,current_status,report_due;
    String reg_no;
    
    enum Status
    {
        NOT_EXAMINED,
        REPORT_DUE,
        REPORT_AVAILABLE
    };
    
    Status status;
       
    public PatientInfoPanel(LaboratoryPanel l,ViewIndividualPatientsPanel v) {
        initComponents();
        set_info(l,v);        
    }
    
    public final void set_info(final LaboratoryPanel l,ViewIndividualPatientsPanel v)
    {
        reg_no_label.setText(v.lpatient.reg_no);
        name_label.setText(v.lpatient.name);
        age_label.setText(v.lpatient.age);
        sex_label.setText(v.lpatient.sex);
        address_label.setText(v.lpatient.address);
        contact_label.setText(v.lpatient.contact);
        
        populate_test_table(v);
        handle_table_events(l,v);
    }
    
    private void alignCenter(JTable table, int column) {
    DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
    centerRenderer.setHorizontalAlignment(JLabel.CENTER);
    table.getColumnModel().getColumn(column).setCellRenderer(centerRenderer);
}
    public final void populate_test_table(ViewIndividualPatientsPanel v)
    {
        for(int i=0;i<test_info_table.getColumnCount();i++){
            alignCenter(test_info_table, i);
        }
        DefaultTableModel table = (DefaultTableModel) test_info_table.getModel();
        String cur_status=null;
        for(int i=0;i<v.lpatient.tests.size();i++){
            
            switch(v.lpatient.tests.get(i).rep_status){
                case "A":
                    cur_status="Report Available";
                    break;
                case "NA":
                    cur_status="Not Examined";
                    break;
                case "RDO":
                    cur_status="Report Due On "+v.lpatient.tests.get(i).rep_due_date;
                    break;
            }
        table.addRow(new Object[]{v.lpatient.tests.get(i).test_name,cur_status});
        }
    }
    
    public void handle_table_events(final LaboratoryPanel l,final ViewIndividualPatientsPanel v)
    {
        test_info_table.setRowHeight(30);
        test_info_table.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        
        //mouse clicked event
        test_info_table.addMouseListener(new java.awt.event.MouseAdapter() {
             @Override
           public void mouseClicked(java.awt.event.MouseEvent evt) {
           int row = test_info_table.getSelectedRow();
           int col = 1;
           
           String test_type = test_info_table.getValueAt(row, 0).toString();
          
           if(evt.getClickCount()>1)
           {           
               switch(v.lpatient.tests.get(row).rep_status)
               {
                   case "NA":
                       if(JOptionPane.showConfirmDialog(null, "Are You Sure Sample is received??", "Please  Confirm", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
                        {
                            not_examined_action(l, v, test_type);
                        }
                       break;
                   
                   case "RDO":
                       report_due_action(l,v,test_type);
                       break;
                       
                   case "A":
                       if(JOptionPane.showConfirmDialog(null, "Are You Sure You Want to Deliver the Report??", "Please  Confirm", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
                        {
                            test_info_table.setValueAt("Report Delivered ", row, col);
                        }
                       break;
               }
           }
         }
        });
    }
     public void request_changeofstatus(String lab_regno,String test,String Status)
    {
        HashMap<String,String> info=new HashMap();
        info.put(fields.lab_registration_no, lab_regno);
        info.put(fields.test,test);
        info.put(fields.report_status, Status);
        query q = new query(codes.datainput);
        q.section = this.__name;
        q.payload =info;
        try {
            rshared.server.send(q, this.getdo());
        } catch (GlobalException ex) {
            Logger.getLogger(PatientInfoPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
       
               
    }
     
     public void not_examined_action(LaboratoryPanel l,ViewIndividualPatientsPanel v,String test_name)
     {
         request_changeofstatus(v.lpatient.reg_no,test_name,"RDO");
         this.removeAll();
         JPanel base = l.get_laboratory_working_panel();
         base.removeAll();
         base.add(new ViewIndividualPatientsPanel(l, v.lpatient.reg_no));
         l.revalidate();
         l.repaint();
     }
     
    public void report_due_action(final LaboratoryPanel l,ViewIndividualPatientsPanel v,String test_name)
    {
        lab_test_details_panel.removeAll();
        lab_test_details_panel.add(new ReportResultsPanel(l,this,v,test_name));
        this.revalidate();
        this.repaint();
    }
    
    public JTable get_test_info_table()
    {
        return test_info_table;
    }
   
    public JPanel get_lab_test_details_panel()
    {
        return lab_test_details_panel;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        reg_no_label = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        name_label = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        age_label = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        sex_label = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        address_label = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        contact_label = new javax.swing.JLabel();
        lab_test_details_panel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        test_info_table = new javax.swing.JTable();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(41, 37, 37)));

        jLabel1.setFont(new java.awt.Font("Trebuchet MS", 0, 16)); // NOI18N
        jLabel1.setText("Lab Registration No. :");

        reg_no_label.setFont(new java.awt.Font("Trebuchet MS", 0, 16)); // NOI18N
        reg_no_label.setText("Reg No.");

        jLabel2.setFont(new java.awt.Font("Trebuchet MS", 0, 16)); // NOI18N
        jLabel2.setText("Name :");

        name_label.setFont(new java.awt.Font("Trebuchet MS", 0, 16)); // NOI18N
        name_label.setText("name");

        jLabel3.setFont(new java.awt.Font("Trebuchet MS", 0, 16)); // NOI18N
        jLabel3.setText("Age :");

        age_label.setFont(new java.awt.Font("Trebuchet MS", 0, 16)); // NOI18N
        age_label.setText("age");

        jLabel5.setFont(new java.awt.Font("Trebuchet MS", 0, 16)); // NOI18N
        jLabel5.setText("Sex :");

        sex_label.setFont(new java.awt.Font("Trebuchet MS", 0, 16)); // NOI18N
        sex_label.setText("sex");

        jLabel7.setFont(new java.awt.Font("Trebuchet MS", 0, 16)); // NOI18N
        jLabel7.setText("Address :");

        address_label.setFont(new java.awt.Font("Trebuchet MS", 0, 16)); // NOI18N
        address_label.setText("address");

        jLabel8.setFont(new java.awt.Font("Trebuchet MS", 0, 16)); // NOI18N
        jLabel8.setText("Contact :");

        contact_label.setFont(new java.awt.Font("Trebuchet MS", 0, 16)); // NOI18N
        contact_label.setText("contact");

        lab_test_details_panel.setBorder(javax.swing.BorderFactory.createTitledBorder("Lab Test Details"));

        test_info_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "  Test", "  Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(test_info_table);

        javax.swing.GroupLayout lab_test_details_panelLayout = new javax.swing.GroupLayout(lab_test_details_panel);
        lab_test_details_panel.setLayout(lab_test_details_panelLayout);
        lab_test_details_panelLayout.setHorizontalGroup(
            lab_test_details_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(lab_test_details_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 403, Short.MAX_VALUE)
                .addContainerGap())
        );
        lab_test_details_panelLayout.setVerticalGroup(
            lab_test_details_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(lab_test_details_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 255, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel5))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel3)
                                    .addGap(30, 30, 30)))
                            .addComponent(jLabel2))
                        .addGap(47, 47, 47)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(sex_label, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(age_label, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(name_label, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(address_label, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(contact_label, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(38, 38, 38)
                        .addComponent(reg_no_label)))
                .addGap(80, 80, 80)
                .addComponent(lab_test_details_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(41, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(58, 58, 58)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(reg_no_label))
                        .addGap(49, 49, 49)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(name_label))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(age_label))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(sex_label))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(address_label))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(contact_label)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addComponent(lab_test_details_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(71, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel address_label;
    private javax.swing.JLabel age_label;
    private javax.swing.JLabel contact_label;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel lab_test_details_panel;
    private javax.swing.JLabel name_label;
    private javax.swing.JLabel reg_no_label;
    private javax.swing.JLabel sex_label;
    private javax.swing.JTable test_info_table;
    // End of variables declaration//GEN-END:variables
}

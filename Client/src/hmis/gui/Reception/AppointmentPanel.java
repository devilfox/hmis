/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.gui.Reception;

import hmis.DB.Queries.Staff_Timings;
import hmis.Shared.Commobjects.primitive.ReceptionModuleUpdates;
import hmis.Shared.Commobjects.primitive.updaterequest;
import hmis.Shared.Commobjects.standard.query;
import hmis.Shared.Identifiers.codes;
import hmis.Shared.Identifiers.fields;
import hmis.Shared.Identifiers.request_types;
import hmis.Shared.Identifiers.section;
import hmis.Shared.sharedresources.Do;
import hmis.Shared.sharedresources.rshared;
import hmis.gui.base._rbase;
import hmis.gui.base.processingdialog;
import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Rupesh
 */
public class AppointmentPanel extends _rbase{
public static String __name = section.appointmentpanel;
public processingdialog _pd;
List<Staff_Timings> doc_schedule_list;
String time = null;
    public Do getdo(){
        return new Do(__name,this);
    }
    public void refresh(){
        this.revalidate();
        this.refresh();
    }
    public void requestupdate() throws hmis.Layer.core.GlobalException{
        updaterequest request = new updaterequest();
        request.section = __name;
        rshared.server.send(request,new Do(__name,this));
    }
    HashMap<String,Object> available_info=new HashMap();
    HashMap<String,Object> available_docs;
    List<HashMap<String,String>> appt_list;
    public void pipeupdate(ReceptionModuleUpdates updates){
        _pd.kill();
                time = updates.time;
                available_info=(HashMap<String, Object>) updates.updates;
                available_docs=(HashMap<String, Object>) available_info.get("Available_Doctors_Info");  
                appt_list=(List<HashMap<String,String>>) available_info.get("Appointments");
                System.out.println(updates.time);
                
               
    }
    @Override
    public void pipequeryreply(query q){
        _pd.kill();
        switch (q.code){
            case request_types.rq_feasible_appointment:{
                if((Boolean)((HashMap<String,Object>)q.payload).get(fields.issuccess)){
                    try {
                        String doc = (String)((HashMap<String,Object>)q.payload).get(fields.doctor);
                        String docid = (String)((HashMap<String,Object>)q.payload).get(fields.doctorid);
                        String date = (String)((HashMap<String,Object>)q.payload).get(fields.date);
                        String time = (String)((HashMap<String,Object>)q.payload).get("_time");
                        Thread.sleep(20);
                        tkappointment.enablefeasiblebutton(true);
                        tkappointment.enablesave(true);
                        
                        tkappointment.SetSelectedDoc(doc);
                        JOptionPane.showMessageDialog(null,"Appointment possible: \ndoctor: "+doc+ "\ndate: "+date+"\ntime: "+time);
                        
                    } catch (InterruptedException ex) {
                        Logger.getLogger(AppointmentPanel.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }else{
                    
                        tkappointment.enablefeasiblebutton(true);
                            tkappointment.enablesave(false);
                        _pd.kill();
                            JOptionPane.showMessageDialog(null,"Appointment slot not available for given information! :)");
                    
                }
                break;
            }
            case request_types.rq_appointment_register:{
                if((Boolean)((HashMap<String,Object>)q.payload).get(fields.issuccess)){
                    
                        String doc = (String)((HashMap<String,Object>)q.payload).get(fields.doctor);
                        String _day = (String)((HashMap<String,Object>)q.payload).get(fields.date);
                        String name = (String)((HashMap<String,Object>)q.payload).get(fields.name);
                        String contact = (String)((HashMap<String,Object>)q.payload).get(fields.contact);
                        
                        _pd.kill();
                        tkappointment.enablesave(false);
                        JOptionPane.showMessageDialog(null,"Appointment successfully registered: \ndoctor: "+doc+ "\ndate: "+_day+"\nPatient name: "+name+"\nContact: "+contact);
                    
                }else{
                     
                        tkappointment.enablefeasiblebutton(true);
                            tkappointment.enablesave(false);
                            
                        _pd.kill();
                            JOptionPane.showMessageDialog(null,"Appointment slot not available for given information! :)");
                    
                }
                break;
            }
        }
    }
    /**
     * Creates new form AppointmentPanel
     */
    takeappointment tkappointment;;
    ReceptionPanel rp;
    @Override
       public void enabled(Boolean flag){
            rp.enabled(flag);
       }
    public AppointmentPanel(ReceptionPanel _rp) {
        rp=_rp;
        initComponents();
         _pd = new processingdialog(this, null);
         _pd.exec();
         
    }
    
    public void action_on_viewappointment_button_click()
    {
        if(appt_list!=null && appt_list.size()>0){
            display_panel.setLayout(new BorderLayout());
            display_panel.removeAll();

            display_panel.add(new viewappoinment(rp,appt_list));
            this.revalidate();
            this.repaint();
        }else{
            JOptionPane.showMessageDialog(this, "No appointments found!");
        }
    }
    public void action_on_takeappointment_button_click()
    {
        
        display_panel.setLayout(new BorderLayout());
        display_panel.removeAll();
        tkappointment = new takeappointment(available_docs,this);
        display_panel.add(tkappointment);
        this.revalidate();
        this.repaint();
    }
    
    public void onfeasibleappointmentcheck(String dept,String doc,String day,String month,String year){
        query q = new query(codes.datainput);
        q.section = section.appointmentpanel;
        q.code = request_types.rq_feasible_appointment;
        
        HashMap<String,Object> pl = new HashMap();
        pl.put(fields.department, dept);
        pl.put(fields.doctor,doc);
        pl.put(fields.date,day+"/"+month+"/"+year);
        q.payload = pl;
        
        _pd = new processingdialog(this, q);
        _pd.exec();
        if(!_pd.iskilled()){
                tkappointment.enablefeasiblebutton(false);
                tkappointment.enablesave(false);
            }
    }
    public void onappointmentregister(String pname,String pcontact,String dept,String doc,String day,String month,String year,String doc_id){
        query q = new query(codes.datainput);
        q.section = section.appointmentpanel;
        q.code = request_types.rq_appointment_register;
        
        HashMap<String,Object> pl= new HashMap();
        pl.put(fields.name, pname);
        pl.put(fields.contact,pcontact);
        pl.put(fields.department, dept);
        pl.put(fields.doctor,doc);
        pl.put(fields.date,year+"-"+month+"-"+day);
        pl.put(fields.doctorid,doc_id);
        
        q.payload = pl;
        
        _pd = new processingdialog(this,q);
        _pd.exec();
        if(!_pd.iskilled()){
                tkappointment.enablefeasiblebutton(false);
                tkappointment.enablesave(false);
            }
    }
    
   
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        appoinmentselect_panel = new javax.swing.JPanel();
        appoinment_panel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        viewapppoinment_button = new javax.swing.JRadioButton();
        takeappoinment_button = new javax.swing.JRadioButton();
        display_panel = new javax.swing.JPanel();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 255, 204)));

        appoinmentselect_panel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        appoinment_panel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("Traditional Arabic", 0, 24)); // NOI18N
        jLabel1.setText("Appointment");

        javax.swing.GroupLayout appoinment_panelLayout = new javax.swing.GroupLayout(appoinment_panel);
        appoinment_panel.setLayout(appoinment_panelLayout);
        appoinment_panelLayout.setHorizontalGroup(
            appoinment_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(appoinment_panelLayout.createSequentialGroup()
                .addGap(413, 413, 413)
                .addComponent(jLabel1)
                .addContainerGap(458, Short.MAX_VALUE))
        );
        appoinment_panelLayout.setVerticalGroup(
            appoinment_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(appoinment_panelLayout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(0, 10, Short.MAX_VALUE))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        buttonGroup1.add(viewapppoinment_button);
        viewapppoinment_button.setFont(new java.awt.Font("Trebuchet MS", 0, 14)); // NOI18N
        viewapppoinment_button.setText("View Appointment");
        viewapppoinment_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewapppoinment_buttonActionPerformed(evt);
            }
        });
        viewapppoinment_button.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                viewapppoinment_buttonKeyPressed(evt);
            }
        });

        buttonGroup1.add(takeappoinment_button);
        takeappoinment_button.setFont(new java.awt.Font("Trebuchet MS", 0, 14)); // NOI18N
        takeappoinment_button.setText("Take Apoointment");
        takeappoinment_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                takeappoinment_buttonActionPerformed(evt);
            }
        });
        takeappoinment_button.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                takeappoinment_buttonKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addComponent(viewapppoinment_button)
                .addGap(180, 180, 180)
                .addComponent(takeappoinment_button)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(viewapppoinment_button)
                    .addComponent(takeappoinment_button))
                .addContainerGap(13, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout appoinmentselect_panelLayout = new javax.swing.GroupLayout(appoinmentselect_panel);
        appoinmentselect_panel.setLayout(appoinmentselect_panelLayout);
        appoinmentselect_panelLayout.setHorizontalGroup(
            appoinmentselect_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(appoinmentselect_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(appoinmentselect_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(appoinment_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        appoinmentselect_panelLayout.setVerticalGroup(
            appoinmentselect_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, appoinmentselect_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(appoinment_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9))
        );

        display_panel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout display_panelLayout = new javax.swing.GroupLayout(display_panel);
        display_panel.setLayout(display_panelLayout);
        display_panelLayout.setHorizontalGroup(
            display_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        display_panelLayout.setVerticalGroup(
            display_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 379, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(display_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(appoinmentselect_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(appoinmentselect_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(display_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void viewapppoinment_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewapppoinment_buttonActionPerformed
        action_on_viewappointment_button_click();
        // TODO add your handling code here:
    }//GEN-LAST:event_viewapppoinment_buttonActionPerformed

    private void takeappoinment_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_takeappoinment_buttonActionPerformed
        action_on_takeappointment_button_click();
        // TODO add your handling code here:
    }//GEN-LAST:event_takeappoinment_buttonActionPerformed

    private void viewapppoinment_buttonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_viewapppoinment_buttonKeyPressed
    if(evt.getKeyCode()== KeyEvent.VK_ENTER)
       action_on_viewappointment_button_click();
        // TODO add your handling code here:
    }//GEN-LAST:event_viewapppoinment_buttonKeyPressed

    private void takeappoinment_buttonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_takeappoinment_buttonKeyPressed
        if(evt.getKeyCode()== KeyEvent.VK_ENTER)
        action_on_takeappointment_button_click(); // TODO add your handling code here:
    }//GEN-LAST:event_takeappoinment_buttonKeyPressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel appoinment_panel;
    private javax.swing.JPanel appoinmentselect_panel;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JPanel display_panel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton takeappoinment_button;
    private javax.swing.JRadioButton viewapppoinment_button;
    // End of variables declaration//GEN-END:variables
}

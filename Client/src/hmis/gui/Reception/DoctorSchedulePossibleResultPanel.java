/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.gui.Reception;

import hmis.DB.Queries.Staff_Timings_Strings;
import hmis.Layer.DB.Staff_Timings;
import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Rupesh
 */
public class DoctorSchedulePossibleResultPanel extends javax.swing.JPanel {
    
    List<Staff_Timings> doctor_list=new ArrayList<>();
    String doctor_name,doctor_speciality;
    String sunday_routine,monday_routine,tuesday_routine,wednesday_routine,
            thursday_routine,friday_routine,saturday_routine;
    /**
     * Creates new form DoctorScheduleFinalResultPanel
     */
    
    public DoctorSchedulePossibleResultPanel(List<Staff_Timings_Strings> matches,DoctorSchedulePanel p)
    {
        initComponents();
        
        specific_doctor_schedule_table.setRowHeight(30);
        display_search_table(matches,p);
        
        initializeTableEvents(p);
    }
    
    public DoctorSchedulePossibleResultPanel() {
        initComponents();
    }
    
      private void alignCenter(JTable table, int column) {
    DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
    centerRenderer.setHorizontalAlignment(JLabel.CENTER);
    table.getColumnModel().getColumn(column).setCellRenderer(centerRenderer);
}
    
    private void display_search_table(DoctorSchedulePanel p)
    {
        for(int i=0;i<specific_doctor_schedule_table.getColumnCount();i++){
            alignCenter(specific_doctor_schedule_table, i);
        }
        DefaultTableModel table = (DefaultTableModel) specific_doctor_schedule_table.getModel();
        
        table.addRow(new Object[]{doctor_name,doctor_speciality,
                                  sunday_routine,
                                  monday_routine,
                                  tuesday_routine,
                                  wednesday_routine,
                                  thursday_routine,
                                  friday_routine,
                                  saturday_routine
                                    });
        
        }
    
    private void display_search_table(List<Staff_Timings_Strings> doctor_list ,DoctorSchedulePanel p)
    {
        JPanel base = p.get_base_panel();
        base.removeAll();
        base.setLayout(new BorderLayout());
        base.add(this);
        p.repaint();
        p.revalidate();
        
        DefaultTableModel table = (DefaultTableModel) specific_doctor_schedule_table.getModel();
        
        for(int i=0;i<doctor_list.size();i++)
        {
        table.addRow(new Object[]{doctor_list.get(i).name,
                                  doctor_list.get(i).speciality,
                                  doctor_list.get(i).sun,
                                  doctor_list.get(i).mon,
                                  doctor_list.get(i).tue,
                                  doctor_list.get(i).wed,
                                  doctor_list.get(i).thu,
                                  doctor_list.get(i).fri,
                                  doctor_list.get(i).sat
                                });
        }
    }
    
    
    private void initializeTableEvents(final DoctorSchedulePanel p)
    {
        //specific_doctor_schedule_table.setRowHeight(30);
       
        specific_doctor_schedule_table.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        
        //mouse clicked event
        specific_doctor_schedule_table.addMouseListener(new java.awt.event.MouseAdapter() {
             @Override
           public void mouseClicked(java.awt.event.MouseEvent evt) {
           int row = specific_doctor_schedule_table.getSelectedRow();
           int col = 0;
 
           if(evt.getClickCount()>1)
           {      
               doctor_name = (String) specific_doctor_schedule_table.getValueAt(row, col);
               doctor_speciality = (String) specific_doctor_schedule_table.getValueAt(row, col+1);
               sunday_routine= (String) specific_doctor_schedule_table.getValueAt(row, col+2);
               monday_routine= (String) specific_doctor_schedule_table.getValueAt(row, col+3);
               tuesday_routine= (String) specific_doctor_schedule_table.getValueAt(row, col+4);
               wednesday_routine= (String) specific_doctor_schedule_table.getValueAt(row, col+5);
               thursday_routine= (String) specific_doctor_schedule_table.getValueAt(row, col+6);
               friday_routine= (String) specific_doctor_schedule_table.getValueAt(row, col+7);
               saturday_routine= (String) specific_doctor_schedule_table.getValueAt(row, col+8);
              
               JPanel base = p.get_base_panel();
               base.removeAll();
               base.setLayout(new BorderLayout());
               base.add(new DoctorScheduleFinalResultPanel(doctor_name,doctor_speciality,sunday_routine,monday_routine,tuesday_routine,wednesday_routine,thursday_routine,friday_routine,saturday_routine));
               p.repaint();
               p.revalidate();
           }
         }
        });
    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        specific_doctor_schedule_table = new javax.swing.JTable();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(52, 78, 97)));

        specific_doctor_schedule_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "   Doctor's Name", "   Specialization", "   Sunday", "   Monday", "   Tuesday", "   Wednesday", "   Thursday", "   Friday", "   Saturday"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(specific_doctor_schedule_table);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 898, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(12, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable specific_doctor_schedule_table;
    // End of variables declaration//GEN-END:variables
}

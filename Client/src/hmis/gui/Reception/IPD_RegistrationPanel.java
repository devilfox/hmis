/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.gui.Reception;

import hmis.DB.Queries.room_info;
import hmis.Layer.DB.DBManager;
import hmis.Layer.DB.IPD_Reg_Query;
import hmis.Layer.DB.Old_patient;
import hmis.Shared.Commobjects.primitive.ReceptionModuleUpdates;
import hmis.Shared.Commobjects.primitive.updaterequest;
import hmis.Shared.Commobjects.standard.query;
import hmis.Shared.Identifiers.codes;
import hmis.Shared.Identifiers.fields;
import hmis.Shared.Identifiers.reception;
import hmis.Shared.Identifiers.request_types;
import hmis.Shared.Identifiers.section;
import hmis.Shared.sharedresources.Do;
import hmis.Shared.sharedresources.rshared;
import hmis.gui.base._rbase;
import hmis.gui.base.processingdialog;
import hmis.gui.main.ValidateInput;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import javax.swing.JOptionPane;
/**
 *
 * @author Rupesh
 */

public class IPD_RegistrationPanel extends _rbase {
    public static String __name = section.ipdregisterpanel;
    public processingdialog _pd;
    Object tosend;
    public Do getdo(){
        return new Do(__name,this);
    }
    public void refresh(){
        this.revalidate();
        this.refresh();
    }
    public void requestupdate() throws hmis.Layer.core.GlobalException{
        updaterequest request = new updaterequest();
        request.section = __name;
        request.type=request_types.rq_available_docs_n_rooms;
        rshared.server.send(request,new Do(__name,this));
    }
    
    
    public void pipeupdate(ReceptionModuleUpdates updates){
        _pd.kill();
        switch(updates.type){
            case request_types.rq_available_docs_n_rooms:{ 
                available_info=(HashMap<String, Object>) updates.updates;
                available_docs=(HashMap<String, Object>) available_info.get("Available_Doctors_Info");
                available_rooms=(HashMap<String, Object>)available_info.get("Available_Rooms_Info"); 
                break;
                }
            default:
                break;
        }
        
         initialize_doctor_combo_from_hashmap();
        initialize_rooms_combo_from_hashmap();
    }
    public void pipequeryreply(query q)
    {
        _pd.kill();
        switch(q.code){
            case reception.OPDPatientInfoRequest:
                patient_available_info=(HashMap<String, String>) q.payload;
                
                if(set_form_fields()){
                    
                    save_button.setEnabled(true);
                }else{
                    
                    save_button.setEnabled(false);
                    JOptionPane.showMessageDialog(null,"Patient not found! Please check examination id :(");
                }
                break;
            case reception.IPDPatientRegister:{
                    
                    save_button.setEnabled(false);
                    HashMap<String,Object> pl = (HashMap<String,Object>)q.payload;
                    
                    if((Boolean)pl.get(fields.issuccess)){
                        JOptionPane.showMessageDialog(null,"registration successful! :)"+"\nPatient ID: "+(String)pl.get(fields.patientid)+"\nExam ID: "+(String)pl.get(fields.examination_id));
                    }else{
                        JOptionPane.showMessageDialog(null,"error!, please check your examination id properly :(");
                    }
                break;
            }
                
            default:
                break;
        }
         
        
    }
    /**
     * Creates new form IPD_RegistrationPanel
     */
    
    /**
     * Creates new form IPD_RegistrationPanel
     */
    HashMap<String,Object> available_info=new HashMap();
    HashMap<String,Object> available_docs=new HashMap();
    HashMap<String,Object> available_rooms=new HashMap();
    HashMap<String,String> patient_available_info=new HashMap();
    int current_year;
    //variables to store data from form field
    String registration_no;
    String examination_id;
    int reg_no;
    int exam_id;
    String name;
    String age;
    String dob_year;
    String dob_month;
    String dob_day;
    String sex;
    String address;
    String contact;
    String guardian;
    String marital_status;
    String guardian_contact;
    String department;
    String reference_doctor;
    String room_no;
    String room_type;
    String bed_no;
    String diagnosed_with;
    String doc_id;
    DBManager db1=null;
    IPD_Reg_Query q1=null;
    //this class is used for holding details available from opd reg 
    //and is used for checking if any fields have been changed during ipd or not
    Old_patient previous_details=null;  
    
    ReceptionPanel Reception;
    @Override
       public void enabled(Boolean flag){
        
            Reception.enabled(flag);
       }
    public IPD_RegistrationPanel(ReceptionPanel rec) {
        Reception = rec;
        initComponents();
        initializeDateAndTime();
       // initializeDOBcombo();
        save_button.setEnabled(false);

        _pd = new processingdialog(this, null);
        _pd.exec();
        
       
        //this sleep is temporarily intended for setting time delay for constructor to be called
        //so as to obtain data from DB
        
     
  
        
        /*
        try {
            db1 = new DBManager("jdbc:mysql://localhost/?allowMultiQueries=true","root","");
            q1=new IPD_Reg_Query(db1.getConnection());
        } catch (GlobalException ex) {
            ex.printStackTrace();
        }*/
              
    }
    
    public void initialize_doctor_combo_from_hashmap()
    {
     for (Map.Entry entry : available_docs.entrySet()) {
           depart_combo.addItem(entry.getKey());      
        }   
    }
    
    public void initialize_rooms_combo_from_hashmap()
    {
    for (Map.Entry entry:available_rooms.entrySet()){
      roomtype_combo.addItem(entry.getKey());
      }
    }
    
    public final void initializeDateAndTime()
    {
          Date date=new Date();
          SimpleDateFormat ft =new SimpleDateFormat ("yyyy-MM-dd E");
          date_text.setText(ft.format(date));      
          SimpleDateFormat ft1=new SimpleDateFormat ("hh:mm a");
          time_text.setText(ft1.format(date));
          
          //To access current date
          SimpleDateFormat ft2 =new SimpleDateFormat ("yyyy");
          String curr_yr = ft2.format(date);
          
          try{
              current_year = Integer.parseInt(curr_yr);
          }
          catch(NumberFormatException e){
              System.out.println(e.getMessage());
          }
    }
    
//    public final void initializeDOBcombo()
//    {
//        for(int i=current_year-150;i<=current_year;i++)
//        {
//           year_combo.addItem(i);
//        }
//        
//        year_combo.setSelectedIndex(100);
//       
//       String[] month = {"January","February","March","April",
//                         "May","June","July","August",
//                         "September","October","November","December"};
//       
//       for(int i=0;i<month.length;i++)
//       {
//           month_combo.addItem(month[i]);       
//       }
//    }

//    public void day_add(int n)
//    {
//       day_combo.removeAllItems();
//       for(int i=1;i<=n;i++)
//       {  
//          day_combo.addItem(i);
//       }
//           
//    }
    public void action_on_save_button_click()
    {
         get_ipd_data();
        
        // If all input data are valid
        if(validate_input_data())
        {
            //store info in databse
          
            _pd = new processingdialog(this,register_ipd_patient());
            _pd.init();
            _pd.exec();
           if(!_pd.iskilled()){
                save_button.setEnabled(false);
            }
        }
    }
    public void action_on_ok_button_click()
    {
         // obtain exaination id
        examination_id = exam_id_text.getText().toString();
       //Perform Request for available patient data for examination_id obtained above
        // --------------------------------------------------------------
        if(validate_examination_id()){
                  //this.processingdialogtimeoutreached = true;
            query q = new query(codes.datainput);
            q.section = this.__name;
            q.code = reception.OPDPatientInfoRequest;
            q.payload = examination_id;
            
            
            _pd = new processingdialog(this,q);
            _pd.init();
            _pd.exec();
           if(!_pd.iskilled()){
                save_button.setEnabled(false);
            }
        }
    }
    
    public void action_on_refresh_button_click()
    {
        exam_id_text.setText("");
        name_text.setText("");
        age_text.setText("");
        address_text.setText("");
        contact_text.setText("");
        guardian_text.setText("");
        guardian_contact_text.setText("");
        diagnosed_text.setText("");
        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        ipd_label = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        exam_id_label = new javax.swing.JLabel();
        exam_id_text = new javax.swing.JTextField();
        ok_button = new javax.swing.JButton();
        regist_label = new javax.swing.JLabel();
        regist_text = new javax.swing.JLabel();
        date_label = new javax.swing.JLabel();
        date_text = new javax.swing.JLabel();
        time_label = new javax.swing.JLabel();
        time_text = new javax.swing.JLabel();
        name_label = new javax.swing.JLabel();
        name_text = new javax.swing.JTextField();
        age_label = new javax.swing.JLabel();
        age_text = new javax.swing.JTextField();
        address_label = new javax.swing.JLabel();
        address_text = new javax.swing.JTextField();
        sex_label = new javax.swing.JLabel();
        sex_combo = new javax.swing.JComboBox();
        contact_label = new javax.swing.JLabel();
        contact_text = new javax.swing.JTextField();
        guardian_label = new javax.swing.JLabel();
        guardian_text = new javax.swing.JTextField();
        marital_label = new javax.swing.JLabel();
        marital_combo = new javax.swing.JComboBox();
        guardian_contact_label = new javax.swing.JLabel();
        guardian_contact_text = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        depart_combo = new javax.swing.JComboBox();
        doctor_label = new javax.swing.JLabel();
        doctor_combo = new javax.swing.JComboBox();
        room_label = new javax.swing.JLabel();
        roomno_combo = new javax.swing.JComboBox();
        roomtype_label = new javax.swing.JLabel();
        roomtype_combo = new javax.swing.JComboBox();
        bedno_label = new javax.swing.JLabel();
        bedno_combo = new javax.swing.JComboBox();
        diagnosed_label = new javax.swing.JLabel();
        diagnosed_text = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        save_button = new javax.swing.JButton();
        refresh_button = new javax.swing.JButton();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 255)));

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        ipd_label.setFont(new java.awt.Font("Arial", 0, 24)); // NOI18N
        ipd_label.setText("IPD Registration");
        ipd_label.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        ipd_label.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(433, 433, 433)
                .addComponent(ipd_label)
                .addContainerGap(475, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(ipd_label)
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        exam_id_label.setFont(new java.awt.Font("Arial Narrow", 0, 16)); // NOI18N
        exam_id_label.setText("Enter Examination ID");

        exam_id_text.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N
        exam_id_text.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                exam_id_textKeyPressed(evt);
            }
        });

        ok_button.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N
        ok_button.setText("OK");
        ok_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ok_buttonActionPerformed(evt);
            }
        });
        ok_button.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ok_buttonKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(exam_id_label)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(exam_id_text, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ok_button)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(exam_id_label)
                    .addComponent(exam_id_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ok_button))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        regist_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        regist_label.setText("Reg.No:");

        regist_text.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        regist_text.setText("Number");

        date_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        date_label.setText("Date:");

        date_text.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        date_text.setText("yyyy/mm/dd");

        time_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        time_label.setText("Time:");

        time_text.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        time_text.setText("hr:min");

        name_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        name_label.setText("Name:");

        name_text.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        age_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        age_label.setText("Age:");

        age_text.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        address_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        address_label.setText("Address:");

        address_text.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        sex_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        sex_label.setText("Sex:");

        sex_combo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        sex_combo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Male", "Female", "Other" }));

        contact_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        contact_label.setText("Contact:");

        contact_text.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        guardian_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        guardian_label.setText("Guardian:");

        guardian_text.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        marital_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        marital_label.setText("Marital Status");

        marital_combo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Single", "Married", "Divorced", " " }));

        guardian_contact_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        guardian_contact_label.setText("Guardian's Contact:");

        guardian_contact_text.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setText("Department:");

        depart_combo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        depart_combo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                depart_comboActionPerformed(evt);
            }
        });

        doctor_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        doctor_label.setText("Ref.Doctor");

        doctor_combo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        doctor_combo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Upendra Devkota", "Bhagwan Koirala" }));

        room_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        room_label.setText("Room.No:");

        roomno_combo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        roomno_combo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                roomno_comboActionPerformed(evt);
            }
        });

        roomtype_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        roomtype_label.setText("Room type:");

        roomtype_combo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        roomtype_combo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                roomtype_comboActionPerformed(evt);
            }
        });

        bedno_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        bedno_label.setText("Bed.No:");

        bedno_combo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        bedno_combo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "20", "21A", "100C" }));
        bedno_combo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bedno_comboActionPerformed(evt);
            }
        });

        diagnosed_label.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        diagnosed_label.setText("Diagnosed With:");

        diagnosed_text.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(regist_label)
                                    .addComponent(name_label)
                                    .addComponent(age_label))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(regist_text))
                            .addComponent(room_label))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 332, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(date_label)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(date_text, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(roomtype_label)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(roomtype_combo, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(93, 93, 93))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(guardian_label)
                            .addComponent(guardian_contact_label)
                            .addComponent(jLabel1)
                            .addComponent(address_label)
                            .addComponent(diagnosed_label))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(address_text, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(depart_combo, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(roomno_combo, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(guardian_contact_text, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(guardian_text, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(age_text, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(name_text, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(diagnosed_text, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(113, 113, 113)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(sex_label)
                            .addComponent(marital_label)
                            .addComponent(contact_label, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(bedno_label, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(doctor_label, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(83, 83, 83)
                        .addComponent(time_label)))
                .addGap(5, 5, 5)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(contact_text)
                    .addComponent(marital_combo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sex_combo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(time_text, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(doctor_combo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(bedno_combo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(89, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(regist_label)
                    .addComponent(regist_text)
                    .addComponent(date_label)
                    .addComponent(date_text)
                    .addComponent(time_label)
                    .addComponent(time_text))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(name_label)
                    .addComponent(name_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(age_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(age_label)
                    .addComponent(sex_label)
                    .addComponent(sex_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(address_label)
                    .addComponent(address_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(contact_label)
                    .addComponent(contact_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(guardian_label)
                    .addComponent(marital_label)
                    .addComponent(marital_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(guardian_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(guardian_contact_label)
                    .addComponent(guardian_contact_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(depart_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(doctor_label)
                    .addComponent(doctor_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(room_label)
                    .addComponent(roomno_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(roomtype_label)
                    .addComponent(roomtype_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bedno_label)
                    .addComponent(bedno_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(diagnosed_label)
                    .addComponent(diagnosed_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 24, Short.MAX_VALUE))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        save_button.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        save_button.setText("Save");
        save_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                save_buttonActionPerformed(evt);
            }
        });
        save_button.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                save_buttonKeyPressed(evt);
            }
        });

        refresh_button.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        refresh_button.setText("Refresh");
        refresh_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refresh_buttonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(save_button, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(refresh_button)
                .addGap(31, 31, 31))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(save_button)
                    .addComponent(refresh_button))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void depart_comboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_depart_comboActionPerformed
        // TODO add your handling code here:
        String selected = (String) depart_combo.getSelectedItem();
        for(int i=0;i<available_docs.keySet().toArray().length;i++){
            String dept_name=(String)available_docs.keySet().toArray()[i];
            if(selected.equals(dept_name))
            {
                Vector<HashMap<String,String>> same_dept_docs=((Vector)available_docs.get(dept_name));
                doctor_combo.removeAllItems();
                for(int j=0;j<same_dept_docs.size();j++)
                {   
                 doctor_combo.addItem(same_dept_docs.get(j).get(fields.doctor));
                }
            } 
        }
    }//GEN-LAST:event_depart_comboActionPerformed

    //This event is called when all the data are entered and Save button is clicked
    private void save_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_save_buttonActionPerformed
        // get all the IPD data 
       action_on_save_button_click();
    }//GEN-LAST:event_save_buttonActionPerformed

    //This event is called when Examination ID is entered at first
    private void ok_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ok_buttonActionPerformed
       
        action_on_ok_button_click();
    }//GEN-LAST:event_ok_buttonActionPerformed

    private void roomno_comboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_roomno_comboActionPerformed
        // TODO add your handling code here:
        Vector<room_info> same_typed_rooms=(Vector<room_info>)(available_rooms.get(roomtype_combo.getSelectedItem().toString()));
        if(same_typed_rooms.size()>=0){
        int index=roomno_combo.getSelectedIndex();
        //index has been set to zero since roomno_combo.getSelectedIndex returns value of -1 during transitions
        //and hence causes error while accessing data from vector
        if(index<0)
            index=0;
        //split the string containing available_beds_no returned from DB in room_info type class
        String beds[]=same_typed_rooms.get(index).beds_available.split(",");
        bedno_combo.removeAllItems();;
        for(int i=0;i<beds.length;i++)
        {
            bedno_combo.addItem(beds[i]);
        }
        }
    }//GEN-LAST:event_roomno_comboActionPerformed

    private void roomtype_comboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_roomtype_comboActionPerformed
        // TODO add your handling code here:
        String selected=roomtype_combo.getSelectedItem().toString();
        for(int i=0;i<available_rooms.keySet().toArray().length;i++)
        {
            String room_type=(String) available_rooms.keySet().toArray()[i];
            
            if(selected.equalsIgnoreCase(room_type)){
                Vector<room_info> same_typed_rooms=(Vector<room_info>) available_rooms.get(room_type);
                roomno_combo.removeAllItems();
                for(int j=0;j<same_typed_rooms.size();j++)
                {
                    roomno_combo.addItem(same_typed_rooms.get(j).room_no);
                }
            }
        }
    }//GEN-LAST:event_roomtype_comboActionPerformed

    private void bedno_comboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bedno_comboActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bedno_comboActionPerformed

    private void save_buttonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_save_buttonKeyPressed
        if(evt.getKeyCode()== KeyEvent.VK_ENTER)
            action_on_save_button_click();
        // TODO add your handling code here:
    }//GEN-LAST:event_save_buttonKeyPressed

    private void exam_id_textKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_exam_id_textKeyPressed
         if(evt.getKeyCode()== KeyEvent.VK_ENTER)
             action_on_ok_button_click();
        // TODO add your handling code here:
    }//GEN-LAST:event_exam_id_textKeyPressed

    private void ok_buttonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ok_buttonKeyPressed
     if(evt.getKeyCode()== KeyEvent.VK_ENTER)
         action_on_ok_button_click();// TODO add your handling code here:
    }//GEN-LAST:event_ok_buttonKeyPressed

    private void refresh_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refresh_buttonActionPerformed
        action_on_refresh_button_click();
        // TODO add your handling code here:
    }//GEN-LAST:event_refresh_buttonActionPerformed

   
    
    public boolean validate_examination_id()
    {
        if(!ValidateInput.validateRegistration(examination_id)){
            JOptionPane.showMessageDialog(null, "PLESE  ENTER  A  VALID  REGSITRATION  NO", "INPUT ERROR", JOptionPane.ERROR_MESSAGE);
            exam_id_text.setText("");
            return false;
        }
        
        return true;
    }
    
    //handle query based on examination ID
    public Boolean set_form_fields()
    {
        
        try {
            //database handling code to obtain data based on examination ID
              if(patient_available_info.size()>0)
              {
                  registration_no=patient_available_info.get(fields.patientid);
                  name=patient_available_info.get(fields.name);
                  sex=patient_available_info.get(fields.sex);
                  address=patient_available_info.get(fields.address);
                  contact=patient_available_info.get(fields.contact);
                  fill_form_field();
                  if(registration_no==null||name==null){return false;}
                  return true;
              }
              else{
                  //reset all fields
                  return false;
              }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
     }
    
    //fill the form fields with data from database
    public void fill_form_field()
    {
        regist_text.setText(registration_no);
         
        name_text.setText(name);
        name_text.setEditable(false);
        
        age_text.setText(age);
        
        switch(sex)
        {
            case "M":
                sex_combo.setSelectedIndex(0);
                break;
                
            case "F":
                sex_combo.setSelectedIndex(1);
                break;
                
             default:
                sex_combo.setSelectedIndex(2);
                break;
        }
        
        address_text.setText(address);
        contact_text.setText(contact);
        
    //    depart_combo.setSelectedItem(department);
    }
    
    //obtain all the IPD data
    public void get_ipd_data()
    {
        
        dob_year = "2001";
        int month = 05;
        dob_month = String.valueOf(month);
        dob_day =  "01";
        guardian = guardian_text.getText();
        guardian_contact = guardian_contact_text.getText();
        marital_status = marital_combo.getSelectedItem().toString();
        reference_doctor = doctor_combo.getSelectedItem().toString();
        room_no = roomno_combo.getSelectedItem().toString();
        room_type = roomtype_combo.getSelectedItem().toString();
        bed_no = bedno_combo.getSelectedItem().toString();
        diagnosed_with = diagnosed_text.getText();
        contact=contact_text.getText().toString();
        reg_no=Integer.parseInt(registration_no);
        exam_id=Integer.parseInt(examination_id);
        doc_id=Get_Doc_ID();
        age=age_text.getText().toString();
        
//        switch (reference_doctor) {
//            case "Upendra Devkota":
//                doc_id=30;
//                break;
//            case "Bhagwan Koirala":
//                doc_id=50;
//                break;
//            default:
//                break;
//        }
    }
    
    public String Get_Doc_ID(){
        String doctor_id=null;
        String dept=depart_combo.getSelectedItem().toString();
        String selected_doc=doctor_combo.getSelectedItem().toString();
        Vector<HashMap<String,String>> same_dept_docs=(Vector<HashMap<String,String>>) available_docs.get(dept);
        for(int i=0;i<same_dept_docs.size();i++)
        {
            if(selected_doc.equalsIgnoreCase(same_dept_docs.get(i).get(fields.doctor))){
                doctor_id=same_dept_docs.get(i).get(fields.doctorid);
            }
        }
        return doctor_id;
        
    }
    //to validate input data
    public boolean validate_input_data()
    {
        if(!ValidateInput.validateName(guardian)){
            JOptionPane.showMessageDialog(null, "INVALID  GUARDIAN'S  NAME", "INPUT  ERROR", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
        else if(!ValidateInput.validateContact(guardian_contact)){
            JOptionPane.showMessageDialog(null, "INVALID  GUARDIAN'S  CONTACT", "INPUT  ERROR", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
        else if(!ValidateInput.validateAddress(diagnosed_with)){
            JOptionPane.showMessageDialog(null, "PLEASE  ENTER  DIAGNOSED  WITH", "INPUT  ERROR", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
        return true;
    }
    
    //store the info in database
    public query register_ipd_patient()
    {
        //database handling code to store data in database
//        Old_patient new_details=new Old_patient(name, address, sex, contact,reg_no);
//        Room_Info room_details=new Room_Info(room_type, room_no, bed_no);
        HashMap<String,String> data=new HashMap();
        data.put(fields.examination_id,String.valueOf(exam_id));
        //data.put(fields.name, name);
        data.put(fields.address, address);
        //data.put(fields.sex, sex);
        data.put(fields.contact,contact);
        data.put(fields.patientid,String.valueOf(reg_no));
        data.put(fields.diagnosed_with, diagnosed_with);
        data.put(fields.marital_status, marital_status);
        data.put(fields.guardian_name,guardian);
        data.put(fields.guardian_contact,guardian_contact);
        data.put(fields.date_of_birth,(dob_year+"-"+dob_month+"-"+dob_day));
        data.put(fields.room_type,room_type);
        data.put(fields.room_no,room_no);
        data.put(fields.bed_no,bed_no);
        data.put(fields.doctorid,doc_id);
        data.put(fields.age, age);
        data.put(fields.current_date,date_text.getText().toString().split(" ")[0].trim());
        
        query q = new query(codes.datainput);
        q.section = this.__name;
        q.code = reception.IPDPatientRegister;
        q.payload =data;
        return q;
//        IPD_Reg_Query_Details ipd_details=new IPD_Reg_Query_Details(exam_id, new_details, diagnosed_with, marital_status, guardian, guardian_contact, department, doc_id,java.sql.Date.valueOf(dob_year+"-"+dob_month+"-"+dob_day), room_details);
//        q1.Insert_IPD_Data(ipd_details, previous_details);  
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel address_label;
    private javax.swing.JTextField address_text;
    private javax.swing.JLabel age_label;
    private javax.swing.JTextField age_text;
    private javax.swing.JComboBox bedno_combo;
    private javax.swing.JLabel bedno_label;
    private javax.swing.JLabel contact_label;
    private javax.swing.JTextField contact_text;
    private javax.swing.JLabel date_label;
    private javax.swing.JLabel date_text;
    private javax.swing.JComboBox depart_combo;
    private javax.swing.JLabel diagnosed_label;
    private javax.swing.JTextField diagnosed_text;
    private javax.swing.JComboBox doctor_combo;
    private javax.swing.JLabel doctor_label;
    private javax.swing.JLabel exam_id_label;
    private javax.swing.JTextField exam_id_text;
    private javax.swing.JLabel guardian_contact_label;
    private javax.swing.JTextField guardian_contact_text;
    private javax.swing.JLabel guardian_label;
    private javax.swing.JTextField guardian_text;
    private javax.swing.JLabel ipd_label;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JComboBox marital_combo;
    private javax.swing.JLabel marital_label;
    private javax.swing.JLabel name_label;
    private javax.swing.JTextField name_text;
    private javax.swing.JButton ok_button;
    private javax.swing.JButton refresh_button;
    private javax.swing.JLabel regist_label;
    private javax.swing.JLabel regist_text;
    private javax.swing.JLabel room_label;
    private javax.swing.JComboBox roomno_combo;
    private javax.swing.JComboBox roomtype_combo;
    private javax.swing.JLabel roomtype_label;
    private javax.swing.JButton save_button;
    private javax.swing.JComboBox sex_combo;
    private javax.swing.JLabel sex_label;
    private javax.swing.JLabel time_label;
    private javax.swing.JLabel time_text;
    // End of variables declaration//GEN-END:variables
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.gui.Reception;

import hmis.Shared.Commobjects.primitive.ReceptionModuleUpdates;
import hmis.Shared.Commobjects.primitive.updaterequest;
import hmis.Shared.Commobjects.standard.query;
import hmis.Shared.Identifiers.codes;
import hmis.Shared.Identifiers.fields;
import hmis.Shared.Identifiers.reception;
import hmis.Shared.Identifiers.section;
import hmis.Shared.sharedresources.Do;
import hmis.Shared.sharedresources.rshared;
import hmis.gui.base._rbase;
import hmis.gui.base.processingdialog;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;

/**
 *
 * @author Bhuwan
 */
public class LabTest extends _rbase{

    /**
     * Creates new form LabTest
     */
processingdialog _pd;    
    public static String __name = section.labregisterpanel;
    public Do getdo(){
        return new Do(__name,this);
    }
    public void refresh(){
        this.revalidate();
        this.repaint();
    }
    
    public void requestupdate() throws hmis.Layer.core.GlobalException{
        updaterequest request = new updaterequest();
        request.type=reception.UpdateModule;
        request.section = __name;
        rshared.server.send(request,getdo());
    }
    HashMap<String,String> all_tests;
    public void pipeupdate(ReceptionModuleUpdates updates){
        _pd.kill();
        String time = updates.time;
        String[] dt = time.split("-");
        String date = dt[0]+"-"+dt[1]+"-"+dt[2];
        String tym = dt[4]+":"+dt[5]+":"+dt[6]+" "+dt[7];
        set_time_label.setText(tym);
        set_date_label.setText(date);
        all_tests=(HashMap<String,String>)updates.updates;
        //System.out.println(""+all_tests);
        set_checkbox();
    }
    public void pipequeryreply(query q){
        _pd.kill();
        HashMap<String,String> rep = (HashMap<String,String>)q.payload;
        if(rep.get(fields.issuccess).equalsIgnoreCase(Boolean.TRUE.toString())){
            JOptionPane.showMessageDialog(null, "Lab registration sucessful! Lab reg ID: "+rep.get(fields.lab_registration_no)); 
            save_button.setEnabled(false);
            billing_dialog.setBounds(290,150,430,240);
            
            //examlabel.setText((String)pl.get(fields.patientid));
            billing_dialog.setVisible(true);
            set_dialogbox();
            billing_dialog.setResizable(false);
        }else{
            JOptionPane.showMessageDialog(null, "Registration unsucessful! please try again!"); 
        }
    }
    
    private String name,age,sex,contact,address,date_ofreg,test_list,lab_regno,tests_performed;
    float cost;
    private ArrayList checkboxes;
//    private final String test[]={"Blood","Tool","Urine","eds"};
    ReceptionPanel Reception;
    public LabTest(ReceptionPanel rec) {
        Reception = rec;
        initComponents();
        test_panel.setLayout(new FlowLayout(FlowLayout.LEFT));
         _pd = new processingdialog(this, null);
        _pd.exec();
        
        
    }
     @Override
       public void enabled(Boolean flag){
            Reception.enabled(flag);
       }
    
    public void set_checkbox()
    {
       checkboxes=new ArrayList();
       JCheckBox chk;
       
//       Vector <String> vector=new Vector<String>();
//       for(String x:test)
//            vector.add(x);
//       
//       Collections.sort(vector);
      //loop for adding tests dynamically
       for (Map.Entry entry : all_tests.entrySet()) {
         //dept_combo.addItem(entry.getKey());
           chk=new JCheckBox((String)entry.getKey());
           test_panel.add(chk);
           test_panel.revalidate();
           test_panel.repaint();
           checkboxes.add(chk);
        }
    }
    
    public HashMap<String,String> packdata(){
        HashMap<String,String> details=new HashMap();
        name=name_text.getText().toString();
        age=age_text.getText().toString();
        address=address_text.getText().toString();
        contact=contact_text.getText().toString();
        date_ofreg=set_date_label.getText().toString();
        sex=sex_combobox.getSelectedItem().toString();
        
        if(name.equalsIgnoreCase("")||age.equalsIgnoreCase("")){
            JOptionPane.showMessageDialog(null,"Please fill fields properly :(");
            return null;
        }
        switch(sex){
            case "Male":
                sex="M";
                break;
            case "Female":
                sex="F";
                break;
            case "Other":
                sex="O";
                break;
        }
    //  lab_regno="210";
        details.put(fields.name, name);
        details.put(fields.age, age);
        details.put(fields.sex, sex);
        details.put(fields.address, address);
        details.put(fields.contact,contact);
        details.put(fields.registration_date,date_ofreg);
        details.put(fields.test_id,test_list);
        
    //  details.put(fields.lab_registration_no,lab_regno);
        return details;
    }
    
    public void request_for_registration()
    {
        query q = new query(codes.datainput);
        q.section = this.__name;
        q.payload = this.packdata();
        if(q!=null){
            System.out.println(""+q.payload);
            _pd = new processingdialog(this, q);
            _pd.exec();
           
        }
            
    }
    
     public void set_dialogbox()
    {
        namelabel.setText(name);
        test_label.setText(tests_performed);
        costlabel1.setText(String.valueOf(cost));
        //doctornamelabel.setText(doctor);
  
    }
    
   
     
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        billing_dialog = new javax.swing.JDialog();
        jPanel3 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        namelabel = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        test_label = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        costlabel1 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        printbutton = new javax.swing.JButton();
        labtest_panel = new javax.swing.JPanel();
        labtest_label = new javax.swing.JLabel();
        all_panel = new javax.swing.JPanel();
        time_label = new javax.swing.JLabel();
        set_time_label = new javax.swing.JLabel();
        date_label = new javax.swing.JLabel();
        set_date_label = new javax.swing.JLabel();
        name_label = new javax.swing.JLabel();
        name_text = new javax.swing.JTextField();
        ag_label = new javax.swing.JLabel();
        age_text = new javax.swing.JTextField();
        sex_label = new javax.swing.JLabel();
        sex_combobox = new javax.swing.JComboBox();
        address_label = new javax.swing.JLabel();
        address_text = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        contact_text = new javax.swing.JTextField();
        test_panel = new javax.swing.JPanel();
        save_button = new javax.swing.JButton();

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel2.setText("Name:");

        namelabel.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        namelabel.setText("name...........");

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel7.setText("Tests:");

        test_label.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        test_label.setText("doctor name.................");

        jLabel8.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel8.setText("Total Cost:");

        costlabel1.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        costlabel1.setText("8902893/-");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8))
                .addGap(22, 22, 22)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(costlabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(test_label, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(namelabel, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(124, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(namelabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(test_label))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(costlabel1))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jButton1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jButton1.setText("Ok");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        printbutton.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        printbutton.setText("Print");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(printbutton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(printbutton)
                    .addComponent(jButton1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout billing_dialogLayout = new javax.swing.GroupLayout(billing_dialog.getContentPane());
        billing_dialog.getContentPane().setLayout(billing_dialogLayout);
        billing_dialogLayout.setHorizontalGroup(
            billing_dialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        billing_dialogLayout.setVerticalGroup(
            billing_dialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 102)));

        labtest_panel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        labtest_label.setFont(new java.awt.Font("Arial Narrow", 0, 36)); // NOI18N
        labtest_label.setText("Lab Test Registration");

        javax.swing.GroupLayout labtest_panelLayout = new javax.swing.GroupLayout(labtest_panel);
        labtest_panel.setLayout(labtest_panelLayout);
        labtest_panelLayout.setHorizontalGroup(
            labtest_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(labtest_panelLayout.createSequentialGroup()
                .addGap(333, 333, 333)
                .addComponent(labtest_label)
                .addContainerGap(334, Short.MAX_VALUE))
        );
        labtest_panelLayout.setVerticalGroup(
            labtest_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(labtest_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labtest_label, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        all_panel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        time_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        time_label.setText("Time:");

        set_time_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        date_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        date_label.setText("Date:");

        set_date_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        name_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        name_label.setText("Name:");

        name_text.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        ag_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        ag_label.setText("Age:");

        age_text.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        age_text.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                age_textActionPerformed(evt);
            }
        });

        sex_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        sex_label.setText("Sex:");

        sex_combobox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        sex_combobox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Male", "Female", "Other" }));

        address_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        address_label.setText("Address:");

        address_text.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Contact:");

        contact_text.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        test_panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Test", 0, 0, new java.awt.Font("Traditional Arabic", 0, 18))); // NOI18N

        javax.swing.GroupLayout test_panelLayout = new javax.swing.GroupLayout(test_panel);
        test_panel.setLayout(test_panelLayout);
        test_panelLayout.setHorizontalGroup(
            test_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 934, Short.MAX_VALUE)
        );
        test_panelLayout.setVerticalGroup(
            test_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 85, Short.MAX_VALUE)
        );

        save_button.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        save_button.setText("Save");
        save_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                save_buttonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout all_panelLayout = new javax.swing.GroupLayout(all_panel);
        all_panel.setLayout(all_panelLayout);
        all_panelLayout.setHorizontalGroup(
            all_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(test_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(all_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(all_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(all_panelLayout.createSequentialGroup()
                        .addGroup(all_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(all_panelLayout.createSequentialGroup()
                                .addComponent(address_label)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(address_text))
                            .addGroup(all_panelLayout.createSequentialGroup()
                                .addGroup(all_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(name_label)
                                    .addComponent(sex_label)
                                    .addComponent(time_label))
                                .addGap(28, 28, 28)
                                .addGroup(all_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(set_time_label)
                                    .addComponent(sex_combobox, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(name_text, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(all_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(date_label)
                            .addComponent(ag_label)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(all_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(set_date_label)
                            .addComponent(age_text, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(contact_text, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(211, 211, 211))
                    .addGroup(all_panelLayout.createSequentialGroup()
                        .addComponent(save_button)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        all_panelLayout.setVerticalGroup(
            all_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(all_panelLayout.createSequentialGroup()
                .addGap(74, 74, 74)
                .addGroup(all_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(time_label)
                    .addComponent(set_time_label, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(date_label)
                    .addComponent(set_date_label))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(all_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(name_label)
                    .addComponent(name_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ag_label)
                    .addComponent(age_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(all_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sex_label)
                    .addComponent(sex_combobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(contact_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(all_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(address_label)
                    .addComponent(address_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(test_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(save_button)
                .addContainerGap(67, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(labtest_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(all_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(labtest_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(all_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void age_textActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_age_textActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_age_textActionPerformed

    private void save_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_save_buttonActionPerformed
        // TODO add your handling code here:
        test_list="";
        tests_performed="";
         cost=0;
        for (Iterator it = checkboxes.iterator(); it.hasNext();) {
            JCheckBox ck = (JCheckBox) it.next();
            if(ck.isSelected()){
            test_list += all_tests.get(ck.getText().toString()).split(",")[0]+",";
            cost+= Float.parseFloat(all_tests.get(ck.getText().toString()).split(",")[1]);
            tests_performed+=ck.getText().toString()+",";
            }
        }
        if(test_list.equalsIgnoreCase("")){
            JOptionPane.showMessageDialog(null,"Please select atleast one test! :(");
            return;
        }
        request_for_registration();
    }//GEN-LAST:event_save_buttonActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        billing_dialog.dispose();
       // refresh_save();
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel address_label;
    private javax.swing.JTextField address_text;
    private javax.swing.JLabel ag_label;
    private javax.swing.JTextField age_text;
    private javax.swing.JPanel all_panel;
    private javax.swing.JDialog billing_dialog;
    private javax.swing.JTextField contact_text;
    private javax.swing.JLabel costlabel1;
    private javax.swing.JLabel date_label;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JLabel labtest_label;
    private javax.swing.JPanel labtest_panel;
    private javax.swing.JLabel name_label;
    private javax.swing.JTextField name_text;
    private javax.swing.JLabel namelabel;
    private javax.swing.JButton printbutton;
    private javax.swing.JButton save_button;
    private javax.swing.JLabel set_date_label;
    private javax.swing.JLabel set_time_label;
    private javax.swing.JComboBox sex_combobox;
    private javax.swing.JLabel sex_label;
    private javax.swing.JLabel test_label;
    private javax.swing.JPanel test_panel;
    private javax.swing.JLabel time_label;
    // End of variables declaration//GEN-END:variables
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package hmis.gui.Reception;

import hmis.Shared.Commobjects.primitive.ReceptionModuleUpdates;
import hmis.Shared.Commobjects.primitive.updaterequest;
import hmis.Shared.Commobjects.standard.query;
import hmis.Shared.Identifiers.codes;
import hmis.Shared.Identifiers.fields;
import hmis.Shared.Identifiers.reception;
import hmis.Shared.Identifiers.request_types;
import hmis.Shared.Identifiers.section;
import hmis.Shared.sharedresources.Do;
import hmis.Shared.sharedresources.rshared;
import hmis.gui.base._rbase;
import hmis.gui.base.processingdialog;
import hmis.gui.main.ValidateInput;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import javax.swing.JOptionPane;

/**
 *
 * @author Rupesh
 */

/**
 *
 * @author Rupesh
 */
public class OPD_RegistrationPanel extends _rbase {
    public static String __name = section.opdregisterpanel;
    Boolean isNew = false;
    processingdialog _pd;
    public String examid;
    
    @Override
    public Do getdo(){
        return new Do(__name,this);
    }
    
    @Override
    public void requestupdate() throws hmis.Layer.core.GlobalException{
        updaterequest request = new updaterequest();
        request.section = __name;
        request.type=request_types.rq_available_doctors;
        rshared.server.send(request,getdo());
    }
    
    @Override
    public void pipeupdate(ReceptionModuleUpdates updates){
        _pd.kill();
        switch(updates.type){
            case request_types.rq_available_doctors:
                if(!is_old_patient){
                    String gt = updates.time;     
                    String[] parts = gt.split(";");
                    String time = parts[0];
                    String[] dt = time.split("-");
                    String date = dt[0]+"-"+dt[1]+"-"+dt[2];
                    String tym = dt[4]+":"+dt[5]+":"+dt[6]+" "+dt[7];
                     date_text.setText(date);
                     time_text.setText(tym);
                     reg_text.setText(parts[1].toString());
                     this.examid = parts[2].toString();
                     examid_label.setText(this.examid);
                }
                    available_docs=(HashMap)updates.updates;
                    dept_combo.removeAllItems();
                    initialize_combo_from_hashmap();
                    break;
        }
    }
    
    @Override
    public void pipequeryreply(query q){
        HashMap<String,Object> pl = (HashMap<String,Object>)q.payload;
        _pd.kill();
        if((Boolean)pl.get(fields.issuccess)){
            //means we got a positive reply
            String pid = (String)pl.get(fields.patientid);
            JOptionPane.showMessageDialog(null,"Registration process sucessful! :)\npatient: "+(String)pl.get(fields.name)+"\nPatient ID: "+(String)pl.get(fields.patientid)+"\nExam. ID: "+(String)pl.get(fields.examination_id));
            set_dialogbox();
            isNew = true;
            save_button.setText("New");
            //for billing dialog
            billing_dialog.setBounds(290,150,505,325);
          
            examlabel.setText((String)pl.get(fields.examination_id));
            patient_idlabel.setText((String)pl.get(fields.patientid));
            billing_dialog.setVisible(true);
            billing_dialog.setResizable(false);
            
            
            
        }else{
            //we were unsucessful in the process
            JOptionPane.showMessageDialog(null,"Registration process unsucessful, please try again! :(");
        }
    }
    /**
     * Creates new form OPD_RegistrationPanel
     */
    
      public boolean processingdialogtimeoutreached = false;
      boolean is_old_patient;
        HashMap<String,Object> available_docs=new HashMap();
        // variables to store data from form field
        String reg;
        String name;
        String age;
        int age_num; 
        String sex;
        String address;
        String contact;
        String department;
        String doctor;
        int doc_id;
    //THIS CONTRUSTOR IS USED IN CASE OF OPD REGISTRATION
        ReceptionPanel Reception;
        @Override
       public void enabled(Boolean flag){
            Reception.enabled(flag);
       }
    public OPD_RegistrationPanel(ReceptionPanel recp) throws hmis.Layer.core.GlobalException{
        Reception = recp;
        initComponents();
        //this sleep is temporary and is used to set delay until data arrives from DB
        is_old_patient=false;
        
        
        _pd = new processingdialog(this, null);
        _pd.exec();
        
    }
    //this constructor is used for patient registration via appointment 
    private ReceptionPanel rp;
    

    public OPD_RegistrationPanel(String _pat_name,String _contact,String _doctorid,String _department,ReceptionPanel e) throws hmis.Layer.core.GlobalException{
        Reception = rp=e;
        initComponents();
        _pd = new processingdialog(this, null);
        _pd.exec();
         if(_pd.iskilled()){
             name_text.setText(_pat_name);
            contact_text.setText(_contact);
         }
        
    }
    
    //This constructor is used in case of OLD PATIENT REGISTRATION
    public OPD_RegistrationPanel(ReceptionPanel rec,String Reg,String Name,String Address,String Sex,String Contact) throws hmis.Layer.core.GlobalException {
        Reception = rec;
        initComponents();
        
        is_old_patient=false;
        _pd = new processingdialog(this, null);
        _pd.exec();
        if(!_pd.iskilled()){
            return;
        }
//        initialize_combo_from_hashmap();

        is_old_patient=true;
        //set the values 
        reg_text.setText(Reg);
        name_text.setText(Name);
        name_text.setEditable(false);
        address_text.setText(Address);
        switch(Sex){
            case "M":
                sex_combo.setSelectedIndex(0);
                break;
                
            case "F":
                sex_combo.setSelectedIndex(1);
                break;
                
             default:
                sex_combo.setSelectedIndex(2);
                 break;
        }
        contact_text.setText(Contact);
    }
    
    public void initialize_combo_from_hashmap()
    { 
       for (Map.Entry entry : available_docs.entrySet()) {
           dept_combo.addItem(entry.getKey());      
        }
    }
    
    public final void initializeDateAndTime(String Date,String Time) {
        date_text.setText(Date);
        time_text.setText(Time);
    }
   
    public void grabformdata(){
        name = name_text.getText().trim().toUpperCase();
        age = age_text.getText().trim();
        sex = sex_combo.getSelectedItem().toString();
        if(sex.equalsIgnoreCase("Male"))
        {
            sex="M";
        }
        else if(sex.equalsIgnoreCase("Female"))
        {
            sex="F";
        }     
        else if(sex.equalsIgnoreCase("Other"))
        {
            sex="O";
        }
        address = address_text.getText().trim();
        contact = contact_text.getText().trim();
        department = dept_combo.getSelectedItem().toString();
        doctor = (String) doctor_combo.getSelectedItem();
        
        Vector<HashMap<String,String>> same_dept_docs=(Vector<HashMap<String,String>>) available_docs.get(department);
        for(int i=0;i<same_dept_docs.size();i++)
        {
            if(doctor.equals(same_dept_docs.get(i).get(fields.doctor)))
            {
                doc_id=Integer.parseInt(same_dept_docs.get(i).get(fields.doctorid));
            }
        }
        
        System.out.println(""+doc_id);
    }
    public void action_on_save_button_click()
    {
        if(!isNew){
                //get all data from the opd forms
        this.grabformdata();
        //Now validate the data
        if(validate_input_data()){
            //now send these data to the server for further process
            //create the server query here
                //set timeout to false
            
            this.processingdialogtimeoutreached = true;
            query q = new query(codes.datainput);
            q.section = this.__name;
            
            if(is_old_patient){
                q.code=reception.OldPatientreRegister;
            }
            else{
                q.code = reception.OPDNewPatientRegister;
            }
            q.payload = this.packdata();
     
         
            _pd = new processingdialog(this,q);       
            _pd.exec();
            if(!_pd.iskilled()){
                save_button.setEnabled(false);
            }
      }  
     
        }else{
            Reception.action_on_opdregistration_click();
        }
        
    }
    private HashMap<String,Object> packdata(){
        //here data will be packed to be sent to the core
        HashMap<String,Object> data = new HashMap();
            data.put(fields.name, name);  
            data.put(fields.age, age);
            data.put(fields.sex, sex); 
            data.put(fields.address, address);
            data.put(fields.contact, contact);
            data.put(fields.department,department);
            data.put(fields.doctor,doctor);
            data.put(fields.doctorid,String.valueOf(doc_id));
            data.put(fields.bloodgroup,"");
            data.put(fields.diagnosed_with,"");
            data.put(fields.patientid,reg_text.getText().toString());
            data.put(fields.examination_id, this.examid);
            data.put(fields.date_of_first_examination,date_text.getText().toString());
            //data.put(fields.date_of_first_examination,"2070-10-11");
            
            return data;
    }
        
        /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        billing_dialog = new javax.swing.JDialog();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        namelabel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        agelabel = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        sexlabel = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        examlabel = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        doctornamelabel = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        patient_idlabel = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        costlabel1 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        printbutton = new javax.swing.JButton();
        opd_panel = new javax.swing.JPanel();
        opd_label = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        reg_label = new javax.swing.JLabel();
        reg_text = new javax.swing.JLabel();
        date_label = new javax.swing.JLabel();
        date_text = new javax.swing.JLabel();
        time_label = new javax.swing.JLabel();
        time_text = new javax.swing.JLabel();
        name_label = new javax.swing.JLabel();
        name_text = new javax.swing.JTextField();
        age_label = new javax.swing.JLabel();
        age_text = new javax.swing.JTextField();
        sex_label = new javax.swing.JLabel();
        sex_combo = new javax.swing.JComboBox();
        address_label = new javax.swing.JLabel();
        address_text = new javax.swing.JTextField();
        contact_label = new javax.swing.JLabel();
        contact_text = new javax.swing.JTextField();
        dept_label = new javax.swing.JLabel();
        dept_combo = new javax.swing.JComboBox();
        doctor_label = new javax.swing.JLabel();
        doctor_combo = new javax.swing.JComboBox();
        examinationid_label = new javax.swing.JLabel();
        examid_label = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        save_button = new javax.swing.JButton();

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 30)); // NOI18N
        jLabel4.setText("Opd Billing");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addGap(141, 141, 141))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jLabel4)
                .addGap(0, 12, Short.MAX_VALUE))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel1.setText("Name:");

        namelabel.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        namelabel.setText("name...........");

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel2.setText("Age:");

        agelabel.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        agelabel.setText("age");

        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel3.setText("Sex:");

        sexlabel.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        sexlabel.setText("sex.....");

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel5.setText("Exam ID :");

        examlabel.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        examlabel.setText("890304-");

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel6.setText("Doctor:");

        doctornamelabel.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        doctornamelabel.setText("doctor name.................");

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel7.setText("Total Cost:");

        patient_idlabel.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        patient_idlabel.setText("890289");

        jLabel8.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel8.setText("Patient ID:");

        costlabel1.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        costlabel1.setText("350");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addGap(22, 22, 22)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(agelabel, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(namelabel, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(doctornamelabel, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(costlabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20)
                        .addComponent(sexlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(examlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(patient_idlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(6, 6, 6))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(namelabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(agelabel)
                    .addComponent(jLabel3)
                    .addComponent(sexlabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(doctornamelabel)
                    .addComponent(jLabel5)
                    .addComponent(examlabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(patient_idlabel)
                    .addComponent(costlabel1))
                .addContainerGap(29, Short.MAX_VALUE))
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jButton1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jButton1.setText("Ok");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        printbutton.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        printbutton.setText("Print");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(printbutton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(printbutton)
                    .addComponent(jButton1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout billing_dialogLayout = new javax.swing.GroupLayout(billing_dialog.getContentPane());
        billing_dialog.getContentPane().setLayout(billing_dialogLayout);
        billing_dialogLayout.setHorizontalGroup(
            billing_dialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        billing_dialogLayout.setVerticalGroup(
            billing_dialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 204, 255)));

        opd_panel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        opd_panel.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N

        opd_label.setFont(new java.awt.Font("Arial Narrow", 0, 36)); // NOI18N
        opd_label.setText("OPD Registration");

        javax.swing.GroupLayout opd_panelLayout = new javax.swing.GroupLayout(opd_panel);
        opd_panel.setLayout(opd_panelLayout);
        opd_panelLayout.setHorizontalGroup(
            opd_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(opd_panelLayout.createSequentialGroup()
                .addGap(356, 356, 356)
                .addComponent(opd_label)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        opd_panelLayout.setVerticalGroup(
            opd_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(opd_panelLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(opd_label)
                .addContainerGap(22, Short.MAX_VALUE))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        reg_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        reg_label.setText("Patient no.");

        reg_text.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        date_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        date_label.setText("Date:");

        date_text.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        time_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        time_label.setText("Time:");

        time_text.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        name_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        name_label.setText("Name:");

        name_text.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        age_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        age_label.setText("Age:");

        age_text.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        sex_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        sex_label.setText("Sex:");

        sex_combo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        sex_combo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Male", "Female", "Other" }));

        address_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        address_label.setText("Address:");

        address_text.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        contact_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        contact_label.setText("Contact:");

        contact_text.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        dept_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        dept_label.setText("Dept:");

        dept_combo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        dept_combo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dept_comboActionPerformed(evt);
            }
        });

        doctor_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        doctor_label.setText("Doctor:");

        doctor_combo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        doctor_combo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                doctor_comboActionPerformed(evt);
            }
        });

        examinationid_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        examinationid_label.setText("Examination id. ");

        examid_label.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(reg_label)
                            .addComponent(date_label))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addComponent(date_text)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(time_label)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(time_text)
                                .addGap(145, 145, 145))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(reg_text, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(66, 66, 66)
                                .addComponent(examinationid_label)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(examid_label, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(doctor_label)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(doctor_combo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(address_label)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(address_text, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(contact_label)
                                    .addComponent(dept_label))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(contact_text)
                                    .addComponent(dept_combo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(name_label)
                            .addComponent(age_label))
                        .addGap(27, 27, 27)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(age_text, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 108, Short.MAX_VALUE)
                                .addComponent(sex_label)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(sex_combo, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(name_text))
                        .addGap(578, 578, 578))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(reg_label)
                    .addComponent(reg_text)
                    .addComponent(examinationid_label)
                    .addComponent(examid_label))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(date_label)
                    .addComponent(date_text)
                    .addComponent(time_label)
                    .addComponent(time_text))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(name_label)
                    .addComponent(name_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(age_label)
                    .addComponent(age_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sex_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sex_label))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(address_label)
                    .addComponent(address_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(contact_label)
                    .addComponent(contact_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dept_label)
                    .addComponent(dept_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(doctor_label)
                    .addComponent(doctor_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        save_button.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        save_button.setText("Save");
        save_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                save_buttonActionPerformed(evt);
            }
        });
        save_button.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                save_buttonKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(save_button, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(848, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(save_button)
                .addContainerGap(40, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(opd_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(opd_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void save_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_save_buttonActionPerformed
      action_on_save_button_click();
    }//GEN-LAST:event_save_buttonActionPerformed

    private void doctor_comboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_doctor_comboActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_doctor_comboActionPerformed

    private void save_buttonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_save_buttonKeyPressed
      if(evt.getKeyCode()== KeyEvent.VK_ENTER)
          action_on_save_button_click();
        // TODO add your handling code here:
    }//GEN-LAST:event_save_buttonKeyPressed

    private void dept_comboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dept_comboActionPerformed
        // TODO add your handling code here:
        String selected = (String) dept_combo.getSelectedItem();
        for(int i=0;i<available_docs.keySet().toArray().length;i++){
            String dept_name=(String)available_docs.keySet().toArray()[i];
            if(selected.equals(dept_name))
            {
                Vector<HashMap<String,String>> same_dept_docs=((Vector)available_docs.get(dept_name));
                doctor_combo.removeAllItems();
                for(int j=0;j<same_dept_docs.size();j++)
                {   
                 doctor_combo.addItem(same_dept_docs.get(j).get(fields.doctor));
                }
            } 
        }
////        switch(selected)
////        {
////            
////            case "str":
////                doctor_combo.removeAllItems();
////                doctor_combo.addItem("Dr. khale");
////                break;            
////        }
    }//GEN-LAST:event_dept_comboActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        billing_dialog.dispose();
        refresh_save();
    }//GEN-LAST:event_jButton1ActionPerformed
    
//    public void get_all_OPD_data()
//    {   
//        
//    }
    
    public boolean validate_input_data()
    {
        if(!ValidateInput.validateName(name)) {
            JOptionPane.showMessageDialog(null, "INVALID  NAME", "INPUT ERROR", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
        else if(!ValidateInput.validateAge(age)) {
            JOptionPane.showMessageDialog(null, "INVALID  AGE", "INPUT ERROR", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
        else if(!ValidateInput.validateAddress(address)) {
            JOptionPane.showMessageDialog(null, "PLEASE  ENTER  ADDRESS", "INPUT ERROR", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
        else if(!ValidateInput.validateContact(contact)) {
            JOptionPane.showMessageDialog(null, "INVALID  CONTACT", "INPUT ERROR", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
      
        
        return true;
    }
    
   
    //function to reset the form fields after registration is complete
    public void reset_all(){
        name_text.setText("");
        age_text.setText("");
        reg_text.setText("");
        address_text.setText("");
        contact_text.setText("");
            
    }
    
    public void refresh_save()
    {
        
        //reg_text.setText("");
        
        
       // examid_label.setText("");
        //time_text.setText("");
        //date_text.setText("");
        
    }
    
     public void set_dialogbox()
    {
        namelabel.setText(name);
        agelabel.setText(age);
        sexlabel.setText(sex);
        doctornamelabel.setText(doctor);
        
        
    }
    
   
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel address_label;
    private javax.swing.JTextField address_text;
    private javax.swing.JLabel age_label;
    private javax.swing.JTextField age_text;
    private javax.swing.JLabel agelabel;
    private javax.swing.JDialog billing_dialog;
    private javax.swing.JLabel contact_label;
    private javax.swing.JTextField contact_text;
    private javax.swing.JLabel costlabel1;
    private javax.swing.JLabel date_label;
    private javax.swing.JLabel date_text;
    private javax.swing.JComboBox dept_combo;
    private javax.swing.JLabel dept_label;
    private javax.swing.JComboBox doctor_combo;
    private javax.swing.JLabel doctor_label;
    private javax.swing.JLabel doctornamelabel;
    private javax.swing.JLabel examid_label;
    private javax.swing.JLabel examinationid_label;
    private javax.swing.JLabel examlabel;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JLabel name_label;
    private javax.swing.JTextField name_text;
    private javax.swing.JLabel namelabel;
    private javax.swing.JLabel opd_label;
    private javax.swing.JPanel opd_panel;
    private javax.swing.JLabel patient_idlabel;
    private javax.swing.JButton printbutton;
    private javax.swing.JLabel reg_label;
    private javax.swing.JLabel reg_text;
    private javax.swing.JButton save_button;
    private javax.swing.JComboBox sex_combo;
    private javax.swing.JLabel sex_label;
    private javax.swing.JLabel sexlabel;
    private javax.swing.JLabel time_label;
    private javax.swing.JLabel time_text;
    // End of variables declaration//GEN-END:variables

}   

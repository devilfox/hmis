/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.gui.Reception;

import hmis.Shared.Commobjects.primitive.ReceptionModuleUpdates;
import hmis.Shared.Commobjects.primitive.updaterequest;
import hmis.Shared.Identifiers.section;
import hmis.Shared.sharedresources.Do;
import hmis.Shared.sharedresources.rshared;
import hmis.gui.base._rbase;
import hmis.gui.main.ValidateInput;
import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EtchedBorder;

/**
 *
 * @author Rupesh
 */
public class OldPatientsMainPanel extends _rbase {
    public static String __name = section.oldpatientpanel;
    public Do getdo(){
        return new Do(__name,this);
    }
    public void refresh(){
        this.revalidate();
        this.refresh();
    }
    public void requestupdate() throws hmis.Layer.core.GlobalException{
        updaterequest request = new updaterequest();
        request.section = __name;
        rshared.server.send(request,new Do(__name,this));
    }
    public void pipeupdate(ReceptionModuleUpdates updates){
        
    }

    SearchByNamePanel search_by_name_panel = new SearchByNamePanel();
    SearchByIDPanel search_by_id_panel = new SearchByIDPanel();
    SearchPatientsResultPanel search_patient_result_panel = new SearchPatientsResultPanel();
    
    String name,reg_no,sex,address,contact;
    int reg;
    Boolean select;
    /**
     * Creates new form SearchPatientPanel
     */
    public OldPatientsMainPanel() {
        initComponents();
        searchButton.setVisible(false);
        
    }
    
    
    public OldPatientsMainPanel(ReceptionPanel reception) {
        initComponents(reception);
        
        
        //this object is created locally py passing section of OldPatientMainPanel and ReceptionPanel
        SearchByNamePanel search_by_name_panel2 = new SearchByNamePanel(this,reception);
        SearchByIDPanel search_by_id_panel2 = new SearchByIDPanel(this,reception);
        
        //then the created object is assigned to other object which is global
        search_by_name_panel = search_by_name_panel2;
        search_by_id_panel = search_by_id_panel2;
        searchButton.setVisible(false);
        
   //     initializeEvents(reception);    //initialize the table events
     //   all_old_patient_search_table.setRowHeight(30);
    }
   
    public void action_on_searchbyname_button_event()
    {
            search_by_name_panel.set_name("");
            searchPatientResultBasePanel.removeAll();
            enter_info_panel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
            enter_info_panel.setLayout(new BorderLayout());
            enter_info_panel.removeAll();
            enter_info_panel.add(search_by_name_panel);
            this.revalidate();
            this.repaint();
            searchButton.setVisible(true);
    }
   
    //for search by id action 
    public void action_on_searchbyid_button_event()
    {
         search_by_id_panel.set_registration_no("");
            searchPatientResultBasePanel.removeAll();
            enter_info_panel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
            enter_info_panel.setLayout(new BorderLayout());
            enter_info_panel.removeAll();
            enter_info_panel.add(search_by_id_panel);
            
            searchButton.setVisible(true);
            this.revalidate();
            this.repaint();
    }
    
    
    //method for search button (when search by name is selected)
      public void action_on_search_button_searchbyname_event(ReceptionPanel r)
    {
               name = search_by_name_panel.get_name();
                //check whether the name is valid or not
                if(!ValidateInput.validateName(name)){
                   JOptionPane.showMessageDialog(null, "ENTER  A  VALID  NAME", "INPUT ERROR", JOptionPane.ERROR_MESSAGE);
                }
                else{
                search_by_id_radioButton.setSelected(false);
                searchPatientResultBasePanel.add(new OldPatientsSearchPanel(name,r));
                }

    }
      public void action_on_search_button_searchbyid_event(ReceptionPanel r)
      {
           try{
                    reg = search_by_id_panel.get_registration_no();
                   }
                catch(NumberFormatException e)
                    {
                        JOptionPane.showMessageDialog(null, "ENTER  A  VALID  REGISTRATION  NO.", "INPUT ERROR", JOptionPane.ERROR_MESSAGE);
                    }
                search_by_name_radioButton.setSelected(false);
                searchPatientResultBasePanel.add(new OldPatientsSearchPanel(reg,r));
      }
      
      
      //method when keyevent is detected in text field
    public void action_on_search_button_event(ReceptionPanel r)
    {
        searchPatientResultBasePanel.removeAll();
         if(select)
         {
             action_on_search_button_searchbyname_event(r);
         }
         else
         {
             action_on_search_button_searchbyid_event(r);
         }
       this.revalidate();
       this.repaint();
      
    }
    
   
            

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        search_by_name_radioButton = new javax.swing.JRadioButton();
        search_by_id_radioButton = new javax.swing.JRadioButton();
        enter_info_panel = new javax.swing.JPanel();
        searchButton = new javax.swing.JButton();
        searchPatientResultBasePanel = new javax.swing.JPanel();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(190, 172, 172)));

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SEARCH AN OLD PATIENT", 0, 0, new java.awt.Font("Andalus", 0, 12))); // NOI18N
        jPanel1.setPreferredSize(new java.awt.Dimension(928, 155));

        jLabel1.setFont(new java.awt.Font("Traditional Arabic", 0, 18)); // NOI18N
        jLabel1.setText("Search By");

        buttonGroup1.add(search_by_name_radioButton);
        search_by_name_radioButton.setFont(new java.awt.Font("Trebuchet MS", 0, 14)); // NOI18N
        search_by_name_radioButton.setText("Name");
        search_by_name_radioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                search_by_name_radioButtonActionPerformed(evt);
            }
        });
        search_by_name_radioButton.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                search_by_name_radioButtonKeyPressed(evt);
            }
        });

        buttonGroup1.add(search_by_id_radioButton);
        search_by_id_radioButton.setFont(new java.awt.Font("Trebuchet MS", 0, 14)); // NOI18N
        search_by_id_radioButton.setText("Reg No.");
        search_by_id_radioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                search_by_id_radioButtonActionPerformed(evt);
            }
        });
        search_by_id_radioButton.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                search_by_id_radioButtonKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout enter_info_panelLayout = new javax.swing.GroupLayout(enter_info_panel);
        enter_info_panel.setLayout(enter_info_panelLayout);
        enter_info_panelLayout.setHorizontalGroup(
            enter_info_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 500, Short.MAX_VALUE)
        );
        enter_info_panelLayout.setVerticalGroup(
            enter_info_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 57, Short.MAX_VALUE)
        );

        searchButton.setText("Search");
        searchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchButtonActionPerformed(evt);
            }
        });
        searchButton.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                searchButtonKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(enter_info_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(39, 39, 39)
                        .addComponent(searchButton))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(search_by_name_radioButton)
                        .addGap(18, 18, 18)
                        .addComponent(search_by_id_radioButton)))
                .addContainerGap(302, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(search_by_name_radioButton)
                    .addComponent(search_by_id_radioButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(enter_info_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(searchButton)
                        .addGap(24, 24, 24))))
        );

        searchPatientResultBasePanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout searchPatientResultBasePanelLayout = new javax.swing.GroupLayout(searchPatientResultBasePanel);
        searchPatientResultBasePanel.setLayout(searchPatientResultBasePanelLayout);
        searchPatientResultBasePanelLayout.setHorizontalGroup(
            searchPatientResultBasePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 924, Short.MAX_VALUE)
        );
        searchPatientResultBasePanelLayout.setVerticalGroup(
            searchPatientResultBasePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 346, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(searchPatientResultBasePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(searchPatientResultBasePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void search_by_name_radioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_search_by_name_radioButtonActionPerformed
        // TODO add your handling code here:
        
      searchPatientResultBasePanel.setLayout(new BorderLayout());
        if(search_by_name_radioButton.isSelected())
        {
           action_on_searchbyname_button_event();
        }
        
    }//GEN-LAST:event_search_by_name_radioButtonActionPerformed

    private void search_by_id_radioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_search_by_id_radioButtonActionPerformed
        // TODO add your handling code here:
       searchPatientResultBasePanel.setLayout(new BorderLayout());
        if(search_by_id_radioButton.isSelected())
        {
         action_on_searchbyid_button_event();
        }
    }//GEN-LAST:event_search_by_id_radioButtonActionPerformed

    //This method is not used. so no need to modify it.
    //The following method is overridden just below which will be used......
    private void searchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchButtonActionPerformed
        // TODO add your handling code here:
      
    }//GEN-LAST:event_searchButtonActionPerformed

    private void search_by_name_radioButtonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_search_by_name_radioButtonKeyPressed
     
        if(evt.getKeyCode()== KeyEvent.VK_ENTER)
       {
           select =true;
            searchPatientResultBasePanel.setLayout(new BorderLayout());
         action_on_searchbyname_button_event();
        
       }
    }//GEN-LAST:event_search_by_name_radioButtonKeyPressed

    private void search_by_id_radioButtonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_search_by_id_radioButtonKeyPressed
       if(evt.getKeyCode()== KeyEvent.VK_ENTER)
       {
           select=false;
          searchPatientResultBasePanel.setLayout(new BorderLayout());
          action_on_searchbyid_button_event();
       }
       
        // TODO add your handling code here:
                                                     

    }//GEN-LAST:event_search_by_id_radioButtonKeyPressed

    private void searchButtonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchButtonKeyPressed
       
    }//GEN-LAST:event_searchButtonKeyPressed

    private void searchButtonKeyPressed(java.awt.event.KeyEvent evt,ReceptionPanel r) {                                        
        if(evt.getKeyCode()==KeyEvent.VK_ENTER)
        {
           action_on_search_button_event(r);
        }
       
    } 
    
    // override method searchButtonActionParformed. It will be used
    private void searchButtonActionPerformed(java.awt.event.ActionEvent evt,ReceptionPanel r) {                                             
         // TODO add your handling code here:
        
         
        //Initialise Search Panel
            searchPatientResultBasePanel.removeAll();
            
            //is search by name is selected
            if(search_by_name_radioButton.isSelected()) {
                name = search_by_name_panel.get_name();
                if(!ValidateInput.validateName(name)){
                   JOptionPane.showMessageDialog(null, "ENTER  A  VALID  NAME", "INPUT ERROR", JOptionPane.ERROR_MESSAGE); 
                }
                else{
                search_by_id_radioButton.setSelected(false);
                searchPatientResultBasePanel.add(new OldPatientsSearchPanel(name,r));
                }
            }
            
            //else if search by id is selected
            else if(search_by_id_radioButton.isSelected()){
                try{
                    reg = search_by_id_panel.get_registration_no();
                    
                    search_by_name_radioButton.setSelected(false);               
                    searchPatientResultBasePanel.add(new OldPatientsSearchPanel(reg,r));
                 }
                catch(NumberFormatException e)
                    {
                        JOptionPane.showMessageDialog(null, "ENTER  A  VALID  REGISTRATION  NO.", "INPUT ERROR", JOptionPane.ERROR_MESSAGE);
                    }
                
                
            }
                
            this.revalidate();
            this.repaint();
    }                       
    
    public JPanel get_search_patient_base_panel()
    {
        return searchPatientResultBasePanel;
    }
    
    public Boolean Is_search_by_name_selected()
    {
        if(search_by_name_radioButton.isSelected())
        {
            search_by_id_radioButton.setSelected(false);
            return true;
        }
        else if(search_by_id_radioButton.isSelected())
        {
            search_by_name_radioButton.setSelected(false);
            return false;
        }
        
        return true;
    }
    
    public JRadioButton get_search_by_name_radiobutton()
    {
        return search_by_name_radioButton;
    }
    
    public JRadioButton get_search_by_id_radiobutton()
    {
        return search_by_id_radioButton;
    }
    //self generated code section
    //No need to modify
    //----------------------------------------------------------------------------------------------------------------------//
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JPanel enter_info_panel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton searchButton;
    private javax.swing.JPanel searchPatientResultBasePanel;
    private javax.swing.JRadioButton search_by_id_radioButton;
    private javax.swing.JRadioButton search_by_name_radioButton;
    // End of variables declaration//GEN-END:variables

   // override initcomponents
    private void initComponents(final ReceptionPanel reception) {
        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        search_by_name_radioButton = new javax.swing.JRadioButton();
        search_by_id_radioButton = new javax.swing.JRadioButton();
        enter_info_panel = new javax.swing.JPanel();
        searchButton = new javax.swing.JButton();
        searchPatientResultBasePanel = new javax.swing.JPanel();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(190, 172, 172)));

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SEARCH AN OLD PATIENT", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Andalus", 0, 12))); // NOI18N
        jPanel1.setPreferredSize(new java.awt.Dimension(928, 155));

        jLabel1.setFont(new java.awt.Font("Traditional Arabic", 0, 18)); // NOI18N
        jLabel1.setText("Search By");

        buttonGroup1.add(search_by_name_radioButton);
        search_by_name_radioButton.setFont(new java.awt.Font("Trebuchet MS", 0, 14)); // NOI18N
        search_by_name_radioButton.setText("Name");
        search_by_name_radioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                search_by_name_radioButtonActionPerformed(evt);
            }
        });
        search_by_name_radioButton.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                search_by_name_radioButtonKeyPressed(evt);
            }
        });

        buttonGroup1.add(search_by_id_radioButton);
        search_by_id_radioButton.setFont(new java.awt.Font("Trebuchet MS", 0, 14)); // NOI18N
        search_by_id_radioButton.setText("Reg No.");
        search_by_id_radioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                search_by_id_radioButtonActionPerformed(evt);
            }
        });
        search_by_id_radioButton.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                search_by_id_radioButtonKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout enter_info_panelLayout = new javax.swing.GroupLayout(enter_info_panel);
        enter_info_panel.setLayout(enter_info_panelLayout);
        enter_info_panelLayout.setHorizontalGroup(
            enter_info_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 500, Short.MAX_VALUE)
        );
        enter_info_panelLayout.setVerticalGroup(
            enter_info_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 57, Short.MAX_VALUE)
        );

        searchButton.setText("Search");
        searchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchButtonActionPerformed(evt,reception);
            }
        });
        searchButton.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                searchButtonKeyPressed(evt,reception);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(enter_info_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(39, 39, 39)
                        .addComponent(searchButton))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(search_by_name_radioButton)
                        .addGap(18, 18, 18)
                        .addComponent(search_by_id_radioButton)))
                .addContainerGap(302, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(search_by_name_radioButton)
                    .addComponent(search_by_id_radioButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(enter_info_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(searchButton)
                        .addGap(24, 24, 24))))
        );

        searchPatientResultBasePanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout searchPatientResultBasePanelLayout = new javax.swing.GroupLayout(searchPatientResultBasePanel);
        searchPatientResultBasePanel.setLayout(searchPatientResultBasePanelLayout);
        searchPatientResultBasePanelLayout.setHorizontalGroup(
            searchPatientResultBasePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 924, Short.MAX_VALUE)
        );
        searchPatientResultBasePanelLayout.setVerticalGroup(
            searchPatientResultBasePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 346, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(searchPatientResultBasePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(searchPatientResultBasePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }
}

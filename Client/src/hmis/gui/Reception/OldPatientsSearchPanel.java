                                                                                                                                                                                                                             /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.gui.Reception;

import hmis.Layer.DB.DBManager;
import hmis.Layer.core.GlobalException;
import hmis.Shared.Commobjects.primitive.ReceptionModuleUpdates;
import hmis.Shared.Commobjects.primitive.updaterequest;
import hmis.Shared.Commobjects.standard.query;
import hmis.Shared.Identifiers.codes;
import hmis.Shared.Identifiers.fields;
import hmis.Shared.Identifiers.reception;
import hmis.Shared.Identifiers.section;
import hmis.Shared.sharedresources.Do;
import hmis.Shared.sharedresources.rshared;
import hmis.gui.base._rbase;
import hmis.gui.base.processingdialog;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Rupesh
 */
public class OldPatientsSearchPanel extends _rbase{

    /**
     * Creates new form NewJPanel
     */
    
    public static String __name = section.oldpatientsearchpanel;
    public Do getdo(){
        return new Do(__name,this);
    }
    public void refresh(){
        this.revalidate();
        this.refresh();
    }
    public void requestupdate() throws hmis.Layer.core.GlobalException{
        updaterequest request = new updaterequest();
        request.section = __name;
        rshared.server.send(request,new Do(__name,this));
    }
    public void pipeupdate(ReceptionModuleUpdates updates){
        
        
    }
    public void pipequeryreply(query q)
    {
        _pd.kill();
        
        HashMap<String,List<HashMap<String,String>>> info=(HashMap<String,List<HashMap<String,String>>>) q.payload; 
        patient_list=info.get(fields.patient_info);
        if(patient_list==null || patient_list.size()<1){
            JOptionPane.showMessageDialog(null,"No results found!");
        }else{
            display_result_table(patient_list,recept);
        }
        
    }
    
    
    String reg_no,name,address,sex,contact;
    int reg;
    DBManager db1=null;
    List<HashMap<String,String>> patient_list=new ArrayList();
    public OldPatientsSearchPanel() {
        initComponents();
    }
    
    //if search is made on the basis of registration no.
    ReceptionPanel recept;
    @Override
       public void enabled(Boolean flag){
            recept.enabled(flag);
       }
    public OldPatientsSearchPanel(int id,ReceptionPanel rec)
    {
         initComponents();
         reg_no = String.valueOf(id);
         recept = rec;
         
         request_for_search(reg_no,"i");
        
         
         
         
    }
    processingdialog _pd;
    public void request_for_search(String info,String type)
    {
        HashMap<String,String> data=new HashMap();
        data.put("available_info",info);
        data.put("search_type",type);
        query q = new query(codes.datainput);
        q.section = this.__name;
        q.code = reception.OldPatientExistanceCheck;
        q.payload =data;
        
        _pd = new processingdialog(this, q);
        _pd.exec();
        
    }
    
    
    //if search is made on the basis of name
     public OldPatientsSearchPanel(String str,ReceptionPanel rec)
    {
         initComponents();
         name = str;
         recept = rec;
         request_for_search(name,"n");    
    }
     
    
    private void alignCenter(JTable table, int column) {
    DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
    centerRenderer.setHorizontalAlignment(JLabel.CENTER);
    table.getColumnModel().getColumn(column).setCellRenderer(centerRenderer);
}
    
     
    //for displaying table if possible matches are found
    public final void display_result_table(List<HashMap<String,String>> patient_matches,ReceptionPanel rec)
    {
        for(int i=0;i<search_patient_table.getColumnCount();i++){
            alignCenter(search_patient_table, i);
        }
        DefaultTableModel table = (DefaultTableModel) search_patient_table.getModel();
        for(int i=0;i<patient_matches.size();i++){
        table.addRow(new Object[]{patient_matches.get(i).get(fields.patientid),patient_matches.get(i).get(fields.name),patient_matches.get(i).get(fields.sex),patient_matches.get(i).get(fields.address),patient_matches.get(i).get(fields.contact)});
        }
        patient_list=patient_matches;
        handle_table_events(rec);
    }
    
    
    //to handle the table events
    public final void handle_table_events(final ReceptionPanel rp)
    {
        search_patient_table.setRowHeight(30);
        search_patient_table.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        
        //mouse clicked event
        search_patient_table.addMouseListener(new java.awt.event.MouseAdapter() {
             @Override
           public void mouseClicked(java.awt.event.MouseEvent evt) {
           int row = search_patient_table.getSelectedRow();
           int col = 0;
           
           if(evt.getClickCount()>1)
           {
               System.out.println("clicked");
               //reg =  search_patient_table.getValueAt(row, col).toString();
               reg_no = search_patient_table.getValueAt(row, col).toString();
               JPanel basePanel = rp.get_receptionWorkingPanel();
               basePanel.removeAll();
               for(int j=0;j<patient_list.size();j++)
               {
                   if(reg_no.equals(patient_list.get(j).get(fields.patientid)))
                   {
                       name=patient_list.get(j).get(fields.name);
                       address=patient_list.get(j).get(fields.address);
                       sex=patient_list.get(j).get(fields.sex);
                       contact=patient_list.get(j).get(fields.contact);
                   }
               }
                   try {
                       basePanel.add(new OPD_RegistrationPanel(recept,reg_no,name,address,sex,contact));
                   } catch (GlobalException ex) {
                       Logger.getLogger(OldPatientsSearchPanel.class.getName()).log(Level.SEVERE, null, ex);
                   }
               rp.revalidate();
               rp.repaint();
           }
         }
        });
       
    }

     public void set_existing_data(HashMap<String,String> p)
    {
        name=p.get(fields.name);
        address=p.get(fields.address);
        sex=p.get(fields.sex);
        contact=p.get(fields.contact);
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        search_patient_table = new javax.swing.JTable();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(8, 87, 105)));

        search_patient_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                " Registration No.", " Name ", " Sex", " Address", " Contact"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(search_patient_table);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 906, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 326, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable search_patient_table;
    // End of variables declaration//GEN-END:variables
}

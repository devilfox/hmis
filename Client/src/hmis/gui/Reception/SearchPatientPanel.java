/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.gui.Reception;

import hmis.Layer.core.GlobalException;
import hmis.Shared.Commobjects.primitive.ReceptionModuleUpdates;
import hmis.Shared.Commobjects.primitive.updaterequest;
import hmis.Shared.Commobjects.standard.query;
import hmis.Shared.Identifiers.codes;
import hmis.Shared.Identifiers.reception;
import hmis.Shared.Identifiers.section;
import hmis.Shared.sharedresources.Do;
import hmis.Shared.sharedresources.rshared;
import hmis.gui.base._rbase;
import hmis.gui.base.processingdialog;
import hmis.gui.main.ValidateInput;
import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EtchedBorder;

/**
 *
 * @author Rupesh
 */
public class SearchPatientPanel extends _rbase {
    public processingdialog _pd;
    public static String __name = section.searchpatientmainpanel;
    public Do getdo(){
        return new Do(__name,this);
    }
    public void refresh(){
        this.revalidate();
        this.refresh();
    }
    
    public void requestupdate() throws hmis.Layer.core.GlobalException{
        updaterequest request = new updaterequest();
        request.section = __name;
        rshared.server.send(request,new Do(__name,this));
    }
    
    
    SearchByNamePanel search_by_name_panel = new SearchByNamePanel();
    SearchByIDPanel search_by_id_panel = new SearchByIDPanel();
    SearchPatientsResultPanel search_patient_result_panel = new SearchPatientsResultPanel();
    List<HashMap<String,String>> patient_list; 
    String name,reg_no;
    int reg;
    
    public void pipeupdate(ReceptionModuleUpdates updates){
        
        System.out.println("search panel updated");
    }
    public void pipequeryreply(query q){
//        HashMap<String,List> info=new HashMap();
//        info=(HashMap<String, List>) q.payload;
//        patient_list=info.get(fields.patient_info);
//        //System.out.println("in pipequery");
//        _pd.kill();
//        if(patient_list.size()<=0){
//           JOptionPane.showMessageDialog(null,"No results found! :(");  
//        }
    }
    
    /**
     * Creates new form SearchPatientPanel
     */
   Boolean select;
   ReceptionPanel Reception;
    public SearchPatientPanel(ReceptionPanel rec) {
        initComponents();
        Reception = rec;
        
        
        //this object is created locally py passing section of OldPatientMainPanel and ReceptionPanel
        SearchByNamePanel search_by_name_panel2 = new SearchByNamePanel(this);
        SearchByIDPanel search_by_id_panel2 = new SearchByIDPanel(this);
        
        //then the created object is assigned to other object which is global
        search_by_name_panel = search_by_name_panel2;
        search_by_id_panel = search_by_id_panel2;
        searchButton.setVisible(false);
    }
    
    public void action_on_serchbyname_button_event()
    {
            search_by_name_panel.set_name("");
            searchPatientResultBasePanel.removeAll();
            enter_info_panel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
            enter_info_panel.setLayout(new BorderLayout());
            enter_info_panel.removeAll();
            enter_info_panel.add(search_by_name_panel);
            this.revalidate();
            this.repaint();
            searchButton.setVisible(true);
    }
    
    public void action_on_searchbyid_button_event()
    {
         search_by_id_panel.set_registration_no("");
            searchPatientResultBasePanel.removeAll();
            enter_info_panel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
            enter_info_panel.setLayout(new BorderLayout());
            enter_info_panel.removeAll();
            enter_info_panel.add(search_by_id_panel);
            this.revalidate();
            this.repaint();
            searchButton.setVisible(true);
    }
    
    public void action_on_search_button_searchbyname_buttton_event()
    {
        name = search_by_name_panel.get_name().toString();
        
                if(!ValidateInput.validateName(name)){
                   JOptionPane.showMessageDialog(null, "ENTER  A  VALID  NAME", "INPUT ERROR", JOptionPane.ERROR_MESSAGE); 
                }
                else{
                search_by_id_radioButton.setSelected(false);
                
                //request for patient data here
                
                    searchPatientResultBasePanel.add(new SearchPatientsTableResultPanel(name,this));
                
                
                }
                

    }
    
    public void action_on_search_button_searchbyid_button_event()
    {
         try{
                    reg = search_by_id_panel.get_registration_no();
                   }
                catch(NumberFormatException e)
                    {
                        JOptionPane.showMessageDialog(null, "ENTER  A  VALID  REGISTRATION  NO.", "INPUT ERROR", JOptionPane.ERROR_MESSAGE);
                    }
                search_by_name_radioButton.setSelected(false);
       //-----------------------------------------------------------         
                
            //if(patient_list.size()>0)
            
                searchPatientResultBasePanel.add(new SearchPatientsTableResultPanel(reg,this));
            
               
            
    }
    
    public void request_patient_info(String available_info,String search_type)
    {
        
            query q = new query(codes.datainput);
            q.section = this.__name;
            q.code=reception.SearchPatientDetails;
            HashMap<String,String> data=new HashMap();
            data.put("available_info",available_info);
            data.put("search_type", search_type);
            q.payload =data ;
     
            try {
                rshared.server.send(q, this.getdo());
            } catch (hmis.Layer.core.GlobalException ex) {
                Logger.getLogger(OPD_RegistrationPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
            
     
            //System.out.println(""+patient_list);
    }
    
    public void action_on_searchpatient()
    {
         searchPatientResultBasePanel.removeAll();
            
            //is search by name is selected
            if(select) {
                action_on_search_button_searchbyname_buttton_event();
                
            }
            
            //else if search by id is selected
            else{
               action_on_search_button_searchbyid_button_event();
            }
                
            this.revalidate();
            this.repaint();
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        search_by_name_radioButton = new javax.swing.JRadioButton();
        search_by_id_radioButton = new javax.swing.JRadioButton();
        enter_info_panel = new javax.swing.JPanel();
        searchButton = new javax.swing.JButton();
        searchPatientResultBasePanel = new javax.swing.JPanel();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(190, 172, 172)));

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SEARCH A PATIENT", 0, 0, new java.awt.Font("Andalus", 0, 12))); // NOI18N
        jPanel1.setPreferredSize(new java.awt.Dimension(928, 155));

        jLabel1.setFont(new java.awt.Font("Traditional Arabic", 0, 18)); // NOI18N
        jLabel1.setText("Search By");

        buttonGroup1.add(search_by_name_radioButton);
        search_by_name_radioButton.setFont(new java.awt.Font("Trebuchet MS", 0, 14)); // NOI18N
        search_by_name_radioButton.setText("Name");
        search_by_name_radioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                search_by_name_radioButtonActionPerformed(evt);
            }
        });
        search_by_name_radioButton.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                search_by_name_radioButtonKeyPressed(evt);
            }
        });

        buttonGroup1.add(search_by_id_radioButton);
        search_by_id_radioButton.setFont(new java.awt.Font("Trebuchet MS", 0, 14)); // NOI18N
        search_by_id_radioButton.setText("Reg No.");
        search_by_id_radioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                search_by_id_radioButtonActionPerformed(evt);
            }
        });
        search_by_id_radioButton.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                search_by_id_radioButtonKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout enter_info_panelLayout = new javax.swing.GroupLayout(enter_info_panel);
        enter_info_panel.setLayout(enter_info_panelLayout);
        enter_info_panelLayout.setHorizontalGroup(
            enter_info_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 500, Short.MAX_VALUE)
        );
        enter_info_panelLayout.setVerticalGroup(
            enter_info_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 57, Short.MAX_VALUE)
        );

        searchButton.setText("Search");
        searchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchButtonActionPerformed(evt);
            }
        });
        searchButton.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                searchButtonKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(enter_info_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(39, 39, 39)
                        .addComponent(searchButton))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(search_by_name_radioButton)
                        .addGap(18, 18, 18)
                        .addComponent(search_by_id_radioButton)))
                .addContainerGap(302, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(search_by_name_radioButton)
                    .addComponent(search_by_id_radioButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(enter_info_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(searchButton)
                        .addGap(24, 24, 24))))
        );

        searchPatientResultBasePanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout searchPatientResultBasePanelLayout = new javax.swing.GroupLayout(searchPatientResultBasePanel);
        searchPatientResultBasePanel.setLayout(searchPatientResultBasePanelLayout);
        searchPatientResultBasePanelLayout.setHorizontalGroup(
            searchPatientResultBasePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 924, Short.MAX_VALUE)
        );
        searchPatientResultBasePanelLayout.setVerticalGroup(
            searchPatientResultBasePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 346, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(searchPatientResultBasePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(searchPatientResultBasePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void search_by_name_radioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_search_by_name_radioButtonActionPerformed
        // TODO add your handling code here:
        searchPatientResultBasePanel.setLayout(new BorderLayout());
        if(search_by_name_radioButton.isSelected())
        {
            action_on_serchbyname_button_event();
        }
        
    }//GEN-LAST:event_search_by_name_radioButtonActionPerformed

    private void search_by_id_radioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_search_by_id_radioButtonActionPerformed
        // TODO add your handling code here:
        searchPatientResultBasePanel.setLayout(new BorderLayout());
        if(search_by_id_radioButton.isSelected())
        {
            action_on_searchbyid_button_event();
        }
    }//GEN-LAST:event_search_by_id_radioButtonActionPerformed

    private void searchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchButtonActionPerformed
        // TODO add your handling code here:
        
         
        //Initialise Search Panel
            searchPatientResultBasePanel.removeAll();
            
            //is search by name is selected
            if(search_by_name_radioButton.isSelected()) {
                action_on_search_button_searchbyname_buttton_event();
                
            }
            
            //else if search by id is selected
            else{
               action_on_search_button_searchbyid_button_event();
            }
                       
            this.revalidate();
            this.repaint();
           
    }//GEN-LAST:event_searchButtonActionPerformed
    @Override
    public void bgtask(){
        for(int i=0;i<5;i++){
            System.out.println("we are waiting for 5 seconds in OPD panel for core to reply our request");
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                break;
            }
        }
    }
    @Override
    public void oncomplete(){
        System.out.println("complete");
    }
    private void search_by_name_radioButtonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_search_by_name_radioButtonKeyPressed
         
        if(evt.getKeyCode()== KeyEvent.VK_ENTER)
        {
         select=true;
       
            searchPatientResultBasePanel.setLayout(new BorderLayout());
            action_on_serchbyname_button_event();
        
            //do something
        }// TODO add your handling code here:
    }//GEN-LAST:event_search_by_name_radioButtonKeyPressed

    private void search_by_id_radioButtonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_search_by_id_radioButtonKeyPressed
          
        if(evt.getKeyCode()== KeyEvent.VK_ENTER)
        {
            select =false;
        searchPatientResultBasePanel.setLayout(new BorderLayout());
        
            action_on_searchbyid_button_event();
        
        } 
        // TODO add your handling code here:
    }//GEN-LAST:event_search_by_id_radioButtonKeyPressed

    private void searchButtonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchButtonKeyPressed
        // TODO add your handling code here:
       if(evt.getKeyCode()==KeyEvent.VK_ENTER)
       {
           //Initialise Search Panel
            searchPatientResultBasePanel.removeAll();
            
            //is search by name is selected
            if(select) {
                action_on_search_button_searchbyname_buttton_event();
                
            }
            
            //else if search by id is selected
            else{
               action_on_search_button_searchbyid_button_event();
            }
            
                
            this.revalidate();
            this.repaint();
       }
    }//GEN-LAST:event_searchButtonKeyPressed

    public JPanel get_search_patient_base_panel()
    {
        return searchPatientResultBasePanel;
    }
    
    public JRadioButton get_search_by_name_radiobutton()
    {
        return search_by_name_radioButton;
    }
    
    public JRadioButton get_search_by_id_radiobutton()
    {
        return search_by_id_radioButton;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JPanel enter_info_panel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton searchButton;
    private javax.swing.JPanel searchPatientResultBasePanel;
    private javax.swing.JRadioButton search_by_id_radioButton;
    private javax.swing.JRadioButton search_by_name_radioButton;
    // End of variables declaration//GEN-END:variables
}

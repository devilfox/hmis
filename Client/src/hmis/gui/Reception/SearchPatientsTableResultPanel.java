/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.gui.Reception;

import hmis.Shared.Commobjects.primitive.ReceptionModuleUpdates;
import hmis.Shared.Commobjects.primitive.updaterequest;
import hmis.Shared.Commobjects.standard.query;
import hmis.Shared.Identifiers.codes;
import hmis.Shared.Identifiers.fields;
import hmis.Shared.Identifiers.reception;
import hmis.Shared.Identifiers.section;
import hmis.Shared.sharedresources.Do;
import hmis.Shared.sharedresources.rshared;
import hmis.gui.base._rbase;
import hmis.gui.base.processingdialog;
import java.util.HashMap;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Rupesh
 */
public class SearchPatientsTableResultPanel extends _rbase{
  processingdialog _pd;
    /**
     * Creates new form NewJPanel
     */
    public static String __name = section.searchpanel;
    public Do getdo(){
        return new Do(__name,this);
    }
    public void refresh(){
        this.revalidate();
        this.refresh();
    }
    public void requestupdate() throws hmis.Layer.core.GlobalException{
        updaterequest request = new updaterequest();
        request.section = __name;
        rshared.server.send(request,new Do(__name,this));
        
        
    }
    public void pipeupdate(ReceptionModuleUpdates updates){
        
        
    }
    public void pipequeryreply(query q)
    {
        System.out.println("asdfasdfasdf");
        
        HashMap<String,List<HashMap<String,String>>> info=(HashMap<String,List<HashMap<String,String>>>) q.payload; 
        patient_list=info.get(fields.patient_info);
      System.out.println(info.get(fields.patient_info));
      _pd.kill();
        if(patient_list.size()>0){
            display_result_table(patient_list);
        }else{
            
            JOptionPane.showMessageDialog(null, "No results found, Please check input :(");
        }
        // handle_table_events(spp);
        
    }

    String reg_no,name,age,address,contact,sex;
    List<HashMap<String,String>> patient_list;
    public SearchPatientsTableResultPanel() {
        initComponents();
    }
  /*  
    public SearchPatientsTableResultPanel(List<HashMap<String,String>> pat_info,SearchPatientPanel spp)
    {
         initComponents();
         //reg_no = String.valueOf();
         
         //query into database based on id
         
         display_result_table(pat_info);
         handle_table_events(spp);
    }
    */
    //List<HashMap<String,String>> pat_info;
     public SearchPatientsTableResultPanel(String info,SearchPatientPanel spp)
    {
         initComponents();
         
         //query into database based on name
        // patient_list=pat_info;
         request_for_search(info,"n");
         //display_result_table(patient_list);
         handle_table_events(spp);
    }

          public SearchPatientsTableResultPanel(int info,SearchPatientPanel spp)
    {
         initComponents();
         
         //query into database based on name
        // patient_list=pat_info;
         request_for_search(String.valueOf(info),"i");
         //display_result_table(patient_list);
         
         handle_table_events(spp);
    }

        public void request_for_search(String info,String type)
    {
        HashMap<String,String> data=new HashMap();
        data.put("available_info",info);
        data.put("search_type",type);
        query q = new query(codes.datainput);
        q.section = this.__name;
        q.code = reception.OldPatientExistanceCheck;
        q.payload =data;
        
        _pd = new processingdialog(this,q);
        
            _pd.exec();
            
    }
        
    private void alignCenter(JTable table, int column) {
    DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
    centerRenderer.setHorizontalAlignment(JLabel.CENTER);
    table.getColumnModel().getColumn(column).setCellRenderer(centerRenderer);
}
        
    public final void display_result_table(List<HashMap<String,String>> patient_list)
    {
        for(int i=0;i<search_patient_table.getColumnCount();i++){
        alignCenter(search_patient_table,i);
        }
        
        DefaultTableModel table = (DefaultTableModel) search_patient_table.getModel();
        for(int i=0;i<patient_list.size();i++){
        name = patient_list.get(i).get(fields.name);
         age=patient_list.get(i).get(fields.age);
         sex=patient_list.get(i).get(fields.sex);
         address=patient_list.get(i).get(fields.address);
         contact=patient_list.get(i).get(fields.contact);
         reg_no=patient_list.get(i).get(fields.patientid);
        table.addRow(new Object[]{reg_no,name,age,sex,address,contact});}
    }
    
    public final void handle_table_events(final SearchPatientPanel sp)
    {
        search_patient_table.setRowHeight(30);
        search_patient_table.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        
        //mouse clicked event
        search_patient_table.addMouseListener(new java.awt.event.MouseAdapter() {
             @Override
           public void mouseClicked(java.awt.event.MouseEvent evt) {
           int row = search_patient_table.getSelectedRow();
           int col = 0;
           
           if(evt.getClickCount()>1)
           {
               reg_no = (String)search_patient_table.getValueAt(row, col);
               JPanel basePanel = sp.get_search_patient_base_panel();
               basePanel.removeAll();
               HashMap<String,String> selected_patient_info=new HashMap();
               for(int i=0;i<patient_list.size();i++){
                   if(reg_no==patient_list.get(i).get(fields.patientid)){
                       selected_patient_info=patient_list.get(i);
                   }
               }
               basePanel.add(new SearchPatientsResultPanel(selected_patient_info));
               sp.revalidate();
               sp.repaint();
           }
         }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        search_patient_table = new javax.swing.JTable();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(8, 87, 105)));

        search_patient_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "   Registration No.", " Name ", " Age", " Sex", " Address", " Contact"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(search_patient_table);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 906, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 326, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable search_patient_table;
    // End of variables declaration//GEN-END:variables
}

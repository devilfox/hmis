/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.gui.Reception;

import hmis.Layer.core.GlobalException;
import hmis.Shared.Commobjects.primitive.ReceptionModuleUpdates;
import hmis.Shared.Commobjects.primitive.updaterequest;
import hmis.Shared.Commobjects.standard.query;
import hmis.Shared.Identifiers.codes;
import hmis.Shared.Identifiers.fields;
import hmis.Shared.Identifiers.section;
import hmis.Shared.sharedresources.Do;
import hmis.Shared.sharedresources.rshared;
import hmis.gui.base._rbase;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Bhuwan
 */
public final class viewappoinment extends _rbase {

    /**
     * Creates new form viewappoinment
     */
  
    
//    public viewappoinment(LayoutManager lm) {
//        super(lm);
//        
//    }
//    
List<HashMap<String,String>> appointment_list;
    
    
    public static String __name = section.viewappointmentpanel;
    public Do getdo(){
        return new Do(__name,this);
    }
    public void refresh(){
        this.revalidate();
        this.repaint();
    }
    
    public void requestupdate() throws hmis.Layer.core.GlobalException{
        updaterequest request = new updaterequest();
        request.section = __name;
        rshared.server.send(request,getdo());
    }
    
    public void pipeupdate(ReceptionModuleUpdates updates){
        //no updates necessary for viewappointment panel
        //the necessary appointment list from database has been updated in Appointment panel
    }
    
    @Override
    public void pipequeryreply(query q){
        //no reply for query is required since the query is to remove the patient from the appoinment table
        //  and store it in OPD table
    }

    ReceptionPanel rp;
    public viewappoinment(ReceptionPanel _rp,List<HashMap<String,String>> appt_list) {
        rp=_rp;
        initComponents();
        appointment_table.setRowHeight(30);
//        List<HashMap<String,String>> appt_list;
        appointment_list=appt_list;
        display_appointment_table(appt_list);
        initializeTableEvents();
    }
    
      private void alignCenter(JTable table, int column) {
    DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
    centerRenderer.setHorizontalAlignment(JLabel.CENTER);
    table.getColumnModel().getColumn(column).setCellRenderer(centerRenderer);
}
    private void display_appointment_table(List<HashMap<String,String>> appt_list)
    {
        for(int i=0;i<appointment_table.getColumnCount();i++){
            alignCenter(appointment_table, i);
        }
        DefaultTableModel table = (DefaultTableModel) appointment_table.getModel();
//        System.out.println(""+appt_list.size());
        //System.out.println(""+appt_list);
        for(int i=0;i<appt_list.size();i++)
        {
        table.addRow(new Object[]{appt_list.get(i).get(fields.name),
                                  appt_list.get(i).get(fields.appointment_date),
                                  appt_list.get(i).get(fields.doctor),
                                  appt_list.get(i).get(fields.department),
                                  appt_list.get(i).get(fields.contact)
                                });
        }
    }
    
    private void initializeTableEvents()
    {
    
       
        appointment_table.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        
        //mouse clicked event
        appointment_table.addMouseListener(new java.awt.event.MouseAdapter() {
             @Override
           public void mouseClicked(java.awt.event.MouseEvent evt) {
           int row = appointment_table.getSelectedRow();
           int col = 0;
 
           if(evt.getClickCount()>1)
           {      
//               doctor_name = (String) specific_doctor_schedule_table.getValueAt(row, col);
//               doctor_speciality = (String) specific_doctor_schedule_table.getValueAt(row, col+1);
//               sunday_routine= (String) specific_doctor_schedule_table.getValueAt(row, col+2);
//               
            HashMap<String,String> info=new HashMap();
            info.put(fields.appointment_id,appointment_list.get(row).get(fields.appointment_id));
            info.put(fields.name,(String)appointment_table.getValueAt(row, col));
//            info.put(fields.contact,(String)appointment_table.getValueAt(row,col+4));
              
            if(JOptionPane.showConfirmDialog(null,"Do you want to register "+info.get(fields.name)+" for OPD?","Confirm OPD Register",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION){
                request_for_shift_to_opd(info);
                
               JPanel basePanel = rp.get_receptionWorkingPanel();
               basePanel.removeAll();
                   try {
                       basePanel.add(new OPD_RegistrationPanel(appointment_list.get(row).get(fields.name),appointment_list.get(row).get(fields.contact),appointment_list.get(row).get(fields.doctorid),appointment_list.get(row).get(fields.department),rp));
                   } catch (GlobalException ex) {
                       Logger.getLogger(OldPatientsSearchPanel.class.getName()).log(Level.SEVERE, null, ex);
                   }
               rp.revalidate();
               rp.repaint();
            }
            
           }
         }
        });
    }
    public void request_for_shift_to_opd(HashMap<String,String> info){
        
        query q=new query(codes.datainput);
        q.section=this.__name;
        q.payload=info;
        
        try {
                rshared.server.send(q, this.getdo());
            } catch (hmis.Layer.core.GlobalException ex) {
                Logger.getLogger(OPD_RegistrationPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        appointment_table = new javax.swing.JTable();

        jScrollPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        appointment_table.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        appointment_table.setFont(new java.awt.Font("Traditional Arabic", 0, 18)); // NOI18N
        appointment_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Appointment Date", "Doctor", "Department", "Contact"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(appointment_table);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 984, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 336, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable appointment_table;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}

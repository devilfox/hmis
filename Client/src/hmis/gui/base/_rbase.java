/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.gui.base;

import hmis.Shared.Commobjects.primitive.ReceptionModuleUpdates;
import hmis.Shared.Commobjects.standard.query;
import hmis.Shared.sharedresources.Do;

/**
 *
 * @author daredevils
 */
public class _rbase extends javax.swing.JPanel{
    //overrideable class in all child of this class
    public void refresh(){}
    public Do getdo(){return new Do("",null);}
    public void requestupdate()throws hmis.Layer.core.GlobalException{}
    public void pipeupdate(ReceptionModuleUpdates updates){}
    public void pipequeryreply(query q){}
    
    //for processing
    public void bgtask(){}
    public void oncomplete(){}
    public void enabled(Boolean flag){}
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.gui.base;

import hmis.Shared.Commobjects.standard.query;
import hmis.Shared.sharedresources.rshared;
import java.awt.Dialog.ModalityType;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

/**
 *
 * @author daredevils
 */
public class processingdialog{
    
    
    class wrk extends SwingWorker{
        Boolean override = false;
        JDialog dialog;
        _rbase usr;
        query tosend;
        public wrk(_rbase pusr,JDialog d,query q){
            tosend = q;
            usr = pusr;
            dialog = d;
        }
        public void foverride(){
            override = true;
        }
        @Override
        protected Object doInBackground() throws Exception {
          //  usr.enabled(Boolean.FALSE);
            if(tosend!=null){
                rshared.server.send(tosend, usr.getdo());
            }else{
                usr.requestupdate();
            }
            if(override){
                usr.bgtask();
            }else{
                try {
                    for(int i=0;i<5;i++){
                        Thread.sleep(1000);
                    }
                } catch (InterruptedException ex) {
                    //Logger.getLogger(processingdialog.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return null;
        }
        @Override
        protected void done(){  
            if(!this.isCancelled()){
                //means the reply didnt cancel the dialog, then we shouldnt expect any reply further for that request maker
                JOptionPane.showMessageDialog(null, "Core failed to reply!!");
               //rshared.recentcaller = null;
        
            }
            if(override){
                usr.oncomplete();     
            }
         //   usr.enabled(Boolean.TRUE);
            dialog.dispose();
        }  
        
    }
    
    Boolean kill = false;
    final JDialog dialog;
    _rbase user;
    query tosend;
    wrk worker;
    public processingdialog(_rbase panel,query q){
        dialog = new JDialog(null,ModalityType.APPLICATION_MODAL);// modal  
        user = panel;
        dialog.setUndecorated(true);  
        dialog.setLocationRelativeTo(panel);
        JProgressBar bar = new JProgressBar();  
        bar.setIndeterminate(true);  
        bar.setStringPainted(true);  
        bar.setString("Processing...");  
        dialog.add(bar);
        dialog.pack();
        worker = new wrk(user,dialog,q);
    }
    
    public void override(){
        worker.foverride();
    }
    public void init(){
        kill = false;
    }
    public void exec(){
        if(!kill){
            worker.execute();
            dialog.setVisible(true);
        }
    }
    public void kill(){
        kill = true;
        worker.cancel(true);
        dialog.dispose();
        
    }
    public Boolean iskilled(){ //if killed, returns true else will notify the timeout
        return worker.isCancelled();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.gui.main;

/**
 *
 * @author Rupesh
 */
public class ValidateInput {
    
    public static boolean validateName(String name)
    {
        return name.matches("[a-zA-z]+(\\s[a-zA-z]+(['-][a-zA-z]+)*)*\\s*");
    }
    
    public static boolean validateAge(String age)
    {
        try{
            int Age = Integer.parseInt(age);
            
            if(Age<0 || Age>150){
                return false;
            }
        }
        catch(NumberFormatException e){
            return false;
        }
        return age.matches("\\s*\\d+\\s*");
    }
    
    public static boolean validateAddress(String address)
    {
        if(address.equals("")) {
            return false;
        }
        return true;
    }
    
    public static boolean validateContact(String contact)
    {
        return contact.matches("\\s*\\d{6,10}\\s*");
    }
    
    public static boolean validateRegistration(String reg)
    {
        return reg.matches("\\s*\\d+\\s*");
    }
}

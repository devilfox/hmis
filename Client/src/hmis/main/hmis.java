/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.main;

import hmis.Layer.DB.servervars;
import hmis.Layer.core.ClientThread;
import hmis.Layer.core.GlobalException;
import hmis.Layer.core.Server;
import hmis.gui.main.LoginFrame;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;



/**
 *
 * @author Rupesh
 */
public class hmis {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
        try {
            try (Scanner input = new Scanner(new File("server.conf"))) {
                
                servervars.ip = input.next();
                servervars.port = input.nextInt();
                System.out.println(servervars.ip+servervars.port);
            }
            //First connection to the server is to be made
            //This will automatically throw exception if connection is unsucessful
            Server server = new Server();
            
            //connection sucessful, then load the listner thread
            ClientThread listner = new ClientThread("Listner");
            listner.execute();
            
            //server connection is sucessful & listner thread initiated, then load the login frame
            LoginFrame login = new LoginFrame(350,280);
            login.setResizable(false);
            login.setVisible(true);
            
        } catch (GlobalException ex) {
            JOptionPane.showMessageDialog(null,"Oops!! server seems to be down! :(  ");
        } catch(FileNotFoundException ex){
            JOptionPane.showMessageDialog(null,"OOps!! server configuration file not found!!");
        }
        
       
                
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hmis.Admin;

import hmis.Event.logger.log;
import hmis.Layer.Core.Client;
import hmis.Layer.Core.Core;
import hmis.Layer.Core.CoreThread;
import hmis.Layer.Core.CurrentClients;
import hmis.Shared.Exception.GlobalException;
import hmis.Shared.Identifiers.DBCodes;
import hmis.Shared.Identifiers.fields;
import hmis.Shared.Identifiers.staffid;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author root
 */
public class admin {
    
    public static String dbversion;
    public static String address; //address of the server
    public static String user; //user name for the admin
    public static String pass; //pass for admin
    private static Connection db; //resulting database connection for admin
    
    public static Boolean isrunnable = true;
    public static Boolean isrunning = false;
    public static Boolean isStarted = false;
    public static Core maincore;
    public static CoreThread main;
    public static Integer threadscount = 1; //counts the number of parallel threads core is running
    public static Integer port; //Current running port of the hmis server
    public static String hmisUser;
    public static String hmisPass;
    public static String exportdir;
    public static Boolean exportlog;
    public static Integer logcount;
    
    public static Date user_last_modified_doc;
    public static Date user_last_modified_others;
    
    public static Date getlatestmodifieddate(){
        if(user_last_modified_doc.after(user_last_modified_others)){
            return user_last_modified_doc;
        }
        return user_last_modified_others;
    }
    public static void login(String addr, String User,String Pass)throws GlobalException, SQLException{
        String Addr = "jdbc:mysql://"+addr+"?allowMultiQueries=true";
        try {
            db = DriverManager.getConnection(Addr,User,Pass);
            PreparedStatement pst = db.prepareStatement("select version();");
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
                dbversion = rs.getString(1);
            }
        } catch (SQLException ex) {
            throw new GlobalException("","unable to connect to the database",null);
        }
        try {
            InetAddress adr = InetAddress.getLocalHost();
            address = adr.getHostAddress();
        } catch (UnknownHostException ex) {
            Logger.getLogger(admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        user = User; pass = Pass;
        loadserverconfig();
    }
    
    public static void editserverconfig() throws SQLException{
        PreparedStatement pst = db.prepareStatement("update db_admin.serverconfig set port=?,hmisUser=?,hmisPass=?,exportlog=?,exportdir=?,logcount=?;");
        pst.setInt(1, port);
        pst.setString(2, hmisUser);
        pst.setString(3, hmisPass);
        pst.setBoolean(4, exportlog);
        pst.setString(5, exportdir);
        pst.setInt(6, logcount);
        pst.executeUpdate();
        loadserverconfig();
    }   
    //load the server details for running the server further eg, port
    public static void loadserverconfig() throws SQLException{
        //port,hmisuser,hmispass will be loaded here (and other configurations if any)
        //now identifiers.dbcodes will be modified accordingly here
        PreparedStatement pst = db.prepareStatement("SELECT * FROM db_admin.serverconfig;");
        ResultSet result  = pst.executeQuery();
        
        while(result.next()){
            port = result.getInt("port");
            hmisUser = result.getString("hmisUser");
            hmisPass = result.getString("hmisPass");
            user_last_modified_doc = result.getDate("user_last_modified_doc");
            user_last_modified_others = result.getDate("user_last_modified_others");
            exportdir = result.getString("exportdir");
            exportlog = result.getBoolean("exportlog");
            logcount = result.getInt("logcount");
        }
        
        //port = 
        //hmisUser = 
        //hmisPass =
        
        DBCodes.user = hmisUser;
        DBCodes.pass = hmisPass;
        
        //After this phase, the server will be runnable
        isrunnable = true;
    }
    
    public static HashMap<String,String> createEntry(HashMap<String,Object> values) throws SQLException, ParseException{
        String department = (String)values.get(fields.department);
        switch (department){
            case "other":{
                updateOtherModifiedTime();
                loadserverconfig();
                if(((String)values.get(fields.otherdept)).equalsIgnoreCase(staffid.receptionist)){
                        //if the entry to be created is for reception
                    //use values.get(fields.*) *=member you want to access and push
                    //1. place an entry in reception_details table
                    PreparedStatement pst = db.prepareStatement("insert into db_staff.table_reception (name,address,contact,enrolled,gender,department) values(?,?,?,?,?,?)");
                    pst.setString(1, (String)values.get(fields.name));
                    pst.setString(2,(String)values.get(fields.address));
                    pst.setString(3, (String)values.get(fields.contact));
                    pst.setString(4, (String)values.get(fields.enrolledfrom));
                    pst.setString(5, (String)values.get(fields.sex));
                    pst.setString(6, staffid.receptionist);
                    pst.execute();
                    //now get the generated id from the above statement
                    Integer generatedID = null;
                    pst = db.prepareStatement("select max(id) from db_staff.table_reception;");
                    ResultSet r = pst.executeQuery();
                    while(r.next()){
                        generatedID = r.getInt(1);
                    }
                    //now pus the credentials into the authentication database
                    pst = db.prepareStatement("insert into db_authentication.table_auth (username,pass,auth_hive,dept_id,client_id) values(?,?,?,?,?)");
                    pst.setString(1,(String)values.get(fields.username));
                    pst.setString(2, (String)values.get(fields.password));
                    pst.setString(3, "#auth_hive_3");
                    pst.setString(4,"#DEPT_001");
                    pst.setInt(5, generatedID);
                    pst.execute();
                    HashMap<String,String> ret = new HashMap();
                    ret.put(fields.name, (String)values.get(fields.name));
                    ret.put(fields.id, String.valueOf(generatedID));
                    return ret;
                }else{
                    PreparedStatement pst = db.prepareStatement("insert into db_staff.table_reception (name,address,contact,enrolled,gender,department) values(?,?,?,?,?,?)");
                    pst.setString(1, (String)values.get(fields.name));
                    pst.setString(2,(String)values.get(fields.address));
                    pst.setString(3, (String)values.get(fields.contact));
                    pst.setString(4, (String)values.get(fields.enrolledfrom));
                    pst.setString(5, (String)values.get(fields.sex));
                    pst.setString(6, "lab");
                    pst.execute();
                    //now get the generated id from the above statement
                    Integer generatedID = null;
                    pst = db.prepareStatement("select max(id) from db_staff.table_reception;");
                    ResultSet r = pst.executeQuery();
                    while(r.next()){
                        generatedID = r.getInt(1);
                    }
                    //now pus the credentials into the authentication database
                    pst = db.prepareStatement("insert into db_authentication.table_auth (username,pass,auth_hive,dept_id,client_id) values(?,?,?,?,?)");
                    pst.setString(1,(String)values.get(fields.username));
                    pst.setString(2, (String)values.get(fields.password));
                    pst.setString(3, "#auth_hive_3");
                    pst.setString(4,"#DEPT_003");
                    pst.setInt(5, generatedID);
                    pst.execute();
                    HashMap<String,String> ret = new HashMap();
                    ret.put(fields.name, (String)values.get(fields.name));
                    ret.put(fields.id, String.valueOf(generatedID));
                    return ret;
                }
                
            }
            case staffid.doctor:{
                updateDocModifiedTime();
                loadserverconfig();
                //if new entry is created for doctor
                //use values.get(fields.*) *=member you want to access and push
                //check if the department of the doctor is available or not, create if not
                PreparedStatement pst = db.prepareStatement("select ID from db_staff.table_department where Name=?");
                pst.setString(1, ((String)values.get(fields.docdept)).toLowerCase());
                ResultSet res = pst.executeQuery();
                if(!res.next()){
                    //means the result is empty, then push new department into db
                    pst = db.prepareStatement("insert into db_staff.table_department set Name=?");
                    pst.setString(1, ((String)values.get(fields.docdept)).toLowerCase());
                    pst.executeUpdate();
                }
                //now get the department id
                Integer departmentid = null;
                pst = db.prepareStatement("select ID from db_staff.table_department where Name=?");
                pst.setString(1, ((String)values.get(fields.docdept)).toLowerCase());
                ResultSet result = pst.executeQuery();
                while(result.next()){
                    departmentid = result.getInt("ID");
                }
                //now push the timing info
                pst = db.prepareStatement("insert into db_staff.table_staff_timing (Sun_Check_In,Sun_Check_Out,Mon_Check_In,Mon_Check_Out,Tue_Check_In,Tue_Check_Out,Wed_Check_In,Wed_Check_Out,Thu_Check_In,Thu_Check_Out,Fri_Check_In,Fri_Check_Out,Sat_Check_In,Sat_Check_Out) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                setTime(pst,1,(String)values.get("Sun_Check_In"));
                setTime(pst,2,(String)values.get("Sun_Check_Out"));
                setTime(pst,3,(String)values.get("Mon_Check_In"));
                setTime(pst,4,(String)values.get("Mon_Check_Out"));
                setTime(pst,5,(String)values.get("Tue_Check_In"));
                setTime(pst,6,(String)values.get("Tue_Check_Out"));
                setTime(pst,7,(String)values.get("Wed_Check_In"));
                setTime(pst,8,(String)values.get("Wed_Check_Out"));
                setTime(pst,9,(String)values.get("Thu_Check_In"));
                setTime(pst,10,(String)values.get("Thu_Check_Out"));
                setTime(pst,11,(String)values.get("Fri_Check_In"));
                setTime(pst,12,(String)values.get("Fri_Check_Out"));
                setTime(pst,13,(String)values.get("Sat_Check_In"));
                setTime(pst,14,(String)values.get("Sat_Check_Out"));
                pst.executeUpdate();
               // now get the timing id
                Integer timingid = null;
                pst = db.prepareStatement("select max(Timing_ID) from db_staff.table_staff_timing");
                ResultSet rs = pst.executeQuery();
                while(rs.next()){
                    timingid = rs.getInt(1);
                }
                //now push details to the table_staff
                pst = db.prepareStatement("insert into db_staff.table_staff (Name,Gender,Address,Nationality,Qualification,Department_ID,Post,Specialist,Timing_ID,Enrolled_From,Contact) values(?,?,?,?,?,?,?,?,?,?,?)");
                pst.setString(1, (String)values.get(fields.name));  pst.setString(2,(String)values.get(fields.sex));    pst.setString(3, (String)values.get(fields.address));
                pst.setString(4, (String)values.get(fields.nationality));   pst.setString(5, (String)values.get(fields.qualification)); pst.setInt(6,departmentid );
                pst.setString(7,"doctor");  pst.setString(8, (String)values.get(fields.specialist));    pst.setInt(9,timingid); 
                pst.setString(10, (String)values.get(fields.enrolledfrom)); pst.setString(11, (String)values.get(fields.contact));
                pst.executeUpdate();
                //now get the doctors id;
                pst = db.prepareStatement("Select staff_ID from db_staff.table_staff where Name=? and Timing_ID=?;");
                pst.setString(1, (String)values.get(fields.name));
                pst.setInt(2, timingid);
                ResultSet rslt = pst.executeQuery();
                Integer docid = null;
                while(rslt.next()){
                    docid = rslt.getInt(1);
                }
                HashMap<String,String> r= new HashMap();
                r.put(fields.name,(String)values.get(fields.name));
                r.put(fields.doctorid,String.valueOf(docid));
                r.put(fields.docdept, (String)values.get(fields.docdept));
                r.put(fields.deptid, String.valueOf(departmentid));
                return r;
                
               // break;
            }
        }
        return null;
    }
    
    private static void setTime(PreparedStatement pst,int i,String val) throws SQLException, ParseException{
        if(val==null){ pst.setNull(i,java.sql.Types.TIME); return;}
        java.sql.Time t = toTime(val);
        pst.setTime(i,t);
    }
    private static java.sql.Time toTime(String t) throws ParseException{
        if(t==null){return null;}
        DateFormat formatter = new SimpleDateFormat("HHmm");
        return  new java.sql.Time(formatter.parse(t).getTime());
    }
    public static Boolean editEntry(HashMap<String,Object> values) throws SQLException, ParseException{
        String department = (String)values.get(fields.department);
        switch (department){
            case "other":{
                updateOtherModifiedTime();
                loadserverconfig();
                 //if  reception
                //use values.get(fields.*) *=member you want to access and push
                //first, delete the authentication table
                String departments = null;
                String auth_hive = "#auth_hive_3";
                String dept_id = null;
                if(((String)values.get(fields.otherdept)).equalsIgnoreCase("reception")){
                    departments = "reception";
                    dept_id = "#DEPT_001";
                }else{
                    departments = "lab";
                    dept_id="#DEPT_003";
                }
                    PreparedStatement pst = db.prepareStatement("update db_authentication.table_auth set username=?,pass=?,auth_hive=?,dept_id=? where client_id=?; update db_staff.table_reception set name=?,address=?,contact=?,enrolled=?,gender=?,department=? where id=?");
                    pst.setString(1, (String)values.get(fields.username));
                    pst.setString(2, (String)values.get(fields.password));
                    pst.setString(3, auth_hive);
                    pst.setString(4, dept_id);
                    pst.setInt(5, Integer.valueOf((String)values.get(fields.id)));
                    
                    pst.setString(6, (String)values.get(fields.name)); pst.setString(7, (String)values.get(fields.address)); pst.setString(8, (String)values.get(fields.contact)); pst.setString(9,(String)values.get(fields.enrolledfrom)); pst.setString(10, (String)values.get(fields.sex)); pst.setString(11, departments); pst.setInt(12, Integer.valueOf((String)values.get(fields.id)));
                    pst.executeUpdate();
                 
                break;
            }
            case staffid.doctor:{
                updateDocModifiedTime();
                loadserverconfig();
                //if  doctor
                //use values.get(fields.*) *=member you want to access and push
                //now first get the timing id;
                Integer docid = (Integer)values.get(fields.doctorid);
                Integer timingid = (Integer)values.get("timingid");
                
                //edit timings
                PreparedStatement pst = db.prepareStatement("delete from db_staff.table_staff_timing where Timing_ID=?");
                pst.setInt(1, timingid);
                pst.executeUpdate();
                pst = db.prepareStatement("insert into db_staff.table_staff_timing (Sun_Check_In,Sun_Check_Out,Mon_Check_In,Mon_Check_Out,Tue_Check_In,Tue_Check_Out,Wed_Check_In,Wed_Check_Out,Thu_Check_In,Thu_Check_Out,Fri_Check_In,Fri_Check_Out,Sat_Check_In,Sat_Check_Out,Timing_ID) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                setTime(pst,1,(String)values.get("Sun_Check_In"));
                setTime(pst,2,(String)values.get("Sun_Check_Out"));
                setTime(pst,3,(String)values.get("Mon_Check_In"));
                setTime(pst,4,(String)values.get("Mon_Check_Out"));
                setTime(pst,5,(String)values.get("Tue_Check_In"));
                setTime(pst,6,(String)values.get("Tue_Check_Out"));
                setTime(pst,7,(String)values.get("Wed_Check_In"));
                setTime(pst,8,(String)values.get("Wed_Check_Out"));
                setTime(pst,9,(String)values.get("Thu_Check_In"));
                setTime(pst,10,(String)values.get("Thu_Check_Out"));
                setTime(pst,11,(String)values.get("Fri_Check_In"));
                setTime(pst,12,(String)values.get("Fri_Check_Out"));
                setTime(pst,13,(String)values.get("Sat_Check_In"));
                setTime(pst,14,(String)values.get("Sat_Check_Out"));
                pst.setInt(15, timingid);
                pst.executeUpdate();
                
                //now pipe details into the main info table
                pst = db.prepareStatement("select ID from db_staff.table_department where Name=?");
                pst.setString(1, ((String)values.get(fields.docdept)).toLowerCase());
                ResultSet res = pst.executeQuery();
                if(!res.next()){
                    //means the result is empty, then push new department into db
                    pst = db.prepareStatement("insert into db_staff.table_department set Name=?");
                    pst.setString(1, ((String)values.get(fields.docdept)).toLowerCase());
                    pst.executeUpdate();
                }
                //now get the department id
                Integer departmentid = null;
                pst = db.prepareStatement("select ID from db_staff.table_department where Name=?");
                pst.setString(1, ((String)values.get(fields.docdept)).toLowerCase());
                ResultSet result = pst.executeQuery();
                while(result.next()){
                    departmentid = result.getInt("ID");
                }
                
                pst = db.prepareStatement("delete from db_staff.table_staff where staff_ID=?");
                pst.setInt(1, docid);
                pst.executeUpdate();
                
                pst = db.prepareStatement("insert into db_staff.table_staff (Name,Gender,Address,Nationality,Qualification,Department_ID,Post,Specialist,Timing_ID,Enrolled_From,Contact,staff_ID) values(?,?,?,?,?,?,?,?,?,?,?,?)");
                pst.setString(1, (String)values.get(fields.name));  pst.setString(2,(String)values.get(fields.sex));    pst.setString(3, (String)values.get(fields.address));
                pst.setString(4, (String)values.get(fields.nationality));   pst.setString(5, (String)values.get(fields.qualification)); pst.setInt(6,departmentid );
                pst.setString(7,"doctor");  pst.setString(8, (String)values.get(fields.specialist));    pst.setInt(9,timingid); 
                pst.setString(10, (String)values.get(fields.enrolledfrom)); pst.setString(11, (String)values.get(fields.contact));
                pst.setInt(12, docid);   
                pst.executeUpdate();
                break;
            }
                
                
        }
        return true;}
    public static Boolean deleteEntry(HashMap<String,Object> values) throws SQLException{
        String department = (String)values.get(fields.department);
        switch (department){
            case staffid.receptionist:{
                updateOtherModifiedTime();
                loadserverconfig();
                //if  reception
                //use values.get(fields.*) *=member you want to access and push
                //multi statements deleting entry from auth and staff entry
                PreparedStatement pst = db.prepareStatement("delete from db_authentication.table_auth where client_id=?; delete from db_staff.table_reception where id=?;");
                pst.setInt(1, (Integer)values.get(fields.id));
                pst.setInt(2, (Integer)values.get(fields.id));
                pst.execute();
                break;
            }
            case staffid.doctor:{
                updateDocModifiedTime();
                loadserverconfig();
                //if  doctor
                //use values.get(fields.*) *=member you want to access and push
                //first get the timing id to remove timing details
                PreparedStatement pst = db.prepareStatement("select Timing_ID from db_staff.table_staff where staff_ID=?");
                pst.setInt(1, (Integer)values.get(fields.id));
                ResultSet rs = pst.executeQuery();
                Integer timingid=null;
                while(rs.next()){
                    timingid = rs.getInt("Timing_ID");
                }
                pst = db.prepareStatement("delete from db_staff.table_staff where staff_ID=?");
                pst.setInt(1, (Integer)values.get(fields.id));
                pst.executeUpdate();
                pst = db.prepareStatement("delete from db_staff.table_staff_timing where Timing_ID=?");
                pst.setInt(1,timingid);
                pst.executeUpdate();
                break;
            }
                                
        }
        return true;
    }
    
    public static List getdoctordetails() throws SQLException{
        PreparedStatement pst = db.prepareStatement("select Department_ID,Staff_ID,Name from db_staff.table_staff;");
        ResultSet rs = pst.executeQuery();
        List<List> det = new ArrayList();
        while(rs.next()){
            List<String> l = new ArrayList();
            Integer deptid = rs.getInt("Department_ID");
            Integer staffid = rs.getInt("Staff_ID");
            String name = rs.getString("Name");
            String deptName = "";
            PreparedStatement ps = db.prepareStatement("select Name from db_staff.table_department where ID = ?;");
            ps.setInt(1, deptid);
            ResultSet Rs = ps.executeQuery();
            while(Rs.next()){
                deptName = Rs.getString("Name");
            }
            l.add(deptName); l.add(String.valueOf(staffid)); l.add(name);
            det.add(l);
        }
        return det;
    }
    
    public static List getotherdetails() throws SQLException{
        PreparedStatement pst = db.prepareStatement("select * from db_staff.table_reception;");
        ResultSet rs = pst.executeQuery();
        List<List> det = new ArrayList();
        while(rs.next()){
            List<String> l = new ArrayList();
            Integer staffid = rs.getInt("id");
            String name = rs.getString("name");
            l.add(rs.getString("department")); l.add(String.valueOf(staffid)); l.add(name);
            det.add(l);
        }
        return det;
    }
    
    
    

    
    public static Boolean checkdbparams(String user,String pass){
        try {
            Connection database = DriverManager.getConnection("jdbc:mysql://localhost",user,pass);
            database.close();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }
    public static List<List<String>> getdocdepartments() throws SQLException{
        PreparedStatement pst = db.prepareStatement("select * from db_staff.table_department");
        ResultSet rs = pst.executeQuery();
        List<List<String>> vals = new ArrayList();
        while(rs.next()){
            List<String> val = new ArrayList();
            String name = rs.getString("Name");
            String id = String.valueOf(rs.getInt("ID"));
            val.add(id);
            val.add(name);
            vals.add(val);
        }
        return vals;
    }
    
    public static List<List<String>> getotherdepartments() throws SQLException{
        PreparedStatement pst = db.prepareStatement("select * from db_staff.departments_only");
        ResultSet rs = pst.executeQuery();
        List<List<String>> vals = new ArrayList();
        while(rs.next()){
            List<String> val = new ArrayList();
            String name = rs.getString("name");
            String id = rs.getString("id");
            if(!name.equalsIgnoreCase("doctor")){
                val.add(id);
                val.add(name);
                System.out.println(name);
                vals.add(val);
            }
        }
        return vals;
    }
    public static String toMilitary(Time t1,Time t2){
     if(t1==null || t2==null){return "";}
     
     String si[] = t1.toString().split(":");
     String so[]= t2.toString().split(":");
     String s = si[0]+si[1]+"-"+so[0]+so[1];
       return s;
    }
    
    public static HashMap<String,String> getDocInfo(String id) throws SQLException{
        Integer ID = Integer.valueOf(id);
        HashMap<String,String> val = new HashMap();
        PreparedStatement pst = db.prepareStatement("select * from db_staff.table_staff where staff_ID=?");
        pst.setInt(1, ID);
        ResultSet rs = pst.executeQuery();
        Integer deptid = null;
        Integer timingid = null;
        while(rs.next()){
            val.put(fields.name, rs.getString("Name")); 
            val.put(fields.sex, rs.getString("Gender"));
            val.put(fields.address,rs.getString("Address"));
            val.put(fields.nationality, rs.getString("Nationality"));
            val.put(fields.qualification,rs.getString("Qualification"));
            deptid = rs.getInt("Department_ID");
            val.put(fields.deptid, String.valueOf(deptid));
            val.put(fields.post, rs.getString("Post"));
            val.put(fields.specialist, rs.getString("Specialist"));
            val.put(fields.enrolledfrom, rs.getString("Enrolled_From"));
            timingid = rs.getInt("Timing_ID");
            val.put("timingid", String.valueOf(timingid));
            val.put(fields.contact, rs.getString("Contact"));
        }
        pst = db.prepareStatement("select * from db_staff.table_staff_timing where Timing_ID=?");
        pst.setInt(1, timingid);
        ResultSet rslt = pst.executeQuery();
        while(rslt.next()){
            val.put("1",toMilitary(rslt.getTime(2), rslt.getTime(3)));
            val.put("2",toMilitary(rslt.getTime(4), rslt.getTime(5)));
            val.put("3",toMilitary(rslt.getTime(6), rslt.getTime(7)));
            val.put("4",toMilitary(rslt.getTime(8), rslt.getTime(9)));
            val.put("5",toMilitary(rslt.getTime(10), rslt.getTime(11)));
            val.put("6",toMilitary(rslt.getTime(12), rslt.getTime(13)));
            val.put("7",toMilitary(rslt.getTime(14), rslt.getTime(15)));
        }
        pst = db.prepareStatement("select Name from db_staff.table_department where ID=?");
        pst.setInt(1, deptid);
        ResultSet Rs = pst.executeQuery();
        while(Rs.next()){
            val.put(fields.department, Rs.getString("Name"));
        }
        return val;        
    }
    
    public static HashMap<String,String> getOtherInfo(String id) throws SQLException{
        HashMap<String,String> res = new HashMap();
        Integer ID = Integer.valueOf(id);
        PreparedStatement pst = db.prepareStatement("select * from db_staff.table_reception where id=?");
        pst.setInt(1, ID);
        res.put(fields.id, String.valueOf(ID));
        ResultSet rs = pst.executeQuery();
        while(rs.next()){
            res.put(fields.name, rs.getString(1));
            res.put(fields.address, rs.getString(3));
            res.put(fields.contact, rs.getString(4));
            res.put(fields.enrolledfrom, rs.getString(5));
            res.put(fields.sex, rs.getString(6));
            res.put(fields.department,rs.getString(7));
        }
        pst = db.prepareStatement("select * from db_authentication.table_auth where client_id=?");
        pst.setInt(1,ID);
        ResultSet result = pst.executeQuery();
        while(result.next()){
            res.put(fields.username, result.getString(2));
            res.put(fields.password, result.getString(3));
        }
        
        return res;
    }
    
    public static void updateDocModifiedTime() throws SQLException{
        hmis.Layer.Time.Time t = new hmis.Layer.Time.Time();
        PreparedStatement pst = db.prepareStatement("update db_admin.serverconfig set user_last_modified_doc=\""+t.getDate()+"\";");
        pst.executeUpdate();
    }
    public static void updateOtherModifiedTime() throws SQLException{
        hmis.Layer.Time.Time t = new hmis.Layer.Time.Time();
        PreparedStatement pst = db.prepareStatement("update db_admin.serverconfig set user_last_modified_others=\""+t.getDate()+"\";");
        pst.executeUpdate();
    }
     public static List<List<String>> getLabAttribs() throws SQLException{
        PreparedStatement pst = db.prepareStatement("select * from db_laboratory.table_tests");
        ResultSet rs = pst.executeQuery();
        List<List<String>> result = new ArrayList();
        while(rs.next()){
            String test = rs.getString(1);
            Integer testid = rs.getInt(2);
            String attribs = rs.getString(3);
            Integer approxdays = rs.getInt(4);
            Double charge = rs.getDouble(5);
            List<String> val = new ArrayList();
            val.add(String.valueOf(testid)); val.add(test);  val.add(attribs); val.add(String.valueOf(approxdays)); val.add(String.valueOf(charge));
            result.add(val);
        }
        return result;
    }
    public static Integer addLabItem(List<String> val ) throws SQLException{
        PreparedStatement pst = db.prepareStatement("insert into db_laboratory.table_tests (Test,Attributes,Approx_Days,Price) values(?,?,?,?)");
         pst.setString(1, val.get(0));
         pst.setString(2, val.get(1));
         pst.setInt(3, Integer.valueOf(val.get(2)));
         pst.setDouble(4, Double.valueOf(val.get(3)));
         pst.executeUpdate();
         
         Integer id = null;
         pst = db.prepareStatement("select max(Test_ID) from db_laboratory.table_tests");
         ResultSet rs = pst.executeQuery();
         while(rs.next()){
             id = rs.getInt(1);
         }
         
        
         return id;
     }
    public static void deletelabitem(String id) throws SQLException{
        PreparedStatement pst = db.prepareStatement("delete from db_laboratory.table_tests where Test_ID=?");
        pst.setInt(1, Integer.valueOf(id));
        pst.executeUpdate();
    }
    public static void updatelabitem(String id,String test,String attr,String days,String cost) throws SQLException{
        PreparedStatement pst = db.prepareStatement("update db_laboratory.table_tests set Test=?,Attributes=?,Approx_Days=?,Price=? where Test_ID=?");
        pst.setString(1, test); pst.setString(2, attr); pst.setInt(3, Integer.valueOf(days)); pst.setDouble(4, Double.valueOf(cost)); pst.setInt(5, Integer.valueOf(id));
        pst.executeUpdate();
    }
    public static Boolean executeserver(){
        if(isrunnable&&!isrunning){
            //executes the server
            
            try {
                
               CoreThread mainThread = new CoreThread("HMIScore",port,100);
               main = mainThread;
               mainThread.execute();
               Thread.sleep(500);
                System.out.println(isStarted);
               if(!isStarted){throw new GlobalException(null,"","");}
               
               isrunning = true;
               return true;
            } catch (Throwable ex) {
                return false;
            }
        }else{
            //means, server is running, so we need to close it down
            
            try{
                if(maincore==null){
                    return false;
                }else{
                    //closing the core here
                    isrunning = false;
                    main.adminrequestsclose = true;
                    maincore.serversocket.close();
                    log.log("Core stopped, all services down!");
                    return false;
                }
            }catch(Throwable e){
                System.out.println("unable to close");
                e.printStackTrace();
                return true;
            }
            
        }
    }
    public static List<Client> listActiveClients(){
        HashMap store = CurrentClients.store;
        HashMap<Integer,String> key = CurrentClients.keystore;
        List<Client> clients = new ArrayList();
        for(int i=0;i<key.size();i++){
            if(store.containsKey(key.get(i))){
                clients.add((Client)(store.get(key.get(i))));
            }
        }
        return clients;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.DB.Queries;

import hmis.Shared.Identifiers.fields;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;

/**
 *
 * @author Rupak
 */

public class OPD_Reg_Query {
    PreparedStatement pstatement=null;
    Connection connection=null;
    HashMap<String,String> patient_info;
   // Tbl_patient_data patient;
    
    public OPD_Reg_Query(Connection c,HashMap<String,String>info){
        connection =c;
        patient_info=info;
    }
    
    public Boolean Insert_Details(){
        try{
            pstatement=connection.prepareStatement("INSERT INTO db_patient.table_patient (Patient_ID,Name,Gender,Age,Address,Contact_Number,Bloodgroup) VALUES (?,?,?,?,?,?,?);INSERT INTO db_patient.table_opd (Examination_id,Patient_ID,Doctor_ID,Diagnosed_With,Date_Of_First_Examination) VALUES(?,?,?,?,?);");
            pstatement.setInt(1, Integer.parseInt(patient_info.get(fields.patientid)));
            pstatement.setString(2, patient_info.get(fields.name));
            pstatement.setString(3, patient_info.get(fields.sex));
            pstatement.setInt(4, Integer.parseInt(patient_info.get(fields.age)));
            pstatement.setString(5, patient_info.get(fields.address));
            pstatement.setString(6,patient_info.get(fields.contact));
            pstatement.setString(7, patient_info.get(fields.bloodgroup));
            pstatement.setInt(8, Integer.parseInt(patient_info.get(fields.examination_id)));
            pstatement.setInt(9, Integer.parseInt(patient_info.get(fields.patientid)));
            pstatement.setInt(10, Integer.parseInt(patient_info.get(fields.doctorid)));
            pstatement.setString(11, patient_info.get(fields.diagnosed_with));
            pstatement.setDate(12, Date.valueOf(patient_info.get(fields.date_of_first_examination)));
            pstatement.executeUpdate();
            return true;
        }catch(SQLException e)
        {
           return false;
        }
        
    }
    
    
    public Boolean Update_Details(){
        try{
            pstatement=connection.prepareStatement("UPDATE db_patient.table_patient SET Age=?,Address=?,Contact_Number=? WHERE Patient_ID=?;INSERT INTO db_patient.table_opd (Examination_id,Patient_ID,Doctor_ID,Diagnosed_With,Date_Of_First_Examination) VALUES(?,?,?,?,?);");
            pstatement.setInt(1,Integer.parseInt(patient_info.get(fields.age)));
            pstatement.setString(2,patient_info.get(fields.address));
            pstatement.setString(3,patient_info.get(fields.contact));
            pstatement.setInt(4, Integer.parseInt(patient_info.get(fields.patientid)));
            pstatement.setInt(5, Integer.parseInt(patient_info.get(fields.examination_id)) );
            pstatement.setInt(6, Integer.parseInt(patient_info.get(fields.patientid)));
            pstatement.setInt(7,  Integer.parseInt(patient_info.get(fields.doctorid)));
            pstatement.setString(8,  patient_info.get(fields.diagnosed_with));
            pstatement.setDate(9,Date.valueOf(patient_info.get(fields.date_of_first_examination)));
            pstatement.executeUpdate();
            return true;
        }catch(SQLException e)
        {
            return false;
        }
        
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.DB.Queries;

import hmis.Shared.Identifiers.fields;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rupak
 */
public class Update_Ipd_Panel_Constraints_Query {


        
private Connection connection;
private PreparedStatement pstatement;
private ResultSet results;

public Update_Ipd_Panel_Constraints_Query(Connection c){
    connection=c;
}

public HashMap<String,Vector<room_info>> Get_Available_Rooms(){
    HashMap<String,Vector<room_info>> output=new HashMap();
    try{
        pstatement=connection.prepareStatement("SELECT a.Beds_Available,a.Category_ID,a.Room_No,b.Category FROM db_infrastructure.table_room a,db_infrastructure.table_room_category b WHERE a.Category_ID=b.Category_ID ORDER BY a.Category_ID;");
        results=pstatement.executeQuery();
        
        String prev_room_type="";
        String cur_room_type;
        
        
        
        while(results.next())
        {
         Vector<room_info> same_typed_rooms=new Vector();
         room_info single_room=new room_info();
         
         cur_room_type=results.getString("Category");
         single_room.room_no=results.getString("Room_No");
         single_room.category_id=results.getInt("Category_ID");
         single_room.beds_available=results.getString("Beds_Available");
         
         if(cur_room_type.equals(prev_room_type))
         {
             same_typed_rooms.removeAllElements();
             output.get(cur_room_type).add(single_room);
         }
         else{
             same_typed_rooms.add(single_room);
             output.put(cur_room_type, same_typed_rooms);
         }
         prev_room_type=cur_room_type;
        }
    }
    catch(SQLException e){
        e.printStackTrace();
    }
    System.out.println(""+output);
    return output;
    
}

    public HashMap<String,Vector<HashMap<String,String>>> Get_Available_Doctors()
    {
        HashMap<String,Vector<HashMap<String,String>>> output=new HashMap();
        try{
            pstatement=connection.prepareStatement("SELECT b.name,b.Staff_ID,a.name as 'Dept_Name' FROM db_staff.table_department a,db_staff.table_staff b where b.department_id=a.id AND b.post='Doctor' ORDER BY a.name;");
            results=pstatement.executeQuery();
  
            String prev_dept="";
            String cur_dept=null;
            
            while(results.next())
            {
                HashMap<String,String> single_info=new HashMap();
                Vector<HashMap<String,String>> same_dept_info=new Vector();
                
                cur_dept=results.getString("Dept_Name");
                
                single_info.put(fields.doctorid,String.valueOf(results.getInt("Staff_ID")));
                single_info.put(fields.doctor,results.getString("Name"));
                
                
                if(cur_dept.equals(prev_dept))
                {
                    same_dept_info.removeAllElements();
                    output.get(cur_dept).add(single_info);
                    
                }
                else{
                    same_dept_info.add(single_info);
                    output.put(cur_dept, same_dept_info);
                    
                }
                prev_dept=cur_dept;
            }
            
            }   
        catch(SQLException e)
            {
            e.printStackTrace();
            }
        
        return output;
    }
    
    public HashMap<String,String> Get_Patient_Info(int exam_id)
    {
        ResultSet result=null;
        HashMap<String,String> output=new HashMap<>();
        try{
        pstatement=connection.prepareStatement("SELECT a.Patient_ID, a.Name, a.gender, a.Address, a.Contact_Number, a.Bloodgroup from db_patient.table_patient a, db_patient.table_opd b WHERE a.Patient_ID=b.Patient_ID and b.Examination_ID=?;");
        pstatement.setInt(1,exam_id);
        result=pstatement.executeQuery();
        while(result.next()){
          
             output.put(fields.name,result.getString("Name"));
             output.put(fields.address, result.getString("Address"));
             output.put(fields.sex, result.getString("Gender"));
             output.put(fields.contact,result.getString("Contact_Number"));
             output.put(fields.patientid, result.getString("Patient_ID"));
        }
        }
        
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        return output;
        
    }
   
    private void Update_Details(HashMap<String,String> data){
        PreparedStatement pstatement=null;
        try{
            pstatement=connection.prepareStatement("UPDATE db_patient.table_patient SET Age=?, Address=?, Contact_Number=? ,Bloodgroup=? WHERE PATIENT_ID=?");
            System.out.println(""+data.get(fields.age));
            pstatement.setInt(1, Integer.parseInt(data.get(fields.age)));
            pstatement.setString(2, data.get(fields.address));
            pstatement.setString(3, data.get(fields.contact));
            pstatement.setString(4, data.get(fields.bloodgroup));
            pstatement.setInt(5, Integer.parseInt(data.get(fields.patientid)));
            pstatement.executeUpdate();
        }catch(SQLException e)
        {
            System.out.println("Error while updating data from IPD Panel");
            e.printStackTrace();
        }
        
        try {
            pstatement.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
   
    public Boolean Insert_IPD_Patient_Data(HashMap<String,String> data){
    try {
        pstatement=connection.prepareStatement("INSERT INTO db_patient.table_ipd (Examination_ID,Patient_ID,Doctor_ID,Diagnosed_With,Date_Of_Birth,Current_Room_Number,Current_Bed_Number,Guardian_Name,Guardian_Contact_Number) VALUES (?,?,?,?,?,?,?,?,?);INSERT INTO db_patient.table_opd_to_ipd_transfer_history (Examination_ID,Date_Of_Transfer) VALUES (?,?);");
        pstatement.setInt(1, Integer.parseInt(data.get(fields.examination_id)));
        pstatement.setInt(2, Integer.parseInt(data.get(fields.patientid)));
        pstatement.setInt(3, Integer.parseInt(data.get(fields.doctorid)));
        pstatement.setString(4,data.get(fields.diagnosed_with));
        pstatement.setDate(5,Date.valueOf(data.get(fields.date_of_birth)));
        pstatement.setString(6,data.get(fields.room_no));
        pstatement.setString(7,data.get(fields.bed_no));
        pstatement.setString(8,data.get(fields.guardian_name));
        pstatement.setString(9,data.get(fields.guardian_contact));
        pstatement.setInt(10,Integer.parseInt(data.get(fields.examination_id)));
        pstatement.setDate(11, Date.valueOf(data.get(fields.current_date)));  //current date is to be inserted
        //System.out.println(""+data.get(fields.current_date));
        pstatement.executeUpdate();
        
        Update_Details(data);
        return true;
    } catch (SQLException ex) {
        Logger.getLogger(Update_Ipd_Panel_Constraints_Query.class.getName()).log(Level.SEVERE, null, ex);
        return false;
    }
           
     
    }

}

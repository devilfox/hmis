/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.DB.Queries;

import hmis.Shared.Identifiers.fields;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rupak
 */
public class Update_Lab_Panel_Constraints_Query {
    PreparedStatement pstatement=null;
    Connection connection=null;
    public Update_Lab_Panel_Constraints_Query(Connection c){
     connection=c;   
    }
    
    public Boolean register_patient_for_test(HashMap<String,String> pt_details){
        ResultSet results;
        try {
            pstatement=connection.prepareStatement("INSERT INTO db_laboratory.table_patient (Name,Age,Gender,Address,Contact_Number,Registration_Date) VALUES(?,?,?,?,?,?);");
            pstatement.setString(1,pt_details.get(fields.name));
            pstatement.setInt(2,Integer.parseInt(pt_details.get(fields.age)));
            pstatement.setString(3,pt_details.get(fields.sex));
            pstatement.setString(4,pt_details.get(fields.address));
            pstatement.setString(5,pt_details.get(fields.contact));
            pstatement.setDate(6,Date.valueOf(pt_details.get(fields.registration_date)));
          //  pstatement.setDate(6,Date.valueOf("2014-01-01"));
           // java.sql.
            pstatement.executeUpdate();
            
            pstatement=connection.prepareStatement("SELECT Lab_Registration_No FROM db_laboratory.table_patient WHERE Name=? AND Contact_Number=?;");
            pstatement.setString(1, pt_details.get(fields.name));
            pstatement.setString(2, pt_details.get(fields.contact));
            results=pstatement.executeQuery();
            while(results.next())
            {
            pt_details.put(fields.lab_registration_no,String.valueOf(results.getInt("Lab_Registration_No")));    
            }
            
            System.out.println(""+pt_details.get(fields.lab_registration_no));
            System.out.println(""+pt_details.get(fields.test_id).split(",").length);
            pstatement=connection.prepareStatement("INSERT INTO db_laboratory.table_testreports (Lab_Registration_No,Test_ID,Results,Sample_Date,Report_Due_Date,Report_Availability_Status) VALUES(?,?,?,?,?,?);");
            
            for(String x:pt_details.get(fields.test_id).trim().split(",")){
            pstatement.setInt(1,Integer.parseInt(pt_details.get(fields.lab_registration_no)));
            pstatement.setInt(2,Integer.parseInt(x));
            pstatement.setString(3, "");
            pstatement.setDate(4,null);
            //this date holds no meaning as it will be modified during another query,yet needs to be set other than null
            pstatement.setDate(5,Date.valueOf("2014-04-01"));
            pstatement.setString(6,"NA");
            pstatement.executeUpdate();
            }
              return true;
            }
        
        catch (SQLException ex) {
            return false;
            //Logger.getLogger(Update_Lab_Panel_Constraints_Query.class.getName()).log(Level.SEVERE, null, ex);
        }
      
    }
   
    public List<HashMap<String,String>> Get_Available_Tests(){
        ResultSet results;
        List<HashMap<String,String>> available_test_list=new ArrayList();
        try {
            pstatement=connection.prepareStatement("SELECT Test,Test_ID FROM db_laboratory.table_tests;");
            results=pstatement.executeQuery();
            HashMap<String,String> single_info=new HashMap();
            while(results.next())
            {
                single_info.clear();
                single_info.put(fields.test,results.getString("Test"));
                single_info.put(fields.test_id,String.valueOf(results.getInt("Test_ID")));
                available_test_list.add(single_info);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Update_Lab_Panel_Constraints_Query.class.getName()).log(Level.SEVERE, null, ex);
        }
        return available_test_list;
    }    
    public HashMap<String,String> Get_All_Tests(){
        ResultSet results;
        HashMap<String,String> all_tests_list=new HashMap();
        try {
            pstatement=connection.prepareStatement("SELECT Test,Test_ID,Price FROM db_laboratory.table_tests;");
            results=pstatement.executeQuery();
            while(results.next()){
                all_tests_list.put(results.getString("Test"),String.valueOf(results.getInt("Test_ID"))+","+String.valueOf(results.getFloat("Price")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Update_Lab_Panel_Constraints_Query.class.getName()).log(Level.SEVERE, null, ex);
        }
        return all_tests_list;
    }
}
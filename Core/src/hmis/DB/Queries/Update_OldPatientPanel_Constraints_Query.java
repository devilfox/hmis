/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.DB.Queries;

import hmis.Shared.Identifiers.fields;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Rupak
 */
public class Update_OldPatientPanel_Constraints_Query {
    PreparedStatement pstatement=null;
    Connection connection=null;
    List<HashMap<String,String>> output=new ArrayList();
    
    public Update_OldPatientPanel_Constraints_Query(Connection c){
     connection=c;   
    }
    public List<HashMap<String,String>> Search_patient(String available_info,String type){
    ResultSet result=null;
    
    try{
        if(type.equals("n")){
        pstatement=connection.prepareStatement("Select Patient_ID,Name,Address,Contact_Number,Gender FROM db_patient.table_patient WHERE name LIKE CONCAT(?,'%');");
        pstatement.setString(1,available_info);
        }
        else if(type.equals("i"))
        {
        pstatement=connection.prepareStatement("Select Patient_ID,Name,Address,Contact_Number,Gender FROM db_patient.table_patient WHERE Patient_ID=?");
        pstatement.setInt(1,Integer.parseInt(available_info));
        }
        result=pstatement.executeQuery();
        
        while(result.next()){
            HashMap<String,String> single_patient_details=new HashMap();
            single_patient_details.put(fields.name,result.getString("Name"));
            single_patient_details.put(fields.address, result.getString("Address"));
            single_patient_details.put(fields.sex, result.getString("Gender"));
            single_patient_details.put(fields.contact,result.getString("Contact_Number"));
            single_patient_details.put(fields.patientid,result.getString("Patient_ID"));
            //Old_patient p=new Old_patient(result.getString("Name"),result.getString("Address"),result.getString("Gender"),result.getString("Contact_Number"),Integer.parseInt(result.getString("Patient_ID")));
            output.add(single_patient_details);
        }
        
    }catch(SQLException e)
    {
        System.out.println("Error while extracting patient information from database for old patient panel");
        e.printStackTrace();
    }
        return output;   
}
}

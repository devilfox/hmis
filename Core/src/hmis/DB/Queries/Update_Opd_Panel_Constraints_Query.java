/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.DB.Queries;

import hmis.Shared.Identifiers.fields;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Vector;

/**
 *
 * @author Rupak
 */

public class Update_Opd_Panel_Constraints_Query {
    PreparedStatement pstatement=null;
    public ResultSet results;
    
    Connection c;
    
    public Update_Opd_Panel_Constraints_Query(Connection conn)
    {
        c=conn;
    }
    
    public HashMap<String,Vector<HashMap<String,String>>> Update_Opd_Panel_Constraints()
    {
        HashMap<String,Vector<HashMap<String,String>>> output=new HashMap();
        try{
            pstatement=c.prepareStatement("SELECT b.name,b.Staff_ID,a.name as 'Dept_Name' FROM db_staff.table_department a,db_staff.table_staff b where b.department_id=a.id AND b.post='Doctor' ORDER BY a.name;");
            results=pstatement.executeQuery();
  
            String prev_dept="";
            String cur_dept=null;
            
            while(results.next())
            {
                HashMap<String,String> single_info=new HashMap();
                Vector<HashMap<String,String>> same_dept_info=new Vector();
                
                cur_dept=results.getString("Dept_Name");
                
                single_info.put(fields.doctorid,String.valueOf(results.getInt("Staff_ID")));
                single_info.put(fields.doctor,results.getString("Name"));
                
                
                if(cur_dept.equals(prev_dept))
                {
                    same_dept_info.removeAllElements();
                    output.get(cur_dept).add(single_info);
                    
                }
                else{
                    same_dept_info.add(single_info);
                    output.put(cur_dept, same_dept_info);
                    
                }
                prev_dept=cur_dept;
            }
            
            }   
        catch(SQLException e)
            {
                //e.printStackTrace();
            }
        
        return output;
    }
      
}

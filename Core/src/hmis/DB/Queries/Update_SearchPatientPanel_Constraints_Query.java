/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.DB.Queries;

import hmis.Shared.Identifiers.fields;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Rupak
 */
public class Update_SearchPatientPanel_Constraints_Query {
    PreparedStatement pstatement=null;
    Connection connection=null;
    List<HashMap<String,String>> output=new ArrayList();
    
    public Update_SearchPatientPanel_Constraints_Query(Connection c){
     connection=c;   
    }
    
    public List<HashMap<String,String>> Search_patient(String available_info,String type){
    ResultSet result=null;
    try{
        if(type.equals("n")){
        pstatement=connection.prepareStatement("SELECT a.Patient_ID,a.Name,a.Age,a.Address,a.Contact_Number,a.Gender,d.Name AS Doctor,e.Name AS Department,b.Diagnosed_With,c.Guardian_Name,c.Guardian_Contact_Number,c.Current_Room_Number,c.Current_Bed_Number FROM db_patient.table_patient a, db_patient.table_opd b,db_patient.table_ipd c,db_staff.table_staff d,db_staff.table_department e WHERE a.Name LIKE CONCAT(?,'%') AND b.Patient_ID=a.Patient_ID AND c.Patient_ID=a.Patient_ID AND d.Staff_ID=b.Doctor_ID AND e.id=d.Department_ID AND c.Discharged_On IS NULL;");
        pstatement.setString(1,available_info);
        }
        else if(type.equals("i"))
        {
        pstatement=connection.prepareStatement("SELECT a.Patient_ID,a.Name,a.Age,a.Address,a.Contact_Number,a.Gender,d.Name AS Doctor,e.Name AS Department,b.Diagnosed_With,c.Guardian_Name,c.Guardian_Contact_Number,c.Current_Room_Number,c.Current_Bed_Number FROM db_patient.table_patient a, db_patient.table_opd b,db_patient.table_ipd c,db_staff.table_staff d,db_staff.table_department e WHERE a.Patient_ID=? AND b.Patient_ID=a.Patient_ID AND c.Patient_ID=a.Patient_ID AND d.Staff_ID=b.Doctor_ID AND e.id=d.Department_ID AND c.Discharged_On IS NULL;");
        pstatement.setInt(1,Integer.parseInt(available_info));
        }
        result=pstatement.executeQuery();
        
        while(result.next()){
            HashMap<String,String> single_patient_details=new HashMap();
            single_patient_details.put(fields.name,result.getString("Name"));
            single_patient_details.put(fields.age, result.getString("Age"));
            single_patient_details.put(fields.address, result.getString("Address"));
            single_patient_details.put(fields.contact,result.getString("Contact_Number"));
            single_patient_details.put(fields.sex, result.getString("Gender"));
            single_patient_details.put(fields.doctor,result.getString("Doctor"));
            single_patient_details.put(fields.department,result.getString("Department"));
            single_patient_details.put(fields.patientid,result.getString("Patient_ID"));
            single_patient_details.put(fields.diagnosed_with,result.getString("Diagnosed_With"));
            single_patient_details.put(fields.guardian_name,result.getString("Guardian_Name"));
            single_patient_details.put(fields.guardian_contact,result.getString("Guardian_Contact_Number"));
            single_patient_details.put(fields.room_no,result.getString("Current_Room_Number"));
            single_patient_details.put(fields.bed_no,result.getString("Current_Bed_Number"));
            
            output.add(single_patient_details);
        }
        
    }catch(SQLException e)
    {
        System.out.println("Error while extracting patient information from database for Search Patient Panel");
        e.printStackTrace();
    }
        return output;   
}
}

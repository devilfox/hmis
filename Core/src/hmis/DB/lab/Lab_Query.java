/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.DB.lab;

import hmis.Shared.Identifiers.fields;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rupak
 */
public class Lab_Query {

    PreparedStatement pstatement = null;
    Connection connection = null;

    public Lab_Query(Connection c) {
        connection = c;
    }
    //THIS QUERY IS FOR POPULATING THE TABLE WITH GENERAL PATIENTS INFO PRESENT IN
    //LAB'S VIEW_ALL_PATIENT MODULE

    public List<HashMap<String, String>> Get_All_Patients_Info() {
        List<HashMap<String, String>> output = new ArrayList();
        ResultSet results;
        
        try {
            pstatement = connection.prepareStatement("SELECT a.Name,a.Age,a.Contact_Number,a.Address,a.Lab_Registration_No,b.Report_Due_Date,b.Report_Availability_Status FROM db_laboratory.table_patient a LEFT JOIN db_laboratory.table_testreports b ON b.Lab_Registration_No=a.Lab_Registration_No GROUP BY a.Lab_Registration_No;");
            
            //LEFT JOIN db_laboratory.table_testreports b ON b.Lab_Registration_No=a.Lab_Registration_No GROUP BY a.Lab_Registration_No;");
            
            results = pstatement.executeQuery();
            while (results.next()) {
                HashMap<String, String> single_pat_general_info = new HashMap();
                single_pat_general_info.put(fields.name, results.getString("Name"));
                single_pat_general_info.put(fields.age, String.valueOf(results.getInt("Age")));
                single_pat_general_info.put(fields.contact, results.getString("Contact_Number"));
                single_pat_general_info.put(fields.address, results.getString("Address"));
                single_pat_general_info.put(fields.lab_registration_no, String.valueOf(results.getInt("Lab_Registration_No")));
                output.add(single_pat_general_info);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Lab_Query.class.getName()).log(Level.SEVERE, null, ex);
        }
        return output;
    }
  
   public LabPatient Get_Single_Patient_Info(String lab_reg_no){
       System.out.println("lab_reg_no="+lab_reg_no);
        ResultSet results,results2;
        LabPatient single_patient_info=new LabPatient();
        List<LabTest> tests_list=new ArrayList();
        HashMap<String, String> single_pat_general_info = new HashMap();
        try {
            pstatement = connection.prepareStatement("SELECT a.Name,a.Age,a.Contact_Number,a.Address,a.Lab_Registration_No,a.Gender FROM db_laboratory.table_patient a WHERE a.Lab_Registration_No=?;");
            pstatement.setInt(1, Integer.parseInt(lab_reg_no));
            results=pstatement.executeQuery();
            
            PreparedStatement pstatement2=connection.prepareStatement("SELECT a.Test_ID,a.Results,a.Report_Due_Date,a.Report_Availability_Status,b.Test,b.Attributes FROM db_laboratory.table_testreports a LEFT JOIN db_laboratory.table_tests b ON a.Test_ID=b.Test_ID WHERE a.Lab_Registration_No=?;");
            pstatement2.setInt(1, Integer.parseInt(lab_reg_no));
            results2=pstatement2.executeQuery();
            
            while (results.next()) {
                single_pat_general_info.put(fields.lab_registration_no,results.getString("Lab_Registration_No"));
                single_pat_general_info.put(fields.name, results.getString("Name"));
                single_pat_general_info.put(fields.age, String.valueOf(results.getInt("Age")));
                single_pat_general_info.put(fields.contact, results.getString("Contact_Number"));
                single_pat_general_info.put(fields.address, results.getString("Address"));
                single_pat_general_info.put(fields.lab_registration_no, String.valueOf(results.getInt("Lab_Registration_No")));    
                single_pat_general_info.put(fields.sex, results.getString("Gender"));    
            }
            while(results2.next()){
                LabTest test=new LabTest();
                test.test_id=results2.getInt("Test_ID");
                test.test_name=results2.getString("Test");
                test.results=results2.getString("Results");
                test.rep_due_date=results2.getDate("Report_Due_Date").toString();
                test.rep_status=results2.getString("Report_Availability_Status");
                test.test_attribs=results2.getString("Attributes");
                tests_list.add(test);
            }
            single_patient_info.Set_Fields(single_pat_general_info, tests_list);
        } catch (SQLException ex) {
            Logger.getLogger(Lab_Query.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return single_patient_info;
    }
   
   public void change_test_status(String lab_regno,String test,String status){
        try {
//            if(status.equalsIgnoreCase("RDO")){
//            pstatement=connection.prepareStatement("UPDATE db_laboratory.table_testreports a SET a.Report_Availability_Status=?  WHERE a.Lab_Registration_No=? AND a.Test_ID=(Select Test_ID FROM db_laboratory.table_tests WHERE Test=?);");
//           }
            pstatement=connection.prepareStatement("UPDATE db_laboratory.table_testreports SET Report_Availability_Status=? WHERE Lab_Registration_No=? AND Test_ID=(Select Test_ID FROM db_laboratory.table_tests WHERE Test=?);");
            pstatement.setString(1, status);
            pstatement.setInt(2, Integer.parseInt(lab_regno));
            pstatement.setString(3, test);
            pstatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Lab_Query.class.getName()).log(Level.SEVERE, null, ex);
        }
   }
   public void save_testresults(HashMap<String,String> info){
        try {
            pstatement=connection.prepareStatement("UPDATE db_laboratory.table_testreports SET Results=? WHERE Lab_Registration_No=? AND Test_ID=?;");
            pstatement.setString(1, info.get(fields.test_attributes));
            pstatement.setString(2, info.get(fields.lab_registration_no));
            pstatement.setString(3, info.get(fields.test_id));
            pstatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Lab_Query.class.getName()).log(Level.SEVERE, null, ex);
        }
   }
}

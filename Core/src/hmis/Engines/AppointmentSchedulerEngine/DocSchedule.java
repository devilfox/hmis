/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hmis.Engines.AppointmentSchedulerEngine;

import java.io.Serializable;
import java.util.HashMap;

/**
 *
 * @author root
 * This will be used for appointment scheduling
 * 
 */

public class DocSchedule implements Serializable{
    private String doc_NAME;
    private String doc_ID;
    private String doc_DEPT;
    private Integer doc_reputation; //reputation score out of 10;
    private HashMap<Integer,String> schedule = new HashMap(); //the string should hold time in 24 hours format EG 0800-1400
                                                              //no shifts will be denoted by null
    private HashMap<Integer,Integer> existingAppointment = new HashMap();
    //This will hold existing appointments weightage only accessed by day key (0=sun,1=mon.....6=sat)
    
    //setting the parameters of the details object
    public void setParams(String name, String id, String dept,Integer rep, HashMap<Integer,String> sch,HashMap<Integer,Integer> existingappointments){
            doc_NAME = name;    doc_ID = id;    doc_DEPT = dept; schedule = sch; existingAppointment = existingappointments;    doc_reputation = rep;
    }
    
    public final Integer getCurrentAppWt(int day){
        if(day>=0 && day<=6){
            return existingAppointment.get(day);
        }
        return null;
    }
    public final String getName(){return doc_NAME;}
    public final String getID(){return doc_ID;}
    public final HashMap<Integer,String> getSchedule(){return schedule;}
    public final HashMap<Integer,Integer> getExisitingAppointments(){return existingAppointment;}
    public final Integer getReputation(){
        return doc_reputation;
    }
    public final String getScheduleByDay(Integer day){
        if(day>=0 && day<=6){
            return schedule.get(day);
        }
        return null;
    }
    public final Integer getNoOfAppointmentsOnDay(Integer Day){
            if(Day>=0 && Day<=6){
              if(existingAppointment!=null){
                return existingAppointment.get(Day);
              }else{
                  System.out.println(Day+" "+doc_NAME+ " nahiiii");
                  return 0;
              }
        }
        return 0;
    }
}

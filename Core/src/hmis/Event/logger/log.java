/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Event.logger;

import hmis.Admin.admin;
import hmis.Layer.Time.Time;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author daredevils
 * this will log all the events happening in the system
 */
public class log {
    private static Time time = new Time();
    public static List<String> logs =new ArrayList();
    private static Boolean savelogs = false;
    private static String logpath = "";
    private static Integer maxlogsize = 20;
    
    private static String lastDate;
    
    public static void loadDetails(){
        savelogs = admin.exportlog;
        logpath = admin.exportdir;
        maxlogsize = admin.logcount;
    }
    
    public static String extractdate(String date){
        String[] parts = date.split("-");
        return parts[0]+"-"+parts[1]+"-"+parts[2];
    }
    public static void log(final String log){
        try{
        String current = time.getcurrenttime();
        String currentdate = extractdate(current);
        
        logs.add("["+current+"]   "+log);
        hmis.Layer.gui.main.HomeScreen.log.log("["+current+"]   "+log);

        }catch(Throwable e){
//            e.printStackTrace();
        }
    }
    public static void clear(){logs.clear();}
    public static void exportlog() throws FileNotFoundException, IOException{
        if(savelogs){
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(logpath+"/"+lastDate+".log"))) {
                for(int i=0;i<logs.size();i++){
                    writer.write(logs.get(i));
                }
            }
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Layer.Core;

import hmis.Shared.Commobjects.primitive._rr;
import hmis.Shared.Exception.GlobalException;
import hmis.Layer.Time.Time;
import hmis.layer.DB.Manager.DBManager;
import java.io.*;
import java.net.*;

/**
 *
 * @author daredevils
 */
public class Client {
    
    boolean authen_params_set = false;
    //socket related
    protected Socket socket;
    protected String serverInfo;
    protected String address;
    protected DataInputStream IN;
    protected DataOutputStream OUT;
    protected ObjectInputStream OIS;
    protected ObjectOutputStream OOS;
    //authentication related
    private String clientName;
    private String client_id;
    private String auth_hive;
    private String dept_id;
    private String[] modules;
    private String[] rules_rw;
    private String[] rules_rd;
    
    public DBManager db;
    
    //time related
    private Time time;
    
    public Client(){
        time = new Time();
    }
    public String read() throws GlobalException{                                    //Reads the data from this client socket
        try{
            String getmsg = IN.readUTF();
            return getmsg;
        }
        catch(Throwable e){
            throw new GlobalException("#EX_0002","error reading data from socket",null);
        }
    }
   
    public void write(String message) throws GlobalException{                       //Writes data to this client socket
        try{
              OUT.writeUTF(message);
        }
        catch (Throwable e){
              throw new GlobalException("#EX_0003","error writing data to socket",null);
        }
    }
    public _rr readObj() throws GlobalException{
        try {
            return (_rr)OIS.readObject();
        } catch (Throwable ex) {
            throw new GlobalException("#EX_OBJRD","Error reading object from client",null);
        }
    }
    
    public void writeObj(_rr obj)throws GlobalException{
        try{
            OOS.writeObject(obj);
        }catch(Throwable e){
            throw new GlobalException("","Error writing object to client",null);
        }
    }
    
    public String getDetails(){                                                     //Returns details address and port no of the client
        return address;
    }
    
    public void close() throws GlobalException{                                    //closes the connection with the client
        try{
            socket.close();
        }
        catch(Throwable e)
        {
            throw new GlobalException("#EX_0005","error closing the socket",null);
        }
    }
     
    public void FromAuthentication(String name,String cli_id, String hive,String deptid,String[] module, String[] rw, String[] rd){
        clientName = name;
        authen_params_set = true;
        client_id = cli_id;
        auth_hive = hive;
        dept_id = deptid;
        modules = module;
        rules_rw = rw;
        rules_rd = rd;
    }
    
    public void UpdateTime(){
        time.refresh();
    }
    public String getusername() throws GlobalException{
            //current client ID
        if(authen_params_set){
            return clientName;
        }
        throw new GlobalException("client : ","Authentication parameters not set",null);
    }
    public String getid() throws GlobalException{
        //current client ID
        if(authen_params_set){
            return client_id;
        }
        throw new GlobalException("client : ","Authentication parameters not set",null);
    }
    
    public String getdeptid() throws GlobalException{
        //current clients' department id
        if(authen_params_set){
            return dept_id;
        }
        throw new GlobalException("client : ","Authentication parameters not set",null);
    }

}

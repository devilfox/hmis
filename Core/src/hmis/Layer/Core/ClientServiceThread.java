/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Layer.Core;

import hmis.Admin.admin;
import hmis.Event.logger.log;
import hmis.Layer.DB.Module.AuthenticationModule;
import hmis.Service.ClientHandler.MainHandler.MainHandler;
import hmis.Shared.Commobjects.primitive.Auth_Request;
import hmis.Shared.Commobjects.primitive.Auth_Response;
import hmis.Shared.Commobjects.primitive._rr;
import hmis.Shared.Exception.GlobalException;
import hmis.Shared.Identifiers.DBCodes;
import hmis.Shared.Identifiers.Dept;
import hmis.Shared.Identifiers.codes;
import hmis.layer.DB.Manager.DBManager;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author daredevils
 */
public class ClientServiceThread implements Runnable{
        private Thread thread = null;
        private Client client = null;                                                  //Client to which this thread will service
        private CurrentClients client_list = null;
    
    public ClientServiceThread(String identifier,Client getClient, CurrentClients cl){
        thread = new Thread(this,identifier);
        client = getClient;
        client_list = cl;
    }
    
    public void setAttribs(String identifier,Client getClient, CurrentClients cl){
        thread = new Thread(this,identifier);
        client = getClient;
        client_list = cl;
    }
    
    
    @Override
    public void run(){                                                          
        admin.threadscount++;
            //This is the method that will run when thread executes
//do the thread logic here
//1. receive the authentication parameter from client
//2. Check for the validity of the parameters and if ok, authenticate the client,load modules and rules specific to the client
//   and also check if the modules and rules loaded are valid
//   also register the client in the active clients list        
//3. Send the authenticated signal and client type to the client 
//4. Send the updated parameters from core to client
//5. Service the client (listen to requests);
        //flag will control the fate of this thread based on the exceptions generated
       Boolean flag = true; 
       while(flag){
     
        try {
            //this try-catch block will serve the client on the basis of the exceptions generated
            //first get the authentication object
            //This must be the authentication object according to HMIS protocol
            if(!admin.isrunning){
                flag = false;
            }
            
            log.log("Client connected, waiting for authentication parameters");
            
            _rr Authenticate = client.readObj();
            
            //check for the request id, if its' not the authentication request then raise exception here
            if(!Authenticate.getid().equalsIgnoreCase(codes.AuthenticationRequest)){
                log.log("Core expecting authentication parameters from the client :(");
                throw new GlobalException(codes.ExpectingDiffReq,"Expecting authentication parameters in the request",null);
            }
            
            //connect to the database and request the signin procedure
            DBManager dbConnect = new DBManager(DBCodes.dbName,DBCodes.user,DBCodes.pass);
            AuthenticationModule newSignIn = new AuthenticationModule(dbConnect,client);
            //provide client with its own connection to database
            
            
            //this will generate and throw error code to signify if the authentication is sucessful or not; and all will be handled
            //by catch statement accordingly
            Auth_Request req = (Auth_Request)Authenticate;
            newSignIn.authenticate(req.Username,req.Password);
           
            
        } catch (GlobalException ex) {                                          /*this will catch all the exception and switches to the respective handler*/
    
             switch(ex.getExceptionCode()){
                 case codes.Authenticated:{
                try {
                    System.out.println("<<authenticated>>");
                    //now check if the client has already authenticated
                    try{
                        Client isthere = client_list.Get(client.getid());
                    }catch (Throwable e){
                        //this means the client is new

                           log.log("Client \""+client.getid()+"\" sucessfully authenticated :)");
                        //sucessful authentication
                        //starting the client service protocol here as dictated by numbers 0 and on.
                            System.out.println("------------------------------------------------------------");
                            System.out.println("Core: ["+client.getid()+ ","+client.getdeptid()+"] Authenticated Sucessfully");
                            System.out.println("Core: Get Client ID...DONE "+client.getid());
                            System.out.println("Core: Get Department Name...DONE "+Dept.getDeptNameByID(client.getdeptid()));
                            System.out.println("Core: Listing client to the active list...DONE");
                            System.out.println("Core: Updating client Login time...DONE");
                            System.out.println("Core: Sending successful response to client...DONE");
                            System.out.println("Core: Sending Module Updates to client...DONE");
                            System.out.println("Core: Handing the Client to MainHandler for further Service...DONE");
                            System.out.println("Core: All SYSTEMS OK");
                            System.out.println("------------------------------------------------------------");
                            System.out.println("");     
                        //0. update the client signin time
                        client.UpdateTime();
                        //0. Add the client to the active client list
                        client_list.Insert(client.getid(), client);

                        //1. Send the authentication sucessful signal
                            //this includes creating response and send it to the client
                            //--
                            Auth_Response response = new Auth_Response();
                            response.isSuccess = true;  response.deptid = client.getdeptid();
                            response.username = client.getusername();  response.clientid = client.getid();
                            client.writeObj(response);

                        //2. Service the clients request
                           //calling service handler and passing it the current client
                            MainHandler handle = new MainHandler(((Client)ex.getObject()));
                        //then the handle will execute the service
                        //exception can be raised in case the client disconnects, so handling the exception
                        try{
                            handle.executeService();
                        }catch(GlobalException es){
                            //decide the fate of the thread, i.e exit the thread
                            //remove the entry from the client list
                            //This will create problem if there are multiple clients with same login
                            //So, unique login identification logic is required to be developed to mitigate it
                            client_list.Delete(client.getid());
                              log.log("Client disconnected!!");
                            System.out.println("-------------------------------------------------");
                            System.out.println("Core: ["+client.getid()+ ","+client.getdeptid()+"] has been disconnected");
                            System.out.println("Core: Clearing the fingerprints for "+thread.getName());
                            System.out.println("Core: All SYSTEMS OK");
                            System.out.println("-------------------------------------------------");
                            System.out.println("");
                            //close db connection
                            client.db.close();
                            //Now set the flag to exit the thread
                            flag = false;
                         }
                        break;
                    }//End of case for new client, now we create an else type code logic here
                    
                    //Else, the client has already logged in then we'll send a reply to this client 
                    //stating that it cannot use the same client 
                    //close its db connection
                   // client.db.close();
                    log.log("Client \""+client.getid()+"\" tried multiple login, notifying and disconnecting client :)");
                    Auth_Response response = new Auth_Response();
                    response.setid(codes.AuthenticationRequest);
                    response.isSuccess = false;
                    //Tell the client that it is trying to login with already logged on credentials
                    response.isMultipleInstance = true;
                    client.writeObj(response);
                    
                    break;
                    } catch (GlobalException ex1) {
                         System.out.println(ex1.getExceptionDesc());
                    }
                    break;
                 }
                 
                 
                 case codes.voidAuthentication:{
                     //case when authentication is invalid
                        try {
                            Auth_Response response = new Auth_Response();
                            response.setid(codes.AuthenticationRequest);
                            response.isSuccess = false;
                            //Tell the client that it is actually a void authentication
                            response.isMultipleInstance = false;
                            client.writeObj(response);
                        log.log("Client failed to authenticate :(");
                            System.out.println("-------------------------------------------------");
                            System.out.println("Core: client will be disconnected");
                            System.out.println("Core: Authentication Failed");
                            System.out.println("Core: All SYSTEMS OK");
                            System.out.println("-------------------------------------------------");
                            System.out.println("");
                            flag = false;
                            client.OOS.flush();
                            client.db.close();
                        } catch (Throwable ex1) {
                            
                        }
                     break;
                 }
                     
                 case codes.ErrorConnectingDB:{
                     //DB server is not available
                     try {
                         System.out.println("-------------------------------------------------");
                         System.out.println("Core: All systems Disconnecting");
                         System.out.println("Core: Connecting to Database failed");
                         System.out.println("Core: All systems NOT OK");
                         System.out.println("-------------------------------------------------");
                         System.out.println("");
                         //System.out.println(ex.getExceptionDesc());
                     } catch (Throwable globalException) {
                     }
                     break;
                 }
                     
                 case codes.ExpectingDiffReq:{
                     //If request received is different than expected
                     System.out.println(ex.getExceptionDesc());
                     break;
                 }
                  
                 case codes.RulesError:{
                     //if the rules and modules don't agree
                     System.out.println(ex.getExceptionDesc());
                     break;
                 }
                 case codes.ObjectReadError:{
                try {
                    //it means that client has been disconnected
                    //System.out.println(ex.getExceptionDesc() + " -> "+"Client has been disconnected");
                    //discontinue this thread now by clearing the flag value to false
                    if(client!=null&client.db!=null){
                        client.db.close();
                    }
                } catch (GlobalException ex1) {
                    //Logger.getLogger(ClientServiceThread.class.getName()).log(Level.SEVERE, null, ex1);
                }
                     System.out.println("-------------------------------------------------");
                 try {
                     System.out.println("Core: ["+client.getid()+ ","+client.getdeptid()+"] will be disconnected");
                 } catch (GlobalException ex1) {
                     System.out.println("Core: Client Disconnected without authentication transaction");
                     
                 }
                    log.log("Client disconnected");
                         System.out.println("Core: Client sent illegal request");
                         System.out.println("Core: All SYSTEMS OK");
                         System.out.println("-------------------------------------------------");
                         System.out.println("");
                     flag = false;
                     break;
                 }
                     
             }
        }
       }
            try {
                //after the event has been out of the thread(client is no longer need to ne serviced)-
                //then, try closing the client
                client.close();
            } catch (GlobalException ex) {
                //if the client is already disconnected, then handle the execption safely
                Logger.getLogger(ClientServiceThread.class.getName()).log(Level.SEVERE, null, ex);
            }
         admin.threadscount--;
    }
   
    public void execute(){
        //execute this thread
        thread.start();
    }
    
    public void Join() throws GlobalException{
        try{
            thread.join();
        }
        catch(Throwable e){            
                throw new GlobalException("EX_0006",thread.getName()+ ": thread exiting",null);    
        }
    }
    
    public String IdentifyThread(){                                                   //returns threadname
        return thread.getName();
    }
}

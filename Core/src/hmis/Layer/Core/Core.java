/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Layer.Core;

import hmis.Shared.Exception.GlobalException;
import java.io.*;
import java.net.*;


/**
 *
 * @author daredevils
 */

public class Core extends Client{
   public ServerSocket  serversocket;                                              //This is the socket handler of the server
   private int port;                                                              //This is to hold the port number for server to listen on
   private int backlog;                                                             //This will limit the no of clients to the server
   
   public Core(int Port, int bklog) throws GlobalException{
       port = Port;
       backlog = bklog;
       
       try{
           serversocket = new ServerSocket(port);
       }
       catch(Throwable e){
           throw new GlobalException("EX_0001","cannot bind server to interface in specific port",null);
       }
   }
   
   public Client InitiateServer() throws GlobalException{
       try{
            Client newClient = new Client();                                               //Initiating a client var;
            newClient.socket = serversocket.accept();                                     //Accepting the incoming client connection(Blocking Socket)
            newClient.serverInfo = newClient.socket.getLocalSocketAddress().toString();   //This is the servers data
            newClient.address = newClient.socket.getRemoteSocketAddress().toString();     //Storing the clients info   
            try{
                newClient.IN = new DataInputStream(newClient.socket.getInputStream());         //Storing Input stream(to client) to object
                newClient.OUT = new DataOutputStream(newClient.socket.getOutputStream());       //Storing Output Stream to object
                newClient.OOS = new ObjectOutputStream(newClient.socket.getOutputStream());    //Object Output Stream
                newClient.OIS = new ObjectInputStream(newClient.socket.getInputStream());       //Object input stream
            } catch(Throwable ta){
                throw new GlobalException("x","error Objectstream",newClient);
            }
            return newClient;
       }
       catch(GlobalException e){
               throw e;
       } catch(Throwable ex){
           throw new GlobalException("#EX_0004","error initialising server",null);
       }
           
   }
   public String getHostIP(){
       return serversocket.getLocalSocketAddress().toString();
   }
   
}

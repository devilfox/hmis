/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Layer.Core;

import hmis.Admin.admin;
import hmis.Event.logger.log;
import hmis.Shared.Exception.GlobalException;
import hmis.Shared.Identifiers.DBCodes;
import hmis.layer.DB.Manager.DBManager;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author daredevils
 * 
 *  usage:
 *  CoreThread mainThread = new CoreThread("HMIScore",1010,10);
 *  mainThread.execute();
 * 
 */

public class CoreThread implements Runnable{
    public Boolean adminrequestsclose=false;
    private Thread thread;
    private boolean flag = true;                                                //flag will determine the status and fate of this thread
    
    private int port;
    private int backlog;
    private Core mainCore;                                                      //instance of the core
    private CurrentClients client_list;
    private static int clients_track = 0;
    
    public CoreThread(String identifier,int Port,int no_of_clients){
        thread = new Thread(this,identifier);
        port = Port;
        backlog = no_of_clients;
        client_list = new CurrentClients();
        
    }
    
    @Override
    public void run(){                                           
//This is the method that will run when thread executes
//do the thread logic here
//1. Open the core for connections
//2. Get a client and push it to the client service thread
        admin.threadscount++;
//Create the instance of the core;      
     try{
            
        try {
            //Create the instance of the core;
            //This is where actually core is created
            log.log("<<Initiating core checklist>>");
            mainCore = new Core(port,backlog);                                  
            admin.maincore = mainCore;
            log.log("Port Check ---- no confict,OK");
            //0. Check if the database server is up? will be using minimal rights authentication later
            DBManager dbConnect = new DBManager(DBCodes.dbName,DBCodes.user,DBCodes.pass);
            dbConnect.close();
            log.log("Database Check ---- up and running,OK");
            log.log("Core sucessfully running on "+admin.address+" at port "+admin.port);
            log.log("<<Core checklist completed>>");
            admin.isStarted = true;
            System.out.println("-------------------------------------------------");
            System.out.println("Core: HMIS Checklist intitated");
            System.out.println("Core: Port Check... complete OK");
            System.out.println("Core: Database Connectivity Check... complete OK");
            System.out.println("Core: Checklist Complete, ALL SYSTEMS OK");
            System.out.println("Core: Core listening on " + mainCore.getHostIP());
            System.out.println("-------------------------------------------------");
            System.out.println("");
            
            
            
            
        } catch (GlobalException ex) {
            //any raised exception due to startup error is caught here and rethrown
            log.log("<<Checklist failed, Core unable to start, Please check port or database connectivity>>");
            throw ex;
        }
        while(flag){                                                        
            //flag will track upto when the server is to listen
            try{        
                    //1. Now listen for clients and create the client service thread
                    //Initiating server and get the clients from the server
                       Client newClient = mainCore.InitiateServer();                
                     //Add the client connecting to server into its service thread
                       String client_thread_id = "client_"+String.valueOf(clients_track);
                       clients_track = clients_track+1;
                       System.out.println(client_thread_id);
                       client_list.Insert(client_thread_id, newClient);
                       ClientServiceThread CLThread = new ClientServiceThread(client_thread_id,newClient,client_list);
                     //execute the client Service thread
                       CLThread.execute();
                }
            catch(GlobalException ex){
                
                //this section will hold all exceptions including exception raised
                //due to unethical handling via telnet or other clients
                
                if(adminrequestsclose){
                    flag=false;
                    client_list.closeall();
                    log.log("admin requested to close down the server !!");
                    System.out.println("admin requesting to close the server");
                    throw new GlobalException(null,"","");
                }else{
                    System.out.println(ex.getExceptionCode()+ex.getExceptionDesc());
                    if(ex.getObject()!=null){
                        //means unethical handling detected
                        //so server will close connection to this client
                        Client c = (Client)ex.getObject();
                        try {
                            c.close();
                        } catch (GlobalException ex1) {
                            Logger.getLogger(CoreThread.class.getName()).log(Level.SEVERE, null, ex1);
                        }
                    }
                }
            }
        }
    } catch (GlobalException e){ 
        //this will catch any exception that will be raised due to improper subsystems
            System.out.println("-------------------------------------------------");
            System.out.println("Core: HMIS exiting");
            System.out.println("Core: "+e.getExceptionCode()+" "+e.getExceptionDesc());
            System.out.println("Core: All SYSTEMS NOT OK");
            System.out.println("-------------------------------------------------");
            System.out.println("");
            
        }
        System.out.println("finished");
        admin.isStarted = false;
        admin.threadscount--;
    }
   
    public void execute(){                                                      //Execute the thread method
        thread.start();
    }
    
    public void Join() throws GlobalException{
        try{
            thread.join();
        }
        catch(Throwable e){            
                throw new GlobalException("EX_0006",thread.getName()+ ": thread exiting",null);    
        }
    }
    
    
    public String Identify(){                                                   //returns threadname
        return thread.getName();
    }
    

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Layer.Core;

import hmis.Shared.Exception.GlobalException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author daredevils
 */
public class CurrentClients {
    public static HashMap store = new HashMap();
    public  static HashMap<Integer,String> keystore = new HashMap();
    
    private static int count = 0;
    public void Insert(String key,Client client){
        store.put(key,client);
        keystore.put(count++,key);
    }
    
    public Client Get(String key)throws GlobalException{
        Object val = store.get(key);
        if(val == null){
            throw new GlobalException("get: ","HashMap, cannot find the key",null);
        }
        return (Client)val;
    }
    
    public void Delete(String key) throws GlobalException{
        Object val = store.remove(key);
        if(val == null){
            throw new GlobalException("delete: ","HashMap, cannot find the key",null);
        }
        //now manage keystore
        
    }
    public void closeall(){
        for(int i=0;i<keystore.size();i++){
            if(store.containsKey(keystore.get(i))){
                try {
                    ((Client)store.get(keystore.get(i))).close();
                } catch (GlobalException ex) {
                    Logger.getLogger(CurrentClients.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    public boolean ContainsKey(String key){
        return store.containsKey(key);
    }
    
    public int Size(){
        return store.size();
    }
    
    public void Clear(){
        store.clear();
    }
    
    public void show(){
        System.out.println(store.size());
    }
    
}

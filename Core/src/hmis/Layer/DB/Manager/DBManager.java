/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.layer.DB.Manager;
import hmis.Shared.Exception.GlobalException;
import java.sql.*;

/**
 *
 * @author daredevils
 */
public class DBManager {
    private Connection connect = null;
    private String url;
    private PreparedStatement pst = null;
    private String user;
    private String pass;
    
    public DBManager(String URL, String USER, String PASS) throws GlobalException{
        url = URL;
        user = USER;
        pass = PASS;
        try {
            connect = DriverManager.getConnection(url,user,pass);
        } catch (SQLException ex) {
            throw new GlobalException("#EX_0010","unable to connect to mysql database server",null);
        }
    }
    
    public Connection getConnection(){                                                              //returns connection var to be used by other modules
        return connect;
    }
    
    
    
    public void close()throws GlobalException{
        try {
            connect.close();
        } catch (SQLException ex) {
            throw new GlobalException("#EX_0013","error closing connection to mysql server",null);
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
    }
    
    
}

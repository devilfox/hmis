/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Layer.DB.Module;
import hmis.Layer.Core.Client;
import hmis.Shared.Exception.GlobalException;
import hmis.layer.DB.Manager.DBManager;
import java.sql.*;
/**
 *
 * @author daredevils
 */
public abstract class AbstractModule {
    protected DBManager db;
    protected Client client;                                                    //this will furnish the client var with the informations
    
    protected AbstractModule(DBManager var,Client cli){
        db = var;
        client = cli;
    }
    protected Boolean isResultEmpty(ResultSet rs) throws GlobalException{          //if it returns false; always use do while loop to access the rows
        try{
            if(!rs.next()){
                return true;
            }
            return false;
        }
        catch(Throwable ex){
            throw new GlobalException("","",null);
        }
    }
    protected Boolean has(String s,char ch){
        if(s.indexOf(ch)==-1){
            return false;
        }
        return true;
    }
}

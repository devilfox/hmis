/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Layer.DB.Module;


import hmis.Layer.Core.Client;
import hmis.Shared.Exception.GlobalException;
import hmis.layer.DB.Manager.DBManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author daredevils
 * 
 * usage:
 * DBManager df = new DBManager("jdbc:mysql://localhost:3306/db_authentication","hmis","bishalshrestha");
 * AuthenticationModule newmodule = new AuthenticationModule(df);
 * newmodule.authenticate("bibek","shrestha");
 * 
 */
public class AuthenticationModule extends AbstractModule{
    private String auth_query = "SELECT * FROM db_authentication.table_auth WHERE username=? AND pass=?";
    private String auth_hive_query = "SELECT * FROM db_authentication.table_auth_hive WHERE hive=?";
    private String rules_query = "SELECT * FROM db_authentication.table_rules WHERE dept_id=?";
    
    
    private String client_id;
    private String auth_hive;
    private String dept_id;
    private String modules;
    private String rules_rw;
    private String rules_rd;
    private String[] Modules; //splitted
    private String[] Rules_RD_WR; //this is the splitted pieces
    private String[] Rules_RD;    //contains splitted pieces
    
    
    public AuthenticationModule(DBManager var,Client cli){
        super(var,cli);
    }
    
    
    
    public void authenticate(String user, String pass) throws GlobalException{
        try {
            System.out.println("authenticating");
            PreparedStatement pst1 = db.getConnection().prepareStatement(auth_query);
            pst1.setString(1,user);
            pst1.setString(2, pass);
            ResultSet result1  = pst1.executeQuery();
            
            if(isResultEmpty(result1)){
                throw new GlobalException("#EX_0012","authentiation unsucessful",null);
            }
            
            do{
                client_id = result1.getString("client_id");
                auth_hive = result1.getString("auth_hive");
                dept_id = result1.getString("dept_id");
            }while(result1.next());
            
            PreparedStatement pst2 = db.getConnection().prepareStatement(auth_hive_query);
            pst2.setString(1,auth_hive);
            ResultSet result2 = pst2.executeQuery();
            
            if(isResultEmpty(result2)){
                throw new GlobalException("#EX_0013","database error : hive",null);
            }
            do{
                modules = result2.getString("modules");
            }while(result1.next());
            
            
            PreparedStatement pst3 = db.getConnection().prepareStatement(rules_query);
            pst3.setString(1,dept_id);
            ResultSet result3 = pst3.executeQuery();
            
            
            if(isResultEmpty(result3)){
                throw new GlobalException("#EX_0014","database error : rules",null);
            }
            do{
                rules_rw = result3.getString("rd_wr");
                rules_rd = result3.getString("rd");
            }while(result3.next()); 
            db.close();
            
            //split the modules and rules of the client
            if(has(modules,',')){
                Modules = modules.split(",");
            }
            if(has(rules_rw,',')){
                Rules_RD_WR = rules_rw.split(",");
            }
            if(has(rules_rd,',')){
                Rules_RD = rules_rd.split(",");
            }
            
            //now check the validity of the rules and modules
            isSubset(Modules,Rules_RD_WR);
            isSubset(Modules,Rules_RD);
            
            client.FromAuthentication(user,client_id, auth_hive, dept_id, Modules, Rules_RD_WR, Rules_RD);
            
            throw new GlobalException("#EX_0014","authentication sucessful",client);
            
        } catch (GlobalException ex) {
            throw ex;
        }
        catch(SQLException e){
            throw new GlobalException("#EX_0011","error running the query",null);
        }
        
        
    }
    
    private void isSubset(String[] a, String[] b) throws GlobalException{ 
        //checks if a is subset of b
        int count = 0;
        for(int i=0;i<a.length;i++){
            for(int j=0;j<b.length;j++){
                if(a[i].equalsIgnoreCase(b[j])){
                    count++;
                }
            }
        }
        if(!(count==b.length)){
            throw new GlobalException("#EX_0015","db : modules and rules don't match",null);
        }
    }
}

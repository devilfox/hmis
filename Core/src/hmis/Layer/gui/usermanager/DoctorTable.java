/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Layer.gui.usermanager;

import hmis.Admin.admin;
import hmis.Shared.Identifiers.fields;
import hmis.Shared.Identifiers.staffid;
import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Bhuwan
 */
public class DoctorTable extends basepanel {
    
    /**
     * Creates new form DoctorTable
     */
    String editingid = null;
    String editingtimingid = null;
    
    
    Boolean isclicked = false;
    UserManager usermanager;
    Boolean isnew = false;
    String currentDept;
    String currentID;
    String currentname;
    public DoctorTable(UserManager um) {
        initComponents();
        initEvents();
        usermanager = um;
        newdepart_textfield.setEnabled(false);
        doctor_table.setRowHeight(25);
        doctor_schedule_table.setRowHeight(25);
        populate();
    }
    public DoctorTable(UserManager um,List<List<String>> l){
        usermanager = um;
        initComponents();
        initEvents();
        newdepart_textfield.setEnabled(false);
        doctor_table.setRowHeight(25);
        doctor_schedule_table.setRowHeight(25);
        
            DefaultTableModel table = (DefaultTableModel) doctor_table.getModel();
            List<List<String>> doctor_list = l;
            
        for(int i=0;i<doctor_list.size();i++)
        {
            int j=0;
            table.addRow(new Object[]{doctor_list.get(i).get(j++),
                                  doctor_list.get(i).get(j++),
                                  doctor_list.get(i).get(j++)
                                });
        }
        
    }
    
    public final void populate(){ 
        try {
            DefaultTableModel table = (DefaultTableModel) doctor_table.getModel();
            List<List<String>> doctor_list = admin.getdoctordetails();
            
        for(int i=0;i<doctor_list.size();i++)
        {
            int j=0;
        table.addRow(new Object[]{doctor_list.get(i).get(j++),
                                  doctor_list.get(i).get(j++),
                                  doctor_list.get(i).get(j++)
                                });
        }
        } catch (SQLException ex) {
            Logger.getLogger(DoctorTable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void action_on_search_button_click()
    {
        button_panel.setLayout(new BorderLayout());
        button_panel.removeAll();
       button_panel.add(new SearchPanel(this));
       this.revalidate();
       this.repaint();
    }
    
     public void action_on_new_button_click()
     {
         clear();
         isnew = true;
         edit_dialog.setBounds(150,130,850,420);
         edit_dialog.setVisible(true);
         edit_dialog.setResizable(false);
         department_combobox.removeAllItems();
        try {
            List<List<String>> val = admin.getdocdepartments();
            for(List<String> v:val){
                department_combobox.addItem(v.get(1));
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(DoctorTable.class.getName()).log(Level.SEVERE, null, ex);
        }
     }
     
     public JPanel getpanel()
     {
         return table_panel;
     }
     
     @Override
     public Boolean onSearch(String id){
         doctor_schedule_table.removeAll();
         try {
            
            
            List<List<String>> doctor_list = admin.getdoctordetails();
            
        for(int i=0;i<doctor_list.size();i++){
            int j=0;
            if(doctor_list.get(i).get(1).equalsIgnoreCase(id)){

                List<List<String>> l = new ArrayList();
                List<String> lst = new ArrayList();
                lst.add(doctor_list.get(i).get(j++));
                lst.add(doctor_list.get(i).get(j++));
                lst.add(doctor_list.get(i).get(j++));
                l.add(lst);
                usermanager.docpanelrefresh(l);
                return true;
            }
        }
        } catch (SQLException ex) {
            Logger.getLogger(DoctorTable.class.getName()).log(Level.SEVERE, null, ex);
        }
         
         return false;
     }
     
     private void initEvents(){
         doctor_table.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
         doctor_table.addMouseListener(new java.awt.event.MouseAdapter() {
             @Override
             public void mouseClicked(java.awt.event.MouseEvent evt){
                 int row = doctor_table.getSelectedRow();
                 int column = 0;
                 if(evt.getClickCount()>1){
                    // String value = (String)doctor_table.getValueAt(row, column);
                     onDoubleClick((String)doctor_table.getValueAt(row, 1));
                 }else if (evt.getClickCount()==1){
                     currentDept = (String)doctor_table.getValueAt(row, 0);
                     currentID = (String)doctor_table.getValueAt(row, 1);
                     currentname = (String)doctor_table.getValueAt(row, 2);
                     isclicked = true;
                 }
             }
         });
     }
     
     public void onDoubleClick(String id){
        try {
            editingid = id;
            HashMap<String,String> val = admin.getDocInfo(id);
            editingtimingid = val.get("timingid");
            isnew = false;
            edit_dialog.setBounds(150,130,850,420);
            edit_dialog.setVisible(true);
            edit_dialog.setResizable(false);
            List<List<String>> vals = admin.getdocdepartments();
            department_combobox.removeAllItems();
            for(List<String> v:vals){
                department_combobox.addItem(v.get(1));
                if(v.get(1).equalsIgnoreCase(val.get(fields.department).toLowerCase())){
                    department_combobox.setSelectedItem(v.get(1));
                }
            }
            
            gender_combobox.removeAllItems();
            gender_combobox.addItem("male");
            gender_combobox.addItem("female");
            gender_combobox.addItem("other");
            if(val.get(fields.sex).equalsIgnoreCase("m")){
                gender_combobox.setSelectedItem("male");
            }else if(val.get(fields.sex).equalsIgnoreCase("f")){
                gender_combobox.setSelectedItem("female");
            }else{
                gender_combobox.setSelectedItem("other");
            }
                
            name_textfield.setText(val.get(fields.name));
            address_textfield.setText(val.get(fields.address));
            contact_textfield.setText(val.get(fields.contact));
            nationality_textfield.setText(val.get(fields.nationality));
            qualification_textfield.setText(val.get(fields.qualification));
            specialist_textfield.setText(val.get(fields.specialist));
            enrolledfrome_textfield.setText(val.get(fields.enrolledfrom));
            contact_textfield.setText(val.get(fields.contact));
            for(int i=1;i<=7;i++){
                doctor_schedule_table.setValueAt(val.get(String.valueOf(i)), 0, i-1);
            }
            
            
        
        } catch (SQLException ex) {
            Logger.getLogger(DoctorTable.class.getName()).log(Level.SEVERE, null, ex);
        }
     }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        edit_dialog = new javax.swing.JDialog();
        main_panel = new javax.swing.JPanel();
        element_panel = new javax.swing.JPanel();
        name_label = new javax.swing.JLabel();
        name_textfield = new javax.swing.JTextField();
        address_labe = new javax.swing.JLabel();
        address_textfield = new javax.swing.JTextField();
        contact_label = new javax.swing.JLabel();
        contact_textfield = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        nationality_textfield = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        qualification_textfield = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        specialist_textfield = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        enrolledfrome_textfield = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        gender_combobox = new javax.swing.JComboBox();
        department_label = new javax.swing.JLabel();
        department_combobox = new javax.swing.JComboBox();
        depart_button = new javax.swing.JRadioButton();
        jLabel6 = new javax.swing.JLabel();
        newdepart_textfield = new javax.swing.JTextField();
        doctor_panel = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        doctor_schedule_table = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        save_button = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        button_panel = new javax.swing.JPanel();
        delete_button = new javax.swing.JButton();
        new_button = new javax.swing.JButton();
        search_button = new javax.swing.JButton();
        table_panel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        doctor_table = new javax.swing.JTable();

        edit_dialog.setTitle("Doctor Edit Dialog");
        edit_dialog.setResizable(false);

        main_panel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        element_panel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        name_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        name_label.setText("Name:");

        name_textfield.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        address_labe.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        address_labe.setText("Address:");

        address_textfield.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        contact_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        contact_label.setText("Contact:");

        contact_textfield.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Nationality:");

        nationality_textfield.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Qualification:");

        qualification_textfield.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Specialist:");

        specialist_textfield.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Enrolled From:");

        enrolledfrome_textfield.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Gender:");

        gender_combobox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        gender_combobox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Male", "Female", "Other" }));

        department_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        department_label.setText("Department:");

        department_combobox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        depart_button.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        depart_button.setText("New Department");
        depart_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                depart_buttonActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("New:");

        newdepart_textfield.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout element_panelLayout = new javax.swing.GroupLayout(element_panel);
        element_panel.setLayout(element_panelLayout);
        element_panelLayout.setHorizontalGroup(
            element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(element_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(element_panelLayout.createSequentialGroup()
                        .addComponent(department_label)
                        .addGap(18, 18, 18)
                        .addComponent(department_combobox, 0, 233, Short.MAX_VALUE))
                    .addGroup(element_panelLayout.createSequentialGroup()
                        .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(name_label)
                            .addComponent(contact_label))
                        .addGap(40, 40, 40)
                        .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(contact_textfield)
                            .addComponent(name_textfield)))
                    .addGroup(element_panelLayout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(8, 8, 8)
                        .addComponent(enrolledfrome_textfield))
                    .addGroup(element_panelLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(qualification_textfield)))
                .addGap(38, 38, 38)
                .addComponent(depart_button)
                .addGap(29, 29, 29)
                .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(element_panelLayout.createSequentialGroup()
                        .addComponent(address_labe)
                        .addGap(26, 26, 26)
                        .addComponent(address_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(element_panelLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(nationality_textfield))
                    .addGroup(element_panelLayout.createSequentialGroup()
                        .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6))
                        .addGap(18, 18, 18)
                        .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(specialist_textfield)
                            .addComponent(gender_combobox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(newdepart_textfield))))
                .addContainerGap())
        );
        element_panelLayout.setVerticalGroup(
            element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(element_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(name_label)
                        .addComponent(address_labe)
                        .addComponent(address_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(name_textfield, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(contact_label)
                        .addComponent(contact_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(nationality_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(qualification_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(specialist_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(enrolledfrome_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(gender_combobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(department_label)
                        .addComponent(department_combobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(depart_button)
                        .addComponent(jLabel6)
                        .addComponent(newdepart_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        doctor_panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Doctor Schedule", 0, 0, new java.awt.Font("Traditional Arabic", 0, 18))); // NOI18N

        doctor_schedule_table.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        doctor_schedule_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
            }
        ));
        doctor_schedule_table.setToolTipText("");
        jScrollPane3.setViewportView(doctor_schedule_table);

        javax.swing.GroupLayout doctor_panelLayout = new javax.swing.GroupLayout(doctor_panel);
        doctor_panel.setLayout(doctor_panelLayout);
        doctor_panelLayout.setHorizontalGroup(
            doctor_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, doctor_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3)
                .addContainerGap())
        );
        doctor_panelLayout.setVerticalGroup(
            doctor_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(doctor_panelLayout.createSequentialGroup()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        save_button.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        save_button.setText("Save");
        save_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                save_buttonActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton1.setText("Exit");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(save_button)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(save_button)
                    .addComponent(jButton1))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout main_panelLayout = new javax.swing.GroupLayout(main_panel);
        main_panel.setLayout(main_panelLayout);
        main_panelLayout.setHorizontalGroup(
            main_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(main_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(main_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(element_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(doctor_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        main_panelLayout.setVerticalGroup(
            main_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(main_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(element_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(doctor_panel, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout edit_dialogLayout = new javax.swing.GroupLayout(edit_dialog.getContentPane());
        edit_dialog.getContentPane().setLayout(edit_dialogLayout);
        edit_dialogLayout.setHorizontalGroup(
            edit_dialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(main_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        edit_dialogLayout.setVerticalGroup(
            edit_dialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(main_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        button_panel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        delete_button.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        delete_button.setText("Delete");
        delete_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delete_buttonActionPerformed(evt);
            }
        });

        new_button.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        new_button.setText("New");
        new_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                new_buttonActionPerformed(evt);
            }
        });
        new_button.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                new_buttonKeyPressed(evt);
            }
        });

        search_button.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        search_button.setText("Search");
        search_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                search_buttonActionPerformed(evt);
            }
        });
        search_button.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                search_buttonKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout button_panelLayout = new javax.swing.GroupLayout(button_panel);
        button_panel.setLayout(button_panelLayout);
        button_panelLayout.setHorizontalGroup(
            button_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(button_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(new_button)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(search_button)
                .addGap(131, 131, 131)
                .addComponent(delete_button)
                .addContainerGap())
        );
        button_panelLayout.setVerticalGroup(
            button_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(button_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(button_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(delete_button)
                    .addComponent(new_button)
                    .addComponent(search_button))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        doctor_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Department ID", "Staff ID", "Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(doctor_table);

        javax.swing.GroupLayout table_panelLayout = new javax.swing.GroupLayout(table_panel);
        table_panel.setLayout(table_panelLayout);
        table_panelLayout.setHorizontalGroup(
            table_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, table_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 502, Short.MAX_VALUE)
                .addContainerGap())
        );
        table_panelLayout.setVerticalGroup(
            table_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, table_panelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(73, 73, 73))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(button_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(table_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(table_panel, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(button_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void search_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_search_buttonActionPerformed
       action_on_search_button_click();
        // TODO add your handling code here:
    }//GEN-LAST:event_search_buttonActionPerformed

    private void search_buttonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_search_buttonKeyPressed
        
        if(evt.getKeyCode()== KeyEvent.VK_ENTER)
            action_on_search_button_click();
        // TODO add your handling code here:
    }//GEN-LAST:event_search_buttonKeyPressed

    private void new_buttonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_new_buttonKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER)
            action_on_new_button_click();
        // TODO add your handling code here:
    }//GEN-LAST:event_new_buttonKeyPressed

    private void new_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_new_buttonActionPerformed
        action_on_new_button_click();
        // TODO add your handling code here:
    }//GEN-LAST:event_new_buttonActionPerformed

    private void depart_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_depart_buttonActionPerformed

            department_combobox.setEnabled(!department_combobox.isEnabled());
      newdepart_textfield.setEnabled(!department_combobox.isEnabled());
        // TODO add your handling code here:
    }//GEN-LAST:event_depart_buttonActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        edit_dialog.dispose();
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void save_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_save_buttonActionPerformed
        // TODO add your handling code here:
        getnewdoctordata();
    }//GEN-LAST:event_save_buttonActionPerformed

    private void delete_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_delete_buttonActionPerformed
                // TODO add your handling code here:
        if(JOptionPane.showConfirmDialog(null,"Do You Really Want To Delete??","Please Confirm",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION)
        {
             if (isclicked){
                 onDelete();
             }
        }
    }//GEN-LAST:event_delete_buttonActionPerformed
    public void onDelete(){
        isclicked = false;
        HashMap<String,Object> vals = new HashMap();
        vals.put(fields.department, staffid.doctor);
        vals.put(fields.id, Integer.valueOf(currentID));
        try {
            admin.deleteEntry(vals);
            usermanager.docpanelrefresh();
        } catch (SQLException ex) {
            Logger.getLogger(DoctorTable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public Boolean validateshift(String shift){
        if(shift==null){return true;}
        if(shift.equalsIgnoreCase("-") || shift.equalsIgnoreCase("")){return true;}
        String[] sh = shift.split("-");
        if(sh.length<2){return false;}
        try{
            Integer f = Integer.valueOf(sh[0]);
            f = Integer.valueOf(sh[1]);
        }catch(NumberFormatException e){
            return false;
        }
        return true;
    }
    
    public void getnewdoctordata(){
      String name = name_textfield.getText();
      String contact = contact_textfield.getText();
      String qualification = qualification_textfield.getText();
      String address = address_textfield.getText();
      String nationality = nationality_textfield.getText();
      String specialist = specialist_textfield.getText();
      String gender = "";
      if(((String)gender_combobox.getSelectedItem()).equalsIgnoreCase("male")){
          gender = "M";
      }else if(((String)gender_combobox.getSelectedItem()).equalsIgnoreCase("female")){
          gender = "F";
      }else{gender="O";}
      String department = "";
      if(depart_button.isSelected()){
          department = newdepart_textfield.getText();
      }else{
          department = ((String)department_combobox.getSelectedItem()).toLowerCase();
      }
      String enrolledfrom = enrolledfrome_textfield.getText();
      //get the shifts here
      
      if(name.equalsIgnoreCase("")){JOptionPane.showMessageDialog(null,"Please enter name atleast!"); return;}
      
      String sun = (String)doctor_schedule_table.getValueAt(0,0);
      String mon = (String)doctor_schedule_table.getValueAt(0,1);
      String tue = (String)doctor_schedule_table.getValueAt(0,2);
      String wed = (String)doctor_schedule_table.getValueAt(0,3);
      String thu = (String)doctor_schedule_table.getValueAt(0,4);
      String fri = (String)doctor_schedule_table.getValueAt(0,5);
      String sat = (String)doctor_schedule_table.getValueAt(0,6);
      if(!validateshift(sun) || !validateshift(mon) || !validateshift(tue)|| !validateshift(wed)|| !validateshift(thu)|| !validateshift(fri)|| !validateshift(sat)){
          JOptionPane.showMessageDialog(null,"Invalid doctor shift entered");
          return;
      }
      
      HashMap<String,Object> val = new HashMap();
      val.put(fields.department, staffid.doctor);
      val.put(fields.name, name); val.put(fields.address,address);
      val.put(fields.contact, contact); val.put(fields.qualification,qualification);
      val.put(fields.address, address); val.put(fields.nationality,nationality);
      val.put(fields.sex, gender); val.put(fields.address,address);
      val.put(fields.docdept, department); val.put(fields.enrolledfrom,enrolledfrom);
      val.put(fields.specialist,specialist);
      
      insertTime(val,"Sun_Check_In","Sun_Check_Out",sun);
      insertTime(val,"Mon_Check_In","Mon_Check_Out",mon);
      insertTime(val,"Tue_Check_In","Tue_Check_Out",tue);
      insertTime(val,"Wed_Check_In","Wed_Check_Out",wed);
      insertTime(val,"Thu_Check_In","Thu_Check_Out",thu);
      insertTime(val,"Fri_Check_In","Fri_Check_Out",fri);
      insertTime(val,"Sat_Check_In","Sat_Check_Out",sat);
        try {
            if(isnew){
               HashMap<String,String> ret = admin.createEntry(val);
                edit_dialog.dispose();
                usermanager.docpanelrefresh();
                JOptionPane.showMessageDialog(null,"Entry created sucessfully:\nName: "+ret.get(fields.name)+"\nID: "+ ret.get(fields.doctorid)+"\nDept: "+ret.get(fields.docdept)+"\nDept id: "+ret.get(fields.deptid));                
            }else{
                //means we are updating
                val.put(fields.doctorid, Integer.valueOf(editingid));
                val.put("timingid", Integer.valueOf(editingtimingid));
                admin.editEntry(val);
                edit_dialog.dispose();
                usermanager.docpanelrefresh();
                JOptionPane.showMessageDialog(null,"Editing entry sucessful!");
            }
                
        } catch (Throwable ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null,"Oops! something went wrong, please try again!");
        }
      
      
    }
    private void insertTime(HashMap<String,Object> o,String s,String e,String val){
        String[] vals = splitter(val);
        if(vals==null){
            o.put(s, null);
            o.put(e,null);
           
        }else{
            o.put(s, vals[0]);
            o.put(e, vals[1]);
           
        }
        
    }
    private String[] splitter(String d){
        if(d==null){
            return null;
        }else if(d.equalsIgnoreCase("")||d.equalsIgnoreCase("-")){
            return null;
        }else{
            return d.split("-");
        }
        
    }
    private java.sql.Time toTime(String t) throws ParseException{
        
        DateFormat formatter = new SimpleDateFormat("HH:mm");
        return  new java.sql.Time(formatter.parse(t).getTime());
    }
    private void clear(){
        for(int i=0;i<7;i++){
            doctor_schedule_table.setValueAt("", 0, i);
        }
        name_textfield.setText("");
        address_textfield.setText("");
        contact_textfield.setText("");
        nationality_textfield.setText("");
        qualification_textfield.setText("");
        specialist_textfield.setText("");
        enrolledfrome_textfield.setText("");
        newdepart_textfield.setText("");
        
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel address_labe;
    private javax.swing.JTextField address_textfield;
    private javax.swing.JPanel button_panel;
    private javax.swing.JLabel contact_label;
    private javax.swing.JTextField contact_textfield;
    private javax.swing.JButton delete_button;
    private javax.swing.JRadioButton depart_button;
    private javax.swing.JComboBox department_combobox;
    private javax.swing.JLabel department_label;
    private javax.swing.JPanel doctor_panel;
    private javax.swing.JTable doctor_schedule_table;
    private javax.swing.JTable doctor_table;
    private javax.swing.JDialog edit_dialog;
    private javax.swing.JPanel element_panel;
    private javax.swing.JTextField enrolledfrome_textfield;
    private javax.swing.JComboBox gender_combobox;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JPanel main_panel;
    private javax.swing.JLabel name_label;
    private javax.swing.JTextField name_textfield;
    private javax.swing.JTextField nationality_textfield;
    private javax.swing.JButton new_button;
    private javax.swing.JTextField newdepart_textfield;
    private javax.swing.JTextField qualification_textfield;
    private javax.swing.JButton save_button;
    private javax.swing.JButton search_button;
    private javax.swing.JTextField specialist_textfield;
    private javax.swing.JPanel table_panel;
    // End of variables declaration//GEN-END:variables
}

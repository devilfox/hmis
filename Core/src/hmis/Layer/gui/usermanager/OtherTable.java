/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Layer.gui.usermanager;

import hmis.Admin.admin;
import hmis.Shared.Identifiers.fields;
import hmis.Shared.Identifiers.staffid;
import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Bhuwan
 */
public class OtherTable extends basepanel {
    

    /**
     * Creates new form OtherTable
     */
    String editingid = null;
    
    
    Boolean isnew = false;
    Boolean isclicked = false;
    UserManager usermanager;
    String currentDept;
    String currentID;
    String currentname;
    public OtherTable(UserManager um) {
        initComponents();
        initEvents();
        usermanager = um;
        other_table.setRowHeight(25);
        
        populate();
    }
    public OtherTable(UserManager um,List<List<String>> l){
        usermanager = um;
        initComponents();
        initEvents();
        other_table.setRowHeight(25);
        
            DefaultTableModel table = (DefaultTableModel) other_table.getModel();
            List<List<String>> other_list = l;
            
        for(int i=0;i<other_list.size();i++)
        {
            int j=0;
        table.addRow(new Object[]{other_list.get(i).get(j++),
                                  other_list.get(i).get(j++),
                                  other_list.get(i).get(j++)
                                });
        }
    }
    
    
    public final void populate(){ 
        try {
            DefaultTableModel table = (DefaultTableModel) other_table.getModel();
            List<List<String>> other_list = admin.getotherdetails();
            
        for(int i=0;i<other_list.size();i++)
        {
            int j=0;
        table.addRow(new Object[]{other_list.get(i).get(j++),
                                  other_list.get(i).get(j++),
                                  other_list.get(i).get(j++)
                                });
        }
        } catch (SQLException ex) {
            Logger.getLogger(DoctorTable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
     public void action_on_search_button_click()
    {
         button_panel.setLayout(new BorderLayout());
        button_panel.removeAll();
       button_panel.add(new SearchPanel(this));
       this.revalidate();
       this.repaint();
    }
     
     public void action_on_new_button_click()
     {
         clear();
         isnew = true;
         edit_dialog.setBounds(150,130,582,400);
         edit_dialog.setVisible(true);
         edit_dialog.setResizable(false);
         department_combobox.removeAll();
        try {
            List<List<String>> vals = admin.getotherdepartments();
            for(List<String>val:vals){
                department_combobox.addItem(val.get(1));
             }
        } catch (SQLException ex) {
            Logger.getLogger(OtherTable.class.getName()).log(Level.SEVERE, null, ex);
        }
     }
     @Override
     public Boolean onSearch(String id){
         other_table.removeAll();
         try {
            DefaultTableModel table = (DefaultTableModel) other_table.getModel();
            
            List<List<String>> other_list = admin.getotherdetails();
            
        for(int i=0;i<other_list.size();i++){
            int j=0;
            if(other_list.get(i).get(1).equalsIgnoreCase(id)){
                
                List<List<String>> lst = new ArrayList();
                List<String> l = new ArrayList();
                l.add(other_list.get(i).get(j++));
                l.add(other_list.get(i).get(j++));
                l.add(other_list.get(i).get(j++));
                lst.add(l);
                usermanager.otherpanelrefresh(lst);
                return true;
            }
        }
        } catch (SQLException ex) {
            Logger.getLogger(DoctorTable.class.getName()).log(Level.SEVERE, null, ex);
        }
         
         return false;
     }
     private void onDoubleClick(String id){
                       try {
                           isnew = false;
                            HashMap<String,String> rec = admin.getOtherInfo(id);
                            editingid = rec.get(fields.id);
                            
                           isnew = false;
                           edit_dialog.setBounds(150,130,582,400);
                           edit_dialog.setVisible(true);
                           edit_dialog.setResizable(false);
                           department_combobox.removeAll();
                          try {
                              List<List<String>> vals = admin.getotherdepartments();
                              for(List<String>val:vals){
                                  department_combobox.addItem(val.get(1));
                                  if(val.get(1).equalsIgnoreCase(rec.get(fields.department).toLowerCase())){
                                      department_combobox.setSelectedItem(val.get(1));
                                  }
                               }
                              gender_combobox.removeAllItems();
                              gender_combobox.addItem("male");
                               gender_combobox.addItem("female");
                                gender_combobox.addItem("other");
                              if(rec.get(fields.sex).equalsIgnoreCase("male")){
                                    gender_combobox.setSelectedItem("male");
                                }else if(rec.get(fields.sex).equalsIgnoreCase("female")){
                                    gender_combobox.setSelectedItem("female");
                                }else{
                                    gender_combobox.setSelectedItem("other");
                                }
                            name_textfield.setText(rec.get(fields.name));
                            contact_textfield.setText(rec.get(fields.contact));
                            address_textfield.setText(rec.get(fields.address));
                            enrolledfrome_textfield.setText(rec.get(fields.enrolledfrom));
                            username_textfield.setText(rec.get(fields.username));
                            password_textfield.setText(rec.get(fields.password));
                            password_textfield1.setText(rec.get(fields.password));
                          } catch (SQLException ex) {
                              Logger.getLogger(OtherTable.class.getName()).log(Level.SEVERE, null, ex);
                          }
        } catch (SQLException ex) {
                           Logger.getLogger(OtherTable.class.getName()).log(Level.SEVERE, null, ex);
                       }
     }
     private void initEvents(){
         other_table.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
         other_table.addMouseListener(new java.awt.event.MouseAdapter() {
             @Override
             public void mouseClicked(java.awt.event.MouseEvent evt){
                 int row = other_table.getSelectedRow();
                 int column = 0;
                 if(evt.getClickCount()>1){
//                     String value = (String)other_table.getValueAt(row, column);
                        onDoubleClick((String)other_table.getValueAt(row, 1));
                 }else if (evt.getClickCount()==1){
                     isclicked = true;
                     currentDept = (String)other_table.getValueAt(row, 0);
                     currentID = (String)other_table.getValueAt(row, 1);
                     currentname = (String)other_table.getValueAt(row, 2);
                 }
             }
         });
     }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        edit_dialog = new javax.swing.JDialog();
        main_panel = new javax.swing.JPanel();
        element_panel = new javax.swing.JPanel();
        name_label = new javax.swing.JLabel();
        name_textfield = new javax.swing.JTextField();
        address_labe = new javax.swing.JLabel();
        address_textfield = new javax.swing.JTextField();
        contact_label = new javax.swing.JLabel();
        contact_textfield = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        enrolledfrome_textfield = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        gender_combobox = new javax.swing.JComboBox();
        department_label = new javax.swing.JLabel();
        department_combobox = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        username_textfield = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        password_textfield = new javax.swing.JPasswordField();
        password_textfield1 = new javax.swing.JPasswordField();
        jLabel3 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        save_button = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        table_panel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        other_table = new javax.swing.JTable();
        button_panel = new javax.swing.JPanel();
        delete_button = new javax.swing.JButton();
        new_button = new javax.swing.JButton();
        search_button = new javax.swing.JButton();

        edit_dialog.setTitle("Reception Edit Dialog");
        edit_dialog.setResizable(false);

        main_panel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        element_panel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        name_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        name_label.setText("Name:");

        name_textfield.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        address_labe.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        address_labe.setText("Address:");

        address_textfield.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        contact_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        contact_label.setText("Contact:");

        contact_textfield.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Enrolled From:");

        enrolledfrome_textfield.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        enrolledfrome_textfield.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enrolledfrome_textfieldActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Gender:");

        gender_combobox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        gender_combobox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Male", "Female", "Other" }));

        department_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        department_label.setText("Department:");

        department_combobox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Username:");

        username_textfield.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Password:");

        password_textfield.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        password_textfield.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                password_textfieldActionPerformed(evt);
            }
        });

        password_textfield1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        password_textfield1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                password_textfield1ActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Re-type:");

        javax.swing.GroupLayout element_panelLayout = new javax.swing.GroupLayout(element_panel);
        element_panel.setLayout(element_panelLayout);
        element_panelLayout.setHorizontalGroup(
            element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(element_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(element_panelLayout.createSequentialGroup()
                        .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(element_panelLayout.createSequentialGroup()
                                .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(contact_label)
                                    .addComponent(name_label))
                                .addGap(49, 49, 49)
                                .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(name_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(contact_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(department_label))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, element_panelLayout.createSequentialGroup()
                        .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(element_panelLayout.createSequentialGroup()
                                .addComponent(address_labe)
                                .addGap(50, 50, 50)
                                .addComponent(address_textfield))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, element_panelLayout.createSequentialGroup()
                                .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel3))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(enrolledfrome_textfield)
                                    .addGroup(element_panelLayout.createSequentialGroup()
                                        .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(password_textfield1)
                                            .addComponent(department_combobox, javax.swing.GroupLayout.Alignment.LEADING, 0, 210, Short.MAX_VALUE)
                                            .addComponent(gender_combobox, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(username_textfield, javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(password_textfield, javax.swing.GroupLayout.Alignment.LEADING))
                                        .addGap(0, 29, Short.MAX_VALUE)))))
                        .addGap(307, 307, 307))
                    .addGroup(element_panelLayout.createSequentialGroup()
                        .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        element_panelLayout.setVerticalGroup(
            element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(element_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(name_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(name_label))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(contact_label)
                    .addComponent(contact_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(address_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(address_labe))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(enrolledfrome_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(14, 14, 14)
                .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(gender_combobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(department_label)
                    .addComponent(department_combobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(username_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2)
                    .addComponent(password_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                .addGroup(element_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(password_textfield1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addContainerGap())
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        save_button.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        save_button.setText("Save");
        save_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                save_buttonActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton1.setText("Exit");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(save_button)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(save_button)
                    .addComponent(jButton1))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout main_panelLayout = new javax.swing.GroupLayout(main_panel);
        main_panel.setLayout(main_panelLayout);
        main_panelLayout.setHorizontalGroup(
            main_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(main_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(main_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(element_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        main_panelLayout.setVerticalGroup(
            main_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(main_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(element_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout edit_dialogLayout = new javax.swing.GroupLayout(edit_dialog.getContentPane());
        edit_dialog.getContentPane().setLayout(edit_dialogLayout);
        edit_dialogLayout.setHorizontalGroup(
            edit_dialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(edit_dialogLayout.createSequentialGroup()
                .addComponent(main_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        edit_dialogLayout.setVerticalGroup(
            edit_dialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(main_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        other_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Department ID", "Staff ID", "Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(other_table);

        javax.swing.GroupLayout table_panelLayout = new javax.swing.GroupLayout(table_panel);
        table_panel.setLayout(table_panelLayout);
        table_panelLayout.setHorizontalGroup(
            table_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, table_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 512, Short.MAX_VALUE)
                .addContainerGap())
        );
        table_panelLayout.setVerticalGroup(
            table_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, table_panelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(78, 78, 78))
        );

        button_panel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        delete_button.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        delete_button.setText("Delete");
        delete_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delete_buttonActionPerformed(evt);
            }
        });
        delete_button.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                delete_buttonKeyPressed(evt);
            }
        });

        new_button.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        new_button.setText("New");
        new_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                new_buttonActionPerformed(evt);
            }
        });
        new_button.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                new_buttonKeyPressed(evt);
            }
        });

        search_button.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        search_button.setText("Search");
        search_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                search_buttonActionPerformed(evt);
            }
        });
        search_button.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                search_buttonKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout button_panelLayout = new javax.swing.GroupLayout(button_panel);
        button_panel.setLayout(button_panelLayout);
        button_panelLayout.setHorizontalGroup(
            button_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(button_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(new_button)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(search_button)
                .addGap(131, 131, 131)
                .addComponent(delete_button)
                .addContainerGap())
        );
        button_panelLayout.setVerticalGroup(
            button_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(button_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(button_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(delete_button)
                    .addComponent(new_button)
                    .addComponent(search_button))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(table_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(button_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(table_panel, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(button_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void search_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_search_buttonActionPerformed
        action_on_search_button_click();
        // TODO add your handling code here:
    }//GEN-LAST:event_search_buttonActionPerformed

    private void search_buttonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_search_buttonKeyPressed

        if(evt.getKeyCode()== KeyEvent.VK_ENTER)
        action_on_search_button_click();
        // TODO add your handling code here:
    }//GEN-LAST:event_search_buttonKeyPressed

    private void new_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_new_buttonActionPerformed
        
        action_on_new_button_click();
        // TODO add your handling code here:
    }//GEN-LAST:event_new_buttonActionPerformed

    private void new_buttonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_new_buttonKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER)
            action_on_new_button_click();// TODO add your handling code here:
    }//GEN-LAST:event_new_buttonKeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       
        edit_dialog.dispose();
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void enrolledfrome_textfieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enrolledfrome_textfieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_enrolledfrome_textfieldActionPerformed

    private void delete_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_delete_buttonActionPerformed
        // TODO add your handling code here:
        if(JOptionPane.showConfirmDialog(null,"Do You Really Want To Delete??","Please Confirm",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION)
        {
             if (isclicked){
                 onDelete();
             }
        }
    }//GEN-LAST:event_delete_buttonActionPerformed

    private void save_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_save_buttonActionPerformed
        // TODO add your handling code here:
        String name = ((String)name_textfield.getText()).toLowerCase();
        String contact = ((String)contact_textfield.getText()).toLowerCase();
        String address = ((String)address_textfield.getText()).toLowerCase();
        String enrolled = ((String)enrolledfrome_textfield.getText()).toLowerCase();
        String gender = "";
        if(((String)gender_combobox.getSelectedItem()).equalsIgnoreCase("male")){
            gender = "m";
        }else if(((String)gender_combobox.getSelectedItem()).equalsIgnoreCase("female")){
            gender = "f";
        }else{
            gender = "o";
        }
        String department = "";
        
            department = ((String)department_combobox.getSelectedItem()).toLowerCase();
        
        String username = (String)username_textfield.getText();
        String password = new String(password_textfield.getPassword());
        String repassword = new String(password_textfield1.getPassword());
        
        
        if(username.equalsIgnoreCase("") || password.equalsIgnoreCase("")){
            JOptionPane.showMessageDialog(null,"Credentials cannot be empty!");
            return;
        }else if(name.equalsIgnoreCase("") || department.equalsIgnoreCase("")){
            JOptionPane.showMessageDialog(null,"Please fill the fields properly!");
            return;
        }else if(!password.equals(repassword)){
                JOptionPane.showMessageDialog(null,"Passwords don't match! ");
            return;
        }
        
        HashMap<String,Object> val = new HashMap();
        val.put(fields.department, "other");
        val.put(fields.name, name); val.put(fields.contact, contact);   val.put(fields.address, address);
        val.put(fields.enrolledfrom, enrolled); val.put(fields.sex, gender);    
        val.put(fields.otherdept, department); val.put(fields.username, username);  val.put(fields.password, password);
        
            try {
                if(isnew){
                    HashMap<String,String> ret = admin.createEntry(val);
                    edit_dialog.dispose();
                    usermanager.otherpanelrefresh();
                    JOptionPane.showMessageDialog(null,"entry sucessfully created!\nName: "+ret.get(fields.name)+"\nID: "+ret.get(fields.id));
                }else{
                    //
                    
                    val.put(fields.id, editingid);
                    admin.editEntry(val);
                    edit_dialog.dispose();
                    usermanager.otherpanelrefresh();
                    JOptionPane.showMessageDialog(null,"Editing entry sucessful !");
                }
            } catch (Throwable ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(null,"dulblicate username entered!");
            }
        
        
    }//GEN-LAST:event_save_buttonActionPerformed

    private void password_textfieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_password_textfieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_password_textfieldActionPerformed

    private void delete_buttonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_delete_buttonKeyPressed
                // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER)
        {
            if(JOptionPane.showConfirmDialog(null,"Do You Really Want To Delete??","Please Confirm",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION)
        {
             if (isclicked){
                 onDelete();
             }
        }
        }
    }//GEN-LAST:event_delete_buttonKeyPressed

    private void password_textfield1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_password_textfield1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_password_textfield1ActionPerformed

    public void onDelete(){
        HashMap<String,Object> vals = new HashMap();
        vals.put(fields.department, staffid.receptionist);
        vals.put(fields.id, Integer.valueOf(currentID));
        try {
            admin.deleteEntry(vals);
            usermanager.otherpanelrefresh();
        } catch (SQLException ex) {
            Logger.getLogger(DoctorTable.class.getName()).log(Level.SEVERE, null, ex);
        }
        isclicked = false;
    }
    
    private void clear(){
        name_textfield.setText("");
        contact_textfield.setText("");
        address_textfield.setText("");
        enrolledfrome_textfield.setText("");
        username_textfield.setText("");
        password_textfield.setText("");
        
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel address_labe;
    private javax.swing.JTextField address_textfield;
    private javax.swing.JPanel button_panel;
    private javax.swing.JLabel contact_label;
    private javax.swing.JTextField contact_textfield;
    private javax.swing.JButton delete_button;
    private javax.swing.JComboBox department_combobox;
    private javax.swing.JLabel department_label;
    private javax.swing.JDialog edit_dialog;
    private javax.swing.JPanel element_panel;
    private javax.swing.JTextField enrolledfrome_textfield;
    private javax.swing.JComboBox gender_combobox;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPanel main_panel;
    private javax.swing.JLabel name_label;
    private javax.swing.JTextField name_textfield;
    private javax.swing.JButton new_button;
    private javax.swing.JTable other_table;
    private javax.swing.JPasswordField password_textfield;
    private javax.swing.JPasswordField password_textfield1;
    private javax.swing.JButton save_button;
    private javax.swing.JButton search_button;
    private javax.swing.JPanel table_panel;
    private javax.swing.JTextField username_textfield;
    // End of variables declaration//GEN-END:variables
}

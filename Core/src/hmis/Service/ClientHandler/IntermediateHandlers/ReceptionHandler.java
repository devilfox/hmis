/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Service.ClientHandler.IntermediateHandlers;

import hmis.Admin.admin;
import hmis.Event.logger.log;
import hmis.Layer.Core.Client;
import hmis.Service.ClientHandler.finalServiceHandler.ReceptionHandlerThread;
import hmis.Shared.Commobjects.primitive._rr;
import hmis.Shared.Exception.GlobalException;
import hmis.Shared.Identifiers.codes;

/**
 *
 * @author daredevils
 */
public class ReceptionHandler {
    //client is to whom this handler will service
    Client client;
    //all SQL statements for prepared staments will be enumerated here
    /*
     * 
     * 
     */
    
    public ReceptionHandler(Client cli){
        client = cli;
    }
    public void executeService() throws GlobalException{
        admin.threadscount++;
        //this is the section where all the services will be executed on the basis of the received requests
        //flag will determine the fate of the service handler
        System.out.println("here we are in execution thread");
        Boolean flag = true;
        try{
            while(flag){
                //read requests from the client in form of the object
                //each time, this thread will wait for the requests from the client
                    _rr request = client.readObj();
                    if(request.getid().equalsIgnoreCase(codes.DeauthenticationRequest)){
                        client.close();
                        throw new GlobalException(codes.ObjectReadError,"client disconnected",null);
                    }
                // a next response thread will be executed to respond to the request
                //hence multiple requests will be handled asynchronously/parallely
                //thread name will be the request_ID of request sent by client
                    //only request execpt logout request will be handled by thread
                    log.log("servicing Client \""+client.getid()+"\" request, request id "+request.getid()+" :)");
                    ReceptionHandlerThread thread = new ReceptionHandlerThread(client,request);
                    thread.execute();
                    System.out.println("here-------------");
                    
            }
        }catch(GlobalException ex){
            System.out.println(ex.getExceptionDesc());
            switch (ex.getExceptionCode()){
                case codes.ObjectReadError:{
                    //this means that the client has disconnected
                    //same exception will be reported to lower abstraction level to take necessaru steps
                    admin.threadscount--;
                    throw ex;
                }
            }
        }
        admin.threadscount--;
     }
}


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Service.ClientHandler.MainHandler;

import hmis.Admin.admin;
import hmis.Event.logger.log;
import hmis.Shared.Identifiers.Dept;
import hmis.Layer.Core.Client;
import hmis.Shared.Exception.GlobalException;
import hmis.Service.ClientHandler.IntermediateHandlers.ReceptionHandler;
import hmis.Shared.Identifiers.DBCodes;
import hmis.layer.DB.Manager.DBManager;

/**
 *
 * @author daredevils
 */
public class MainHandler {
    private Client client;
    public MainHandler(Client cli){
        client = cli;
    }
    public void executeService() throws GlobalException{
        admin.threadscount++;
        //this will be the method that will service the client at highest abstraction level
        //0. At this level, this handler will only hands the handle to the respective department handler
        //1. Then the respective department handler will service the client connected
       try{
           client.db = new DBManager(DBCodes.dbName,DBCodes.user,DBCodes.pass);
           
            switch(client.getdeptid()){
                case Dept.Reception:{
                    //the reception department handler will be called here
                    log.log("Client \""+client.getid()+"\" belongs to \"reception\" :)");
                    System.out.println("------------------------------------------------------------------------------");
                    System.out.println("Core: Client Detected as Reception");
                    System.out.println("Core: Handing Client "+client.getid()+" to respective Reception Handler...DONE");
                    System.out.println("Core: ALL SYSTEMS OK");
                    System.out.println("------------------------------------------------------------------------------");
                    ReceptionHandler reception = new ReceptionHandler(client);
                        reception.executeService();
                    break;
                }
                case Dept.Lab:{
                    //the reception department handler will be called here
                    log.log("Client \""+client.getid()+"\" belongs to \"lab\" :)");
                    System.out.println("------------------------------------------------------------------------------");
                    System.out.println("Core: Client Detected as Lab");
                    System.out.println("Core: Handing Client "+client.getid()+" to respective Reception Handler...DONE");
                    System.out.println("Core: ALL SYSTEMS OK");
                    System.out.println("------------------------------------------------------------------------------");
                    ReceptionHandler reception = new ReceptionHandler(client);
                        reception.executeService();
                    break;
                }
                //use case for other department
            }
       } catch(GlobalException e){
           //rethrowing the exception to lower level
           admin.threadscount--;
           throw e;
       }
       admin.threadscount--;
    }
    
}

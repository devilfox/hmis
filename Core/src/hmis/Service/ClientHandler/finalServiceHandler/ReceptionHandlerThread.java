/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Service.ClientHandler.finalServiceHandler;
import hmis.Admin.admin;
import hmis.DB.Queries.Doctor_Schedule_Query;
import hmis.DB.Queries.OPD_Reg_Query;
import hmis.DB.Queries.Staff_Timings;
import hmis.DB.Queries.Update_Appointment_Panel_Constraints_Query;
import hmis.DB.Queries.Update_Ipd_Panel_Constraints_Query;
import hmis.DB.Queries.Update_Lab_Panel_Constraints_Query;
import hmis.DB.Queries.Update_OldPatientPanel_Constraints_Query;
import hmis.DB.Queries.Update_Opd_Panel_Constraints_Query;
import hmis.DB.Queries.Update_SearchPatientPanel_Constraints_Query;
import hmis.DB.lab.LabPatient;
import hmis.DB.lab.Lab_Query;
import hmis.Engines.AppointmentSchedulerEngine.ASEngine;
import hmis.Engines.AppointmentSchedulerEngine.ASEngine.Appointment;
import hmis.Layer.Core.Client;
import hmis.Layer.Time.Time;
import hmis.Shared.Commobjects.primitive.ReceptionModuleUpdates;
import hmis.Shared.Commobjects.primitive._rr;
import hmis.Shared.Commobjects.primitive.updaterequest;
import hmis.Shared.Commobjects.standard.query;
import hmis.Shared.Exception.GlobalException;
import hmis.Shared.Identifiers.codes;
import hmis.Shared.Identifiers.fields;
import hmis.Shared.Identifiers.reception;
import hmis.Shared.Identifiers.request_types;
import hmis.Shared.Identifiers.section;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author daredevils
 This section will Handle specific requests per thread to enable multiple
 requests at once
 */
public class ReceptionHandlerThread implements Runnable{
    //A thread object with [request id as name of thread]
    Thread thread;
    //A client object to whom the thread is servicing
    Client client; 
    //all request goodies will be held by this object
   _rr request;
    private Time t;
    //this will contain all the requirements to service the request selectable by the request ID
    public ReceptionHandlerThread(Client obj, _rr ROBJ){
       thread = new Thread(this,ROBJ.getid());
       client = obj;
       request = ROBJ;
    }
  
    @Override
    public void run(){
        admin.threadscount++;
        //when the thread gets executed
        //will use switch with requestid[requsest.getID()] to properly handle the request
        //This will process all the possible requests generated in the reception section
        try {
            System.out.println("-----------------------------------------------------------------------");
            System.out.println("Core: Processing request from [Client,Dept]: ["+client.getid()+",Reception]");
            System.out.println("Core: Request ID: "+request.getid());
            System.out.println("Core: ALL SYSTEMS OK");
            System.out.println("-----------------------------------------------------------------------");
            //now route to specific processing methods using id of the request
            switch(request.getid()){
                
                case codes.updatemodule:{
                    //1. Here if the client asks to update its modules
                    this.updateprocessor(request);
                    break;
                }
                case codes.datainput:{
                    //Here if the client (reception is sending load of data for the server)
                    this.datainputprocessor((query)request);
                    break;
                }
            }
            
        } catch (GlobalException ex) {
            Logger.getLogger(ReceptionHandlerThread.class.getName()).log(Level.SEVERE, null, ex);
        }
     admin.threadscount--;   
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //request processors here
    private void datainputprocessor(query request) throws GlobalException{
        //process input data from client
        //1. Switch to the specific section
        //2. process input
        //3. signal client (success/failure)
        //Extract the data
        Object data = request.payload;
                    switch (request.section){
                        
                        case section.opdregisterpanel:{
                            //1. Extract the variables from data(patient data)
                            //2. push them into the database sucessfully
                            //3. return success notification to sender
                            Boolean isSuccess = null;
                            
                            HashMap<String,String> info=(HashMap<String,String>) data;
                            OPD_Reg_Query patient_registration=new OPD_Reg_Query(client.db.getConnection(),info);
                            switch(request.code){
                                case reception.OPDNewPatientRegister:
                                    isSuccess = patient_registration.Insert_Details();
                                    break;
                                case reception.OldPatientreRegister:
                                    isSuccess = patient_registration.Update_Details();
                                    break;
                                
                            }                            
                            if(isSuccess!=null){
                                HashMap<String,Object> pl = new HashMap();
                                pl.put(fields.issuccess, isSuccess);
                                pl.put(fields.name, ((HashMap<String,String>) data).get(fields.name));
                                pl.put(fields.patientid, ((HashMap<String,String>) data).get(fields.patientid));
                                pl.put(fields.examination_id, ((HashMap<String,String>) data).get(fields.examination_id));
                                query ret = request;
                                ret.payload = pl;
                                client.writeObj(ret);
                            }
                            break;
                        }
                        case section.oldpatientpanel:{
                            //1. Extract the variables from data(serach by)
                            //2. search the database for patient
                            //3. return success(with patient data)/failure notification to sender
                            
                            
                            //notification will be sent as: uses <section,success/failure,payload>
                            //query reply = this.createreply(request.getid(), Boolean.TRUE, request.section, request.code, data);
                            //client.writeObj(reply)
                            break;
                        }
                        case section.oldpatientsearchpanel:{
                            HashMap<String,List<HashMap<String,String>>> patient_info=new HashMap();
                            HashMap<String,String> search_parameter=new HashMap();
                            search_parameter=(HashMap<String, String>) request.payload;
                            Update_OldPatientPanel_Constraints_Query q=new Update_OldPatientPanel_Constraints_Query(client.db.getConnection());
                            System.out.println("processing request for oldpatientsearchpanel="+request.payload+"---"+search_parameter.get("available_info")+"----"+search_parameter.get("search_type"));
                            patient_info.put(fields.patient_info,q.Search_patient(search_parameter.get("available_info"),search_parameter.get("search_type")));
                            request.payload=patient_info;
                            client.writeObj(request);                         
                            break;
                        }
                        case section.docschedulepanel:{
                            
                            break;
                        }
                        case section.appointmentpanel:{
                            
                            switch(request.code){
                                case request_types.rq_feasible_appointment:{
                                    System.out.println("processing request from--------------------------------------------------<<<<<<<<<<<<");
                                    //now check for feasible appointment for the given data
                                    //retrive timings list first
                                    String dept =(String)((HashMap<String,Object>)request.payload).get(fields.department);
                                    String doc = (String)((HashMap<String,Object>)request.payload).get(fields.doctor);
                                    String date = (String)((HashMap<String,Object>)request.payload).get(fields.date);
                                    
                                    //now drop down the date to specific day here
                                    //using dummy style currently
                                    
                                    Calendar c = Calendar.getInstance();
                                try {
                                    c.setTime(new SimpleDateFormat("dd/M/yyyy").parse(date));
                                } catch (ParseException ex) {
                                    Logger.getLogger(ReceptionHandlerThread.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                    Integer day = c.get(Calendar.DAY_OF_WEEK)-1;//-1 to feed it into our engine which takes 0-6 as 
                                    
                                    
                                    
                                  
                                    
                                    Update_Appointment_Panel_Constraints_Query q=new Update_Appointment_Panel_Constraints_Query(client.db.getConnection());
                                    
       
                                    List<Staff_Timings> schedule_list=q.Retrieve_Doctor_Timings();
                                    //now drop down to the department here
                                    
                                    
                                    //Setup the engine
                                    ASEngine engine = new ASEngine();
                                    //Add respective department schedule into engine
                                    for(int i=0;i<schedule_list.size();i++){
                                        if(schedule_list.get(i).department.equalsIgnoreCase(dept) && (schedule_list.get(i).staff_name.equalsIgnoreCase(doc)||doc.equalsIgnoreCase("Any"))){
                                            System.out.println("Importing");
                                            engine.importDocSchedule(schedule_list.get(i).getSchedule(date,day,q));
                                        }
                                    }
                                    
                                    //get the appointment using engine
                                    System.out.println("<<<-----here----->>>");
                                    Appointment ap = engine.getFeasibleAppointment(day);
                                    System.out.println("<<<-----ends----->>>");
                                    
                                    HashMap<String,Object> map = new HashMap();
                                    //get the proper time information-------------------
                                    if(ap!=null){
                                        Integer factor = 0;
                                        if(ap.day>=day){
                                              factor = ap.day-day;
                                        }else{
                                            factor = 7-day+ap.day;
                                        }
                                        c.add(Calendar.DAY_OF_MONTH, factor);
                                        Time t = new Time();
                                        String from = "";
                                        String to = "";
                                        try {
                         
                                            String[] tym = ap.doc.getScheduleByDay(ap.day).split("-");
                                            String _24from = tym[0].substring(0, 2)+":"+tym[0].substring(2, tym[0].length());
                                            String __24to = tym[1].substring(0, 2)+":"+tym[1].substring(2, tym[1].length());
                                            
                                          
                                            
                                            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
                                            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
                                            
                                            java.util.Date _24HourDt = _24HourSDF.parse(_24from);
                                            from = _12HourSDF.format(_24HourDt);
                                            
                                            java.util.Date __24HourDt = _24HourSDF.parse(__24to);
                                            to = _12HourSDF.format(__24HourDt);
                                            
                                          
                                            
                                        } catch (ParseException ex) {
                                            Logger.getLogger(ReceptionHandlerThread.class.getName()).log(Level.SEVERE, null, ex);
                                        }//---------------------------time information processing ends here
                                        
                                        map.put(fields.issuccess,Boolean.TRUE);
                                        map.put(fields.doctorid, ap.doc.getID());
                                        map.put(fields.doctor, ap.doc.getName());
                                        map.put(fields.date,new SimpleDateFormat("dd MMM yyyy").format(c.getTime())+", "+t.tostrday(c.get(Calendar.DAY_OF_WEEK)-1));
                                        map.put("_time",from+"-"+to);
                                    }else{
                                        map.put(fields.issuccess,Boolean.FALSE);
                                    }
                                    
                                    request.payload = map;
                                    client.writeObj(request);
                                    break;
                                }
                                case request_types.rq_appointment_register:{
                                    //now register the appointment
                                    String dept =(String)((HashMap<String,Object>)request.payload).get(fields.department);
                                    String doc = (String)((HashMap<String,Object>)request.payload).get(fields.doctor);
                                    String date = (String)((HashMap<String,Object>)request.payload).get(fields.date);
                                    String name = (String)((HashMap<String,Object>)request.payload).get(fields.name);
                                    String contact = (String)((HashMap<String,Object>)request.payload).get(fields.contact);
                                    
                                    System.out.println(name+" "+contact+" "+date+" "+doc+" "+dept);
                                    //now push it to the database
                                    Update_Appointment_Panel_Constraints_Query q=new Update_Appointment_Panel_Constraints_Query(client.db.getConnection());
                                    q.Register_Appointment((HashMap<String,String>)request.payload);
                                    
                                    HashMap<String,Object> pl = new HashMap();
                                    pl.put(fields.issuccess, Boolean.TRUE); //this should be extracted from query results
                                    pl.put(fields.date,date);
                                    pl.put(fields.doctor, doc);
                                    pl.put(fields.name,name);
                                    pl.put(fields.contact, contact);
                                    request.payload = pl;
                                    client.writeObj(request);
                                    break;
                                }
                            }
                            
                            
                            break;
                        }
                        case section.viewappointmentpanel:{
                            Update_Appointment_Panel_Constraints_Query q=new Update_Appointment_Panel_Constraints_Query(client.db.getConnection());
                            q.Remove_Appointment_For_OPD_Register(((HashMap<String,String>)request.payload).get(fields.appointment_id));
                            break;
                        }
                        case section.ipdregisterpanel:{
                            Update_Ipd_Panel_Constraints_Query q=new Update_Ipd_Panel_Constraints_Query(client.db.getConnection());
                            switch(request.code){
                                case reception.OPDPatientInfoRequest:{
                                    request.payload=q.Get_Patient_Info(Integer.parseInt((String)request.payload));
                                    client.writeObj(request);
                                    break;
                                }
                                case reception.IPDPatientRegister:{
                                    Boolean flag  = q.Insert_IPD_Patient_Data((HashMap)request.payload);
                                    HashMap<String,Object> pl = new HashMap();
                                    pl.put(fields.issuccess,flag);
                                    
                                   
                                    pl.put(fields.patientid,((HashMap<String,String>)request.payload).get(fields.patientid));
                                    pl.put(fields.examination_id,((HashMap<String,String>)request.payload).get(fields.examination_id));
                                    
                                    request.payload = pl;
                                    
                                    client.writeObj(request);
                                    break;
                                }
                                default:
                                    break;
                            }
                                
                            break;
                        }
                        case section.billingpanel:{
                            
                            break;
                        }
                        case section.searchpanel:{
                            Update_SearchPatientPanel_Constraints_Query q=new Update_SearchPatientPanel_Constraints_Query(client.db.getConnection());
                            HashMap<String,String> incoming_info=new HashMap();
                            incoming_info=(HashMap<String, String>) request.payload;
                            List<HashMap<String,String>> patient_list=q.Search_patient(incoming_info.get("available_info"),incoming_info.get("search_type"));
                            HashMap<String,List<HashMap<String,String>>> output_data=new HashMap();
                            output_data.put(fields.patient_info,patient_list);
                            request.payload=output_data;
                            System.out.println("in search Panel"+patient_list);
                            client.writeObj(request);
                            System.out.println(""+request.section);
                            break;
                        }
                    
                 case section.labregisterpanel:{
                            Update_Lab_Panel_Constraints_Query q=new Update_Lab_Panel_Constraints_Query(client.db.getConnection());
                            Boolean success = q.register_patient_for_test((HashMap<String,String>)request.payload);
                            ((HashMap<String,String>)request.payload).put(fields.issuccess, success.toString());
                            client.writeObj(request);
                            System.out.println(""+request.section);
                            break;
                        }
                      //these queries are for laboratory section 
                        case section.viewindividualpatient:{
                            Lab_Query q=new Lab_Query(client.db.getConnection());
                            HashMap<String,LabPatient> info=new HashMap();
//                            System.out.println(""+(String)request.payload);
                            info.put(fields.patient_info,q.Get_Single_Patient_Info((String)request.payload));
                            request.payload=info;
                            client.writeObj(request);
                            System.out.println("in view individual patient"+info);
                            break;
                        }
                            
                        case section.labreportresultspanel:{
                            Lab_Query q=new Lab_Query(client.db.getConnection());
                            switch(request.code){
                                case "change_reportstatus":{
                                    q.change_test_status(((HashMap<String,String>)request.payload).get(fields.lab_registration_no),((HashMap<String,String>)request.payload).get(fields.test),((HashMap<String,String>)request.payload).get(fields.report_status));
                                    client.writeObj(request);
                                    System.out.println("Updateing status");
                                    break;
                                }
                                case "save_testattribs":{
                                    q.save_testresults((HashMap<String,String>)request.payload);
                                    System.out.println("Updateing test fields "+request.payload);
                                    client.writeObj(request);
                                    break;
                                    
                                }
                            }
                        }
                        case section.labpatientinfopanel:{
                            Lab_Query q=new Lab_Query(client.db.getConnection());
                            q.change_test_status(((HashMap<String,String>)request.payload).get(fields.lab_registration_no),((HashMap<String,String>)request.payload).get(fields.test),((HashMap<String,String>)request.payload).get(fields.report_status));
                            client.writeObj(request);
                            System.out.println("in Patient info panel");
                            break;
                        }
                    }
        
    }
    //this will process the updates
    private void updateprocessor(_rr request) throws GlobalException{
        
                    //now route update request according to the caller
                    String _sec = ((updaterequest)request).section;
                    //now route the request according to the section asked by client
                    switch (_sec){
                        
                        case section.opdregisterpanel:{
                            //Gather updates for OPD panel from DB and send updates to the client
                            Time t = new Time();
                            ReceptionModuleUpdates update = new ReceptionModuleUpdates();
                            String id = String.valueOf((int)(Math.random()*10000));//generate randomness here
                            String examid = String.valueOf((int)(Math.random()*10000)); //generate randomness here
                            update.time = t.getcurrenttime() + ";" +id+";"+examid; 
                            
                            
                            
                            
                            
                            update.updatefor = _sec;
                            String request_type = ((updaterequest)request).type;
                            switch(request_type){
                                case request_types.rq_available_doctors:{
                            //This portion of code reads the doctors,their ids and corresponding departments from database and  
                            //returns them in hashmap so that the doctors list is available during OPD registration
                                  
                                  update.type=request_types.rq_available_doctors;
                                  Update_Opd_Panel_Constraints_Query q1=new Update_Opd_Panel_Constraints_Query(client.db.getConnection());
                                  HashMap<String,Vector<HashMap<String,String>>> available_doctors=q1.Update_Opd_Panel_Constraints();
                                  update.updates = available_doctors;
                                  client.writeObj(update);
                                  break;
                                }
                                default:
                                  break;
                            }
                            break;
                        }
                        case section.oldpatientpanel:{
                            //Grather updates for Old patients from DB and send it to client
                            ReceptionModuleUpdates update = new ReceptionModuleUpdates();
                            update.updatefor = _sec;
                            client.writeObj(update);
                            break;
                        }
                        case section.docschedulepanel:{
                            //Grather updates for doctors schedule from DB and send it to client
                            ReceptionModuleUpdates update = new ReceptionModuleUpdates();
                            update.updatefor = section.docschedulepanel;
                            Doctor_Schedule_Query q=new Doctor_Schedule_Query(client.db.getConnection());
                            HashMap<String,List<Staff_Timings>> data=new HashMap();
                            data.put(fields.schedule, q.Retrieve_Doctor_Timings());
                            update.updates=data;
                            
                             client.writeObj(update);
                            
                            
                            break;
                        }
                        case section.appointmentpanel:{
                            //Grather updates for appointment panel from DB and send it to the client
                                //Grather updates for appointment panel from DB and send it to the client
//                            ReceptionModuleUpdates update = new ReceptionModuleUpdates();
//                            update.updatefor = _sec;
//                            Update_Appointment_Panel_Constraints_Query q=new Update_Appointment_Panel_Constraints_Query(client.db.getConnection());
//                            List<Staff_Timings> schedule_list=q.Retrieve_Doctor_Timings();
//                            HashMap<String,List<Staff_Timings>> output=new HashMap<>();
//                            output.put(fields.schedule, schedule_list);
//                            System.out.println(""+schedule_list.get(0).staff_name);
//                            update.updates=output;
//                            client.writeObj(update);
                               ReceptionModuleUpdates update=new ReceptionModuleUpdates();
                              hmis.Layer.Time.Time t = new Time();
                              update.time = t.getcurrenttime();
                              update.updatefor=_sec;
                              Update_Appointment_Panel_Constraints_Query q=new Update_Appointment_Panel_Constraints_Query(client.db.getConnection());
                              HashMap<String,Object> data=new HashMap();
                              data.put("Available_Doctors_Info", q.Get_Available_Doctors());
                              data.put("Appointments",q.Get_All_Appointments());
                              update.updates=data;
                              
                              System.out.println(""+q.Get_All_Appointments());
                              client.writeObj(update);
                              System.out.println("received update request from appointment panel");
                              break;
                        }
                        
                        case section.ipdregisterpanel:{
                                System.out.println("update request from ipd panel");
                                String request_type = ((updaterequest)request).type;
                 //Grather updates for IPD panel from DB and send it to client                            
                                ReceptionModuleUpdates update = new ReceptionModuleUpdates();
                                update.updatefor = _sec;
                                             
                                 switch(request_type){      
                                       case request_types.rq_available_docs_n_rooms:{
                                           System.out.println("update request for docs and rooms");
                //this portion of code queries for available doctors and available rooms from the database 
                // and makes it available for displaying in reception panel IPD registration section
                                            update.type=request_types.rq_available_docs_n_rooms;
                                            Update_Ipd_Panel_Constraints_Query q1=new Update_Ipd_Panel_Constraints_Query(client.db.getConnection());
                                            HashMap<String,Object> info=new HashMap();
                                            info.put("Available_Rooms_Info", q1.Get_Available_Rooms());
                                            info.put("Available_Doctors_Info",q1.Get_Available_Doctors());
                                            update.updates=info;                           
                                            client.writeObj(update);
                                            break;
                                            }
                                       
                                       default:
                                           break;
                                   
                        }
                            break;
                        }
                        case section.billingpanel:{
                            //Grather updates for billing panel and send it to client
                            ReceptionModuleUpdates update = new ReceptionModuleUpdates();
                            update.updatefor = _sec;
                            client.writeObj(update);
                            break;
                        }
                        case section.searchpanel:{
                            //Grather updates for search panel and send it to client
                            ReceptionModuleUpdates update = new ReceptionModuleUpdates();
                            update.updatefor = _sec;
                            client.writeObj(update);
                            break;
                        }
                      case section.labregisterpanel:{
                          
                            ReceptionModuleUpdates update=new ReceptionModuleUpdates();
                            update.updatefor=_sec;
                            hmis.Layer.Time.Time t = new Time();
                            update.time = t.getcurrenttime();
                            Update_Lab_Panel_Constraints_Query q=new Update_Lab_Panel_Constraints_Query(client.db.getConnection());
        //                    HashMap<String,List> info=new HashMap();
       //                     info.put(fields.test,q.Get_All_Tests());
                            update.updates=q.Get_All_Tests();
                            client.writeObj(update);
                            System.out.println("hehaahAcknowledged request from :"+_sec);
                            break;
                            
                        }
                            
                        case section.viewlallpatient:{
                            System.out.println("request by viewallpatients panel");
                            ReceptionModuleUpdates update = new ReceptionModuleUpdates();
                            update.updatefor =section.viewlallpatient;
                            Lab_Query q=new Lab_Query(client.db.getConnection());
                            HashMap<String,Object> data=new HashMap();
                            data.put("All_Patients_Info",q.Get_All_Patients_Info());
                            update.updates=data;
                            client.writeObj(update);
                            System.out.println("Reached here");
                            break;
                        }    
                    }
                    
    }
    public void execute(){
        thread.start();
    }
}
/*
switch (request.section){
                        
                        case section.opdregisterpanel:{
                            
                            
                            break;
                        }
                        case section.oldpatientpanel:{
                            
                            
                            break;
                        }
                        case section.docschedulepanel:{
                            
                            
                            break;
                        }
                        case section.appointmentpanel:{
                            
                            
                            break;
                        }
                        case section.ipdregisterpanel:{
                            
                            
                            break;
                        }
                        case section.billingpanel:{
                            
                            break;
                        }
                        case section.searchpanel:{
                            
                            break;
                        }
                    }
                    
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hmis.Shared.Commobjects.primitive;

import hmis.Shared.Identifiers.codes;

/**
 *
 * @author root
 */
public class Auth_Response extends _rr{
    public Boolean isSuccess = null;
    public Boolean isMultipleInstance = null; //This will define reason behind the authentication failure, and 
                                               //if this is true, than more than one instance of same person tried to log on else, void authentication
    public String deptid = null;
    public String username = null;
    public String clientid = null;
    public Auth_Response(){
        super.setid(codes.AuthenticationRequest);
    }
}

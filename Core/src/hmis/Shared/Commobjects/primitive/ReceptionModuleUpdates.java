/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Shared.Commobjects.primitive;

import hmis.Shared.Identifiers.codes;
import java.util.HashMap;
import java.util.Vector;

/**
 *
 * @author daredevils
 */
public class ReceptionModuleUpdates extends _rr {
    /*
     * This response will be invoked on client sending update request to the core
     * This class will handle the module updates for reception transffered in network
     */
    
    //For which section is this update intended to Eg. OPD, IPD etc..
    public String updatefor;  
    public String type;
    //According as the "updatefor" refers, specific updates will be retrived here
    /*time is universal update*/
    public String time;
    /*this hashmap will contains all the updates accessible by keywords such as dept,docs,rooms etc.. and will be related to <updatefor> section*/
    public Object updates;
    
    public ReceptionModuleUpdates(){
        super.setid(codes.updatemodule);
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Shared.Exception;

/**
 *
 * @author daredevils
 */
public class GlobalException extends Exception {
    String exceptioncode;                                                       //code will be used to define each exception conditions
    String Description = "No description available";                            //Description is provided to the exception conditions
    Object obj;
    
    public GlobalException(String exception_code, String description,Object Obj){
        exceptioncode = exception_code;
        if(!description.isEmpty()){
            Description = description;
        }
        obj = Obj;
    }
    
    public final String getExceptionCode(){                                           //Returns exceptioncode on demand
        return exceptioncode;
    }
    
    public final String getExceptionDesc(){                                            //Return exceptiondescription on demand
        return Description;
    }
    public Object getObject(){
        return obj;
    }
}

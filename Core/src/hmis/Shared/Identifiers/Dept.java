/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Shared.Identifiers;

/**
 *
 * @author daredevils
 */
public class Dept {
    public static final String Reception = "#DEPT_001";
    public static final String Doctor = "#DEPT_002";
    public static final String Lab = "#DEPT_003";
    public static final String Billing = "#DEPT_004";
    static public String getDeptNameByID(String ID){
        switch(ID){
            case Reception:{
                return "reception";
            }
            case Lab:
                return "lab";
        }
        return "-NULL-";
    }
}

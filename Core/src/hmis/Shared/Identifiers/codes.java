/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Shared.Identifiers;

/**
 *
 * @author daredevils
 * 
 */

public class codes {
    //authentication exception codes 
    public static final String voidAuthentication = "#EX_0012";
    public static final String Authenticated = "#EX_0014";
    
   //client request codes
    public static final String AuthenticationRequest = "#REQ_AUTH_MODULE";
    public static final String DeauthenticationRequest = "#REQ_DEAUTH_MODULE";
    public static final String TimeAndDateRequest = "#REQ_TYM";
    
    //error codes
    public static final String ExpectingDiffReq = "#ERR_007";
    
    //error exception codes 
    public static final String RulesError = "#EX_0015";
    public static final String ErrorConnectingDB = "#EX_0010";
    
    //reading and writing objects error
    public static final String ObjectReadError = "#EX_OBJRD";
    
    //update module request
    public static final String updatemodule ="#REQ_UPDATE_MODULE";
    
    //form data request
    public static final String datainput = "#REQ_DTA_INP";
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Shared.Identifiers;

/**
 *
 * @author daredevils
 */
public class fields {
    public static final String issuccess = "issuccess";
    public static final String name = "name";
    public static final String age = "age";
    public static final String sex = "sex";
    public static final String address = "address";
    public static final String contact = "contact";
    public static final String department = "department";
    public static final String doctor = "doctor";
    public static final String patientid = "patientid";
    public static final String doctorid = "doctorid";
    public static final String bloodgroup="bloodgroup";
    public static final String examination_id="examination_id";
    public static final String diagnosed_with="diagnosed_with";
    public static final String date_of_first_examination="data_of_first_examination";
    public static final String marital_status="marital_status";
    public static final String guardian_name="guardian";
    public static final String guardian_contact="guardian_contact";
    public static final String date_of_birth="dob";
    public static final String room_type="room_type";
    public static final String room_no="room_no";
    public static final String bed_no="bed_no";
    public static final String current_date="current_date";
    public static final String schedule="schedule";
    public static final String patient_info="pat_info";
    public static final String appointment_info="appointment_info";
    public static final String number_of_appointments="num_appointments";
    public static final String appointment_date="appt_date";
    public static final String date ="dte";
    public static final String username = "username";
    public static final String password = "passd";
    public static final String id = "id";
    public static final String nationality = "nation";
    public static final String qualification = "qual";
    public static final String deptid = "deptid";
    public static final String post = "post";
    public static final String specialist = "specialist";
    public static final String enrolledfrom = "ef";
    public static final String timingid = "tid";
    public static final String docdept = "docdept";
    public static final String otherdept = "otherdept";
   
    public static final String day ="_day";
    
    public static final String lab_registration_no="lrn";
    public static final String sample_date="sample_date";
    public static final String registration_date="reg_date";
    public static final String report_due_date="rad";
    public static final String report_status="rs";
    public static final String test_results="tr";
    public static final String test="test";
    public static final String test_id="test_id";
    public static final String test_attributes="t_attrib";
    
    public static final String appointment_id = "apptid";
}
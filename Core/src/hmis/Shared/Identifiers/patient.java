/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Shared.Identifiers;

/**
 *
 * @author Rupak
 */
public class patient {
    public static final String name="name";
    public static final String gender="gender";
    public static final String age="age";
    public static final String address="address";
    public static final String contact="contact_number";
    public static final String bloodgroup="bloodgroup";
    public static final String examination_id="examination_id";
    public static final String diagnosed_with="diagnosed_with";
    public static final String date_of_first_examination="data_of_first_examination";
    public static final String patient_id="patient_id";
    
}

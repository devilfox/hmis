/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hmis.Shared.Identifiers;

/**
 *
 * @author daredevils
 */
public class section {
    public static final String loginpanel = "LF";
    public static final String homescreen = "HS";
    public static final String receptionpanel = "RF";
    public static final String opdregisterpanel = "OPDREGF";
    public static final String oldpatientpanel = "OPF";
    public static final String ipdregisterpanel = "IPDREGF";
    public static final String appointmentpanel = "AF";
    public static final String docschedulepanel = "DSF";
    public static final String searchpanel = "SF";
    public static final String billingpanel = "BF";
    public static final String oldpatientsearchpanel="OPSF";
    public static final String labregisterpanel="LPSF";
    
    public static final String viewlallpatient="VLF";
    public static final String viewindividualpatient="VILF";
    public static final String labreportresultspanel="LRRF";
    public static final String labpatientinfopanel="LPIF";
    
        public static final String viewappointmentpanel="VAF";
}
